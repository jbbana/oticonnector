/**
 * 
 */
package com.oberthur.connector.oti.http;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author Illya Krakovskyy
 * 
 */
public class OTIHttpUtilsTest {

	private static final String SE_LIST_HEADER = "//se-id/CPLC/24;//se-id/ICCID/25";
	private static final String SE_LIST_HEADER_ICCID_HEX = "//se-id/CPLC/24;//se-id/ICCID/25010Faa";
	private static final String SE_LIST_HEADER_INCORRECT_ICCID = "//se-id/CPLC/24;//se-id/ICCID/25010F5G";

	/**
	 * Test method for
	 * {@link com.oberthur.connector.oti.http.OTIHttpUtils#validateSEList(java.lang.String)}
	 * .
	 */
	@Test
	public void testValidateSEList() {
		assertNotNull("SE map should not be null", OTIHttpUtils.parseSEList(SE_LIST_HEADER));
	}

	/**
	 * Test method for
	 * {@link com.oberthur.connector.oti.http.OTIHttpUtils#parseSEList(java.lang.String)}
	 * .
	 */
	@Test
	public void testParseSEList() {
		assertTrue(OTIHttpUtils.validateSEList(SE_LIST_HEADER));
	}

	@Test
	public void testParseSEListHexICCID() {
		assertTrue(OTIHttpUtils.validateSEList(SE_LIST_HEADER_ICCID_HEX));
	}

	@Test
	public void testParseSEListIncorrectICCID() {
		assertFalse(OTIHttpUtils.validateSEList(SE_LIST_HEADER_INCORRECT_ICCID));
	}
}
