/**
 * 
 */
package com.oberthur.connector.oti.http.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.prefs.Preferences;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.oberthur.apdugenerator.utils.Utils;
import com.oberthur.connector.api.SecureElementType;
import com.oberthur.connector.api.xml.bind.notification.Notification;
import com.oberthur.connector.api.xml.bind.response.OutOfSessionResponse;
import com.oberthur.connector.api.xml.bind.response.PrepareCommunicationResponse;
import com.oberthur.connector.api.xml.bind.response.SendScriptResponse;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.dto.AuthenticationDTO;
import com.oberthur.connector.oti.dto.PoRRequestDTO;
import com.oberthur.connector.oti.dto.SEPair;
import com.oberthur.connector.oti.dto.SessionStatus;
import com.oberthur.connector.oti.exceptions.DuplicateStateException;
import com.oberthur.connector.oti.exceptions.MissingSessionException;
import com.oberthur.connector.oti.exceptions.OTIAuthenticationException;
import com.oberthur.connector.oti.exceptions.OTIAuthenticationException.AuthenticationFailReason;
import com.oberthur.connector.oti.exceptions.OTIException;
import com.oberthur.connector.oti.http.ScriptStatus;
import com.oberthur.connector.oti.http.session.OTIAgentSession;
import com.oberthur.connector.oti.ram.state.AgentSessionState;
import com.oberthur.connector.oti.rest.ConnectorServiceCallbackProxy;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;

/**
 * @author Illya Krakovskyy
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class OTIControllerBeanTest {

	@InjectMocks
	private OTIControllerBean bean;

	@Mock
	private OTIPersistenceUtil persistenceUtil;

	@Mock
	private OTIAgentSession agentSession;

	@Mock
    private ConnectorServiceCallbackProxy callback;

	@Mock
	private CriteriaBuilder mockCriteriaBuilder;
	@Mock
	private CriteriaQuery<SessionOTI> mockCriteriaQuery;

	@Mock
	private TypedQuery<SessionOTI> mockQuery;
	@Mock
	private Root<SessionOTI> mockRoot;

	@Mock
	private EntityManager mockEntityManager;

	@Mock
	private SessionOTI mockSession;

	private static final String CONTENT = "AB29800102230EE30a4F08A0000001510000009000231442100102030405060708090A0B0C0C0E0F109000";
	private static final String SECURE_ELEMENT_ID = "4562323FE2142A";
	private static final String AGENT_ID = "01";

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		when(persistenceUtil.getEntityManager()).thenReturn(mockEntityManager);
		when(mockEntityManager.getCriteriaBuilder()).thenReturn(mockCriteriaBuilder);
		when(mockCriteriaBuilder.createQuery(SessionOTI.class)).thenReturn(mockCriteriaQuery);
		when(mockCriteriaQuery.from(SessionOTI.class)).thenReturn(mockRoot);
		when(mockCriteriaQuery.select(mockRoot)).thenReturn(mockCriteriaQuery);
		when(mockCriteriaQuery.where(any(Predicate.class))).thenReturn(mockCriteriaQuery);

		when(mockEntityManager.createQuery(mockCriteriaQuery)).thenReturn(mockQuery);
		when(mockEntityManager.getTransaction()).thenReturn(mock(EntityTransaction.class));

		when(mockQuery.setFirstResult(Mockito.anyInt())).thenReturn(mockQuery);
		when(mockQuery.setMaxResults(Mockito.anyInt())).thenReturn(mockQuery);

        when(callback.prepareCommunicationResponse(any(PrepareCommunicationResponse.class))).thenReturn(Notification.newNotificationOk());
        when(callback.sendScriptResponse(any(SendScriptResponse.class))).thenReturn(Notification.newNotificationOk());
	}

	@Test
	public void authenticateTest() {
		final String passwd = "open cesame";
		final String login = "Alladin";
		final String agentID = "01";

		AuthenticationDTO authDTO = AuthenticationDTO.getDTOBuilderInstance(
		        Arrays.asList(new SEPair("se-id1", SecureElementType.ICCID), new SEPair("se-id2", SecureElementType.ICCID)), agentID,
		        login, passwd).build();

		Predicate mockPredicate = mock(Predicate.class);
		when(mockCriteriaBuilder.and(any(Predicate[].class))).thenReturn(mockPredicate);
		when(mockCriteriaBuilder.or(any(Predicate[].class))).thenReturn(mockPredicate);

		doThrow(NoResultException.class).when(mockQuery).getSingleResult();

		try {
			bean.authenticate(authDTO);
		} catch (OTIAuthenticationException e) {
			assertEquals(e.getFailReason(), AuthenticationFailReason.SESSION_NOT_FOUND);
		}
	}

	@Test
	public void prepareCommunicationRequestTest() {
		List<SEPair> sePairs = Arrays.asList(new SEPair[] { new SEPair(SECURE_ELEMENT_ID, SecureElementType.ICCID) });

		when(mockQuery.getSingleResult()).thenReturn(mockSession);
		when(mockSession.getId()).thenReturn(BigInteger.ONE);
		when(mockSession.getStatus()).thenReturn(SessionStatus.INITIATED);
		when(mockSession.getSessionId()).thenReturn(BigInteger.TEN);
        when(mockSession.getPurgeDate()).thenReturn(new Timestamp(System.currentTimeMillis()));

		try {
			bean.prepareCommunicationRequest(sePairs, AGENT_ID);
		} catch (OTIException e) {
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}

	@Test
	public void porResponseTest() {
		PoRRequestDTO porReq = PoRRequestDTO.getBuilderInstance("21", ScriptStatus.OK, BigInteger.ONE, 1l,
		        Utils.hexStringToByteArray(CONTENT)).build();

		// Test duplicate HTTP session
		HashMap<BigInteger, AgentSessionState> porSessions = new HashMap<BigInteger, AgentSessionState>();
		AgentSessionState porSession = mock(AgentSessionState.class);
		porSessions.put(porReq.getSessionID(), porSession);
		when(agentSession.getPorSessions()).thenReturn(porSessions);

		try {
			bean.porResponse(porReq);
		} catch (OTIException oe) {
			Assert.assertTrue("Duplicate session exception should be thrown", DuplicateStateException.class.isInstance(oe));
			porSessions.clear();
		}

		when(mockSession.getTransactionId()).thenReturn(porReq.getTransactionID());
		when(mockSession.getSessionId()).thenReturn(BigInteger.TEN);

		when(mockQuery.getSingleResult()).thenReturn(mockSession);

		when(agentSession.putPoRSessionState(porReq.getSessionID())).thenReturn(porSession);

		try {
			assertEquals(porReq.getSessionID(), bean.porResponse(porReq));
		} catch (Exception ee) {
			fail(ee.getMessage());
		}

		// Test out-of-session notification
        Preferences.userNodeForPackage(bean.getClass()).put(OTIControllerConfigKeys.OUT_OF_SESSION_NOTIFICATION_EXPECTED.getConfigKey(), "true");

		doThrow(NoResultException.class).when(mockQuery).getSingleResult();

		try {
			bean.porResponse(porReq);
		} catch (MissingSessionException mse) {
		} catch (Exception e) {
			fail(e.getMessage());
		}
		verify(callback, times(1)).outOfSessionResponse(any(OutOfSessionResponse.class));
	}

}
