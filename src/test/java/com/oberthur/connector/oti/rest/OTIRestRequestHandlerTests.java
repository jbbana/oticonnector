package com.oberthur.connector.oti.rest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.oberthur.connector.api.ConnectorService;

@RunWith(MockitoJUnitRunner.class)
public class OTIRestRequestHandlerTests {

	@Mock
	private ConnectorService connectorService;

	@InjectMocks
	private final OTIRestRequestHandler handler = new OTIRestRequestHandler(null);

	@Test
	public void getConnectorServiceTest() {
		Assert.assertNotNull(handler.getConnectorService());
	}
}
