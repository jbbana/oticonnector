package com.oberthur.connector.oti.interceptor;

import com.oberthur.connector.api.SecureElementType;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.service.notification.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import javax.interceptor.InvocationContext;

import java.lang.reflect.Field;
import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Test for sending notifications for the session create
 *
 * @author Michal Kaminski, OT R&D Poland
 */
@Slf4j
public class SessionCreateInterceptorTest {


    private static final String OTI_URL = "http://dummy.com/ram";
    NotificationServerConnector connector = mock(NotificationServerConnector.class);

    @Test
    public void testCreateSession() throws Exception {

        SessionOTI session = aSession();

        InvocationContext mockCtx = mock(InvocationContext.class);
        when(mockCtx.proceed()).thenReturn(session);

        doAnswer(invocation -> {
            log.info("Sending notification with the parameters: seId: {} seType:{} and notification: {}", invocation.getArguments()[0], invocation.getArguments()[1], invocation.getArguments()[2]);
            SessionOTI expect = aSession();
            assertEquals(expect.getSecureElement(), invocation.getArguments()[0].toString());
            assertEquals("uicc", invocation.getArguments()[1].toString());
            Object notification = invocation.getArguments()[2];
            assertTrue(notification instanceof OTIScript);
            assertEquals(OTI_URL, ((OTIScript) notification).getUrl());
            return null;
        }).when(connector).sendNotification(anyString(), anyString(), any(NotificationContent.class));


        prepareInterceptor().createSession(mockCtx);

    }

    private SessionCreateInterceptor prepareInterceptor() throws NoSuchFieldException, IllegalAccessException {
        SessionCreateInterceptor sessionCreateInterceptor = new SessionCreateInterceptor();
        Field notificationService = sessionCreateInterceptor.getClass().getDeclaredField("notificationService");
        notificationService.setAccessible(true);
        notificationService.set(sessionCreateInterceptor, new NotificationServiceBean(connector, OTI_URL));
        return sessionCreateInterceptor;
    }

    private SessionOTI aSession() {
        SessionOTI session = new SessionOTI();
        session.setId(BigInteger.TEN);
        session.setSessionId(BigInteger.ONE);
        session.setSecureElement("0123456789123456789");
        session.setSecureElementType(SecureElementType.ICCID);
        return session;
    }
}
