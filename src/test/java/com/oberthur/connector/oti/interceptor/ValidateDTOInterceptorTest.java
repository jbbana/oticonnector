/**
 * 
 */
package com.oberthur.connector.oti.interceptor;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.interceptor.InvocationContext;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.oberthur.connector.oti.dto.ConnectorDTO;

/**
 * @author Illya Krakovskyy
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ValidateDTOInterceptorTest {

	@InjectMocks
	private ValidateDTOInterceptor interceptor;

	/**
	 * Test method for
	 * {@link com.oberthur.connector.oti.interceptor.ValidateDTOInterceptor#validateDTO(javax.interceptor.InvocationContext)}
	 * .
	 */
	@Test
	public void testValidateDTO() {
		InvocationContext mockCtx = mock(InvocationContext.class);

		when(mockCtx.getMethod()).thenReturn(this.getClass().getMethods()[0]);
		when(mockCtx.getParameters()).thenReturn(new Object[] { new ConnectorDTO() });

		try {
			interceptor.validateDTO(mockCtx);
		} catch (Exception e) {
			assertTrue(
			        String.format("ConstraintViolationException expected, but got %s with message %s", e.getClass().getSimpleName(),
			                e.getMessage()), ConstraintViolationException.class.isInstance(e));
		}
	}
}
