/**
 * 
 */
package com.oberthur.connector.oti.interceptor;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.interceptor.InvocationContext;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.oberthur.connector.api.xml.bind.request.PrepareCommunicationRequest;

/**
 * @author Illya Krakovskyy
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ValidateRequestInterceptorTest {

	@InjectMocks
	private ValidateRequestInterceptor interceptor;

	/**
	 * Test method for
	 * {@link com.oberthur.connector.oti.interceptor.ValidateRequestInterceptor#validateRequest(javax.interceptor.InvocationContext)}
	 * .
	 */
	@Test
	public void testValidateRequest() {
		InvocationContext mockCtx = mock(InvocationContext.class);

		when(mockCtx.getMethod()).thenReturn(this.getClass().getMethods()[0]);
		when(mockCtx.getParameters()).thenReturn(new Object[] { new PrepareCommunicationRequest() });

		try {
			interceptor.validateRequest(mockCtx);
		} catch (Exception e) {
			assertTrue(
			        String.format("ConstraintViolationException expected, but got %s with message %s", e.getClass().getSimpleName(),
			                e.getMessage()), ConstraintViolationException.class.isInstance(e));
		}
	}

}
