/**
 * 
 */
package com.oberthur.connector.oti.interceptor;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.interceptor.InvocationContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * @author Illya Krakovskyy
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class MethodLoggerTest {

	@InjectMocks
	private MethodLogger interceptor;

	/**
	 * Test method for
	 * {@link com.oberthur.connector.oti.interceptor.MethodLogger#aroundInvoke(javax.interceptor.InvocationContext)}
	 * .
	 */
	@Test
	public void testAroundInvoke() {

		InvocationContext mockCtx = mock(InvocationContext.class);

		when(mockCtx.getMethod()).thenReturn(this.getClass().getMethods()[0]);
		try {
			interceptor.aroundInvoke(mockCtx);
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}

}
