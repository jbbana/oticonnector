package com.oberthur.connector.oti.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.oberthur.connector.api.SecureElementType;
import com.oberthur.connector.api.xml.bind.notification.Notification;
import com.oberthur.connector.api.xml.bind.request.PrepareCommunicationRequest;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.dto.ConnectorDTO;
import com.oberthur.connector.oti.service.bean.OTIServiceBean;

@RunWith(MockitoJUnitRunner.class)
public class OTIConnectorServiceBeanTest {

	@InjectMocks
	private OTIConnectorServiceBean bean;

	@Mock
	private OTIServiceBean otiService;


	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPrepareCommunicationRequest() {
		PrepareCommunicationRequest mockRequest = mock(PrepareCommunicationRequest.class);
		when(mockRequest.getTargetIdentifier()).thenReturn("a000000151000000");
		when(mockRequest.getSessionID()).thenReturn("1234");
		when(mockRequest.getSecureElement()).thenReturn("24");
		when(mockRequest.getAgentId()).thenReturn("01");
		when(mockRequest.getMaxResponseDelay()).thenReturn(Integer.valueOf(1000));

        when(otiService.isSessionExist(Mockito.anyString(), any(SecureElementType.class))).thenReturn(Boolean.FALSE);

        SessionOTI session = new SessionOTI();
        session.setSessionId(new BigInteger("1234"));
        when(otiService.prepareCommunicationInitiate(any(ConnectorDTO.class))).thenReturn(session);

		assertEquals(Notification.Status.OK, bean.prepareCommunicationRequest(mockRequest).getStatus());
	}
}
