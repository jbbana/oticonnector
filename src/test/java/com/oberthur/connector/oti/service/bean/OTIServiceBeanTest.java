package com.oberthur.connector.oti.service.bean;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.oberthur.apdugenerator.utils.Utils;
import com.oberthur.connector.api.SecureElementType;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.dto.ConnectorDTO;
import com.oberthur.connector.oti.dto.SendScriptDTO;
import com.oberthur.connector.oti.exceptions.MissingSessionException;
import com.oberthur.connector.oti.http.session.OTIAgentSession;
import com.oberthur.connector.oti.ram.state.AgentSessionState;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;
import com.oberthur.connector.oti.service.startup.purge.PurgeSessionsScheduler;

@RunWith(MockitoJUnitRunner.class)
public class OTIServiceBeanTest {

    private static final String SECURE_ELEMENT_ID = "4562323FE2142A";
    @InjectMocks
    private OTIServiceBean bean;

    @Mock
    private OTIPersistenceUtil persistenceUtil;

    @Mock
    private OTIAgentSession agentSession;

    @Mock
    private CriteriaBuilder mockCriteriaBuilder;

    @Mock
    private CriteriaQuery<SessionOTI> mockCriteriaQuery;

    @Mock
    private TypedQuery<SessionOTI> mockQuery;
    @Mock
    private Root<SessionOTI> mockRoot;

    @Mock
    private EntityManager mockEntityManager;

    @Mock
    private SessionOTI mockSession;

    @Mock
    private PurgeSessionsScheduler purgeScheduler;

    private Validator validator;

    @Before
    public void setUp() throws Exception {
        when(persistenceUtil.getEntityManager()).thenReturn(mockEntityManager);
        when(mockEntityManager.getCriteriaBuilder()).thenReturn(mockCriteriaBuilder);
        when(mockCriteriaBuilder.createQuery(SessionOTI.class)).thenReturn(mockCriteriaQuery);
        when(mockCriteriaQuery.from(SessionOTI.class)).thenReturn(mockRoot);
        when(mockCriteriaQuery.select(mockRoot)).thenReturn(mockCriteriaQuery);
        when(mockCriteriaQuery.where(any(Predicate.class))).thenReturn(mockCriteriaQuery);

        when(mockEntityManager.createQuery(mockCriteriaQuery)).thenReturn(mockQuery);
        when(mockEntityManager.getTransaction()).thenReturn(mock(EntityTransaction.class));

        when(mockQuery.setFirstResult(Mockito.anyInt())).thenReturn(mockQuery);
        when(mockQuery.setMaxResults(Mockito.anyInt())).thenReturn(mockQuery);

        when(mockQuery.getSingleResult()).thenReturn(mockSession);
        when(mockSession.getId()).thenReturn(BigInteger.ONE);
        when(mockSession.getSessionId()).thenReturn(BigInteger.TEN);

        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        validator = vf.getValidator();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void isSessionExistTest() {
        CriteriaQuery<Long> mockLongCQ = mock(CriteriaQuery.class);

        when(mockCriteriaBuilder.createQuery(Long.class)).thenReturn(mockLongCQ);
        when(mockLongCQ.from(SessionOTI.class)).thenReturn(mockRoot);

        when(mockLongCQ.select(any(Expression.class))).thenReturn(mockLongCQ);
        when(mockLongCQ.where(any(Predicate.class))).thenReturn(mockLongCQ);

        TypedQuery<Long> mockLongQuery = mock(TypedQuery.class);
        when(mockEntityManager.createQuery(mockLongCQ)).thenReturn(mockLongQuery);
        when(mockLongQuery.getSingleResult()).thenReturn(BigInteger.ZERO.longValue());

        bean.isSessionExist(SECURE_ELEMENT_ID, SecureElementType.ICCID);
    }

    @Test
    public void prepareCommunicationInitiateTest() {
        // Check validation
        Set<ConstraintViolation<ConnectorDTO>> violations = validator.validate(ConnectorDTO.getBuilderSessionInstance(null, null, null, null, null, null, null).build());
        assertTrue("ConnectorDTO Bean validation failed", violations.size() > 0);

        ConnectorDTO dto = ConnectorDTO.getBuilderSessionInstance("1234", SECURE_ELEMENT_ID, SecureElementType.ICCID, Integer.valueOf(BigInteger.TEN.intValue()), "01", "login",
                "password").build();
        bean.prepareCommunicationInitiate(dto);
    }

    @Test
    public void sendScriptResponseTest() {
        // Check constraints
        Set<ConstraintViolation<SendScriptDTO>> violations = validator.validate(SendScriptDTO.getBuilderInstance(null, null, null, null, null).build());
        assertTrue("SendScriptDTO Bean validation doesn't work", violations.size() > 0);

        final String script = "AE80220580CA0042000000";
        String target = "a000000151000000";

        SendScriptDTO sendScriptDTO = SendScriptDTO.getBuilderInstance(BigInteger.ONE, Integer.valueOf(2000), Long.valueOf(12345), target,
                Arrays.asList(Utils.hexStringToByteArray(script))).build();

        Preferences.userNodeForPackage(bean.getClass()).put(OTIServiceConfigKeys.DEFAULT_VALIDITY_PERIOD.getConfigKey(), "100");

        // Check no HTTP session
        try {
            bean.sendScriptResponse(sendScriptDTO);
        } catch (MissingSessionException mse) {
            assertTrue("Expecting no HTTP session exist", MissingSessionException.MissingExceptionReason.HTTP_SESSION_MISSED == mse.getReason());
        }

        // Normal flow
        Map<BigInteger, AgentSessionState> porSessions = new HashMap<BigInteger, AgentSessionState>();
        porSessions.put(BigInteger.ONE, new AgentSessionState());
        when(agentSession.agentSessionStateExists(any(BigInteger.class))).thenReturn(true);

        try {
            AgentSessionState mockAgentSessionState = mock(AgentSessionState.class);
            when(agentSession.getAgentSessionState(any(BigInteger.class))).thenReturn(mockAgentSessionState);
            when(agentSession.pollForSentState(any(BigInteger.class))).thenReturn(mockAgentSessionState);

            bean.sendScriptResponse(sendScriptDTO);
        } catch (MissingSessionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void dropCommunicationTest() {
        try {
            bean.dropCommunication(BigInteger.ONE, BigInteger.ONE.toString());
        } catch (MissingSessionException e) {
            fail(e.getMessage());
        }
    }
}
