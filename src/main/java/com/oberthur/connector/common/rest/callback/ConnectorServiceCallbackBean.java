package com.oberthur.connector.common.rest.callback;

import static com.oberthur.connector.common.util.ConnectorKey.SEQUENCER_CALLBACK_KEY;
import static com.oberthur.connector.common.util.ConnectorKey.SEQUENCER_OUT_OF_SESSION_PATH;
import static com.oberthur.connector.common.util.ConnectorKey.SEQUENCER_PREPARE_COMMUNICATION_PATH;
import static com.oberthur.connector.common.util.ConnectorKey.SEQUENCER_SEND_SCRIPT_PATH;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import lombok.extern.slf4j.Slf4j;

import com.oberthur.connector.api.ConnectorServiceCallback;
import com.oberthur.connector.api.xml.bind.notification.Notification;
import com.oberthur.connector.api.xml.bind.response.OutOfSessionResponse;
import com.oberthur.connector.api.xml.bind.response.PrepareCommunicationResponse;
import com.oberthur.connector.api.xml.bind.response.SendScriptResponse;
import com.oberthur.connector.common.util.PropertiesUtils;

@Slf4j
public class ConnectorServiceCallbackBean implements ConnectorServiceCallback {

    private static final String SEQUENCER_CALLBACK_URL = PropertiesUtils.getValue(SEQUENCER_CALLBACK_KEY);
    @Inject
    private ExecutorService executorService;

    @Override
    public Notification prepareCommunicationResponse(PrepareCommunicationResponse response) {
        return sendCallBack(response, SEQUENCER_PREPARE_COMMUNICATION_PATH);
    }

    @Override
    public Notification outOfSessionResponse(OutOfSessionResponse response) {
        return sendCallBack(response, SEQUENCER_OUT_OF_SESSION_PATH);
    }

    @Override
    public Notification sendScriptResponse(final SendScriptResponse response) {
        try {
            Future<Notification> future = executorService.submit(new Callable<Notification>() {

                @Override
                public Notification call() throws Exception {
                    return sendCallBack(response, SEQUENCER_SEND_SCRIPT_PATH);
                }
            });
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            return Notification.newNotificationFail(e.getMessage());
        }
    }

    /**
     * Send async response using JAX-RS Client
     * 
     * @param targetUri
     * @param requestParams
     * @return
     */
    public static <E> Notification sendAsyncResponse(String targetUri, E requestParams) {
        log.info("Sending REST async response to sequencer to targetUri = {}", targetUri);

        Client client = ClientBuilder.newClient();
        try {
            WebTarget target = client.target(SEQUENCER_CALLBACK_URL + targetUri);
            Notification response = target.request().accept(MediaType.APPLICATION_JSON).buildPost(Entity.entity(requestParams, MediaType.APPLICATION_JSON))
                    .invoke(Notification.class);
            return response;
        } catch (Exception e) {
            String failMsg = e.getMessage();
            log.error(String.format("Async response failed: %s", failMsg), e);
            return Notification.newNotificationFail(failMsg);
        } finally {
            client.close();
        }
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }

    private <E> Notification sendCallBack(E response, String targetUri) {

        Notification notification = sendAsyncResponse(targetUri, response);

        if (log.isDebugEnabled()) {
            log.info("Sync response from sequencer received. Status: {} ; \n\tReason: {}", notification.getStatus(), notification.getReason());
        }

        return notification;
    }
}
