package com.oberthur.connector.common.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath(AbstractRestRequestHandler.APPLICATION_PATH)
public class RestActivator extends Application {
}
