package com.oberthur.connector.common.rest;

import static com.oberthur.connector.api.xml.bind.notification.Notification.newNotificationFail;
import static com.oberthur.connector.common.rest.SEConnectorContextPath.DROP_COMMUNICATION;
import static com.oberthur.connector.common.rest.SEConnectorContextPath.PREPARE_COMMUNICATION;
import static com.oberthur.connector.common.rest.SEConnectorContextPath.SEND_SCRIPT;
import static com.oberthur.connector.common.rest.SEConnectorContextPath.SEND_TEXT;

import java.math.BigInteger;

import javax.enterprise.context.RequestScoped;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.DatatypeConverter;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;

import com.oberthur.connector.api.ConnectorService;
import com.oberthur.connector.api.xml.bind.notification.Notification;
import com.oberthur.connector.api.xml.bind.notification.ResponseCode;
import com.oberthur.connector.api.xml.bind.request.DropCommunicationRequest;
import com.oberthur.connector.api.xml.bind.request.PrepareCommunicationRequest;
import com.oberthur.connector.api.xml.bind.request.SendSMSRawScriptRequest;
import com.oberthur.connector.api.xml.bind.request.SendScriptRequest;
import com.oberthur.connector.api.xml.bind.request.SendTextRequest;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Slf4j
@RequestScoped
@Api(value = AbstractRestRequestHandler.APPLICATION_PATH + AbstractRestRequestHandler.ADDITIONAL_PATH, description = "Connector's REST API", produces = "http", position = 1)
public abstract class AbstractRestRequestHandler {

	private enum OperationSign {
		SEND_SCRIPT("SendScript"),
		PREPARE_COMMUNICATION("PrepareCommunication"),
		DROP_COMMUNICATION("DropCommunication");

		private String sign;

		private OperationSign(String sign) {
			this.sign = sign;
		}

		public String getSign() {
			return this.sign;
		}
	}

	public static final String APPLICATION_PATH = "rest";
	public static final String ADDITIONAL_PATH = "";

	protected abstract ConnectorService getConnectorService();

	@POST
	@Path(SEND_SCRIPT)
	@ApiOperation(value = "Send Script", response = Notification.class, position = 2)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Notification sendScript(@ApiParam(value = "SendScriptRequest object", required = true) SendScriptRequest request) {
		log.info("REST send script request received");

		log.info("Send Script Request:\n " + "sessionId: " + request.getConversationId() + "\npartnerOid: " + request.getPartnerOid() + "\ntransactionId: "
		        + request.getTransactionId() + "\ntarget: " + request.getTarget() + "\nScript\n: "
		        + (request.getScript() != null && !request.getScript().isEmpty() ? DatatypeConverter.printHexBinary(request.getScript().get(0)) : ""));

		try {
			return getConnectorService().sendScriptRequest(request);
		} catch (ConstraintViolationException cve) {
			return handleRequestValidationError(cve, OperationSign.SEND_SCRIPT);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return newNotificationFail("Send script execution failed, message is: " + e.toString());
		}
	}

	@POST
	@Path(PREPARE_COMMUNICATION)
	@ApiOperation(value = "Prepare Communication", response = Notification.class, position = 3)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Notification prepareCommunicationRequest(@ApiParam(value = "PrepareCommunicationRequest object", required = true) PrepareCommunicationRequest request) {
		log.info("REST prepare communication request received");

		try {
			log.info("Prepare Communication Request:\n" + "sessionID: " + request.getSessionID() + "\ntargetIdentifier: " + request.getTargetIdentifier() + "\npartnerOID: "
			        + request.getPartnerOID() + "\nsecureElement: " + request.getSecureElement() + "\nserviceId: " + request.getServiceId() + "\nestimatedAPDUScriptSize: "
			        + request.getEstimatedAPDUScriptSize() + "\nrequestedScriptFormat: " + request.getRequestedScriptFormat());

			return getConnectorService().prepareCommunicationRequest(request);
		} catch (ConstraintViolationException cve) {
			return handleRequestValidationError(cve, OperationSign.PREPARE_COMMUNICATION);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return newNotificationFail("Prepare communication execution failed, message is: " + e.toString());
		}
	}

	@POST
	@Path(DROP_COMMUNICATION)
	@ApiOperation(value = "Drop Communication", response = Notification.class, position = 4)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Notification dropCommunication(@ApiParam(value = "DropCommunicationRequest object", required = true) DropCommunicationRequest request) {
		BigInteger sessionId = request.getConversationId();
		String partnerOID = request.getPartnerOID();

		if (log.isDebugEnabled()) {
			log.debug("REST drop communication request received:\n\tpartnerOID = {}\n\tseCSessionId = {}", partnerOID, sessionId);
		}
		try {
			return getConnectorService().disconnect(sessionId, partnerOID);
		} catch (ConstraintViolationException cve) {
			return handleRequestValidationError(cve, OperationSign.DROP_COMMUNICATION);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return newNotificationFail("Drop communication execution failed, message is: " + e.toString());
		}
	}

	@POST
	@Path(SEND_TEXT)
	@ApiOperation(value = "Send Text", response = Notification.class, position = 5)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Notification sendText(@ApiParam(value = "SendTextRequest object", required = true) SendTextRequest request) {
		if (log.isDebugEnabled()) {
			log.info("SendTextRequest:\n" + "\tconversationId : {}\n" + "\tmobileSubscription : {}\n" + "\ttext: {}\n" + "\ttransactionID: {}\n" + "\tpid: {}\n" + "\tdcs : {}\n"
			        + "\taskNetworkAck : {}\n" + "\tvp : {}\n",

			request.getConversationId(), request.getMobileSubscription(), request.getText() != null ? DatatypeConverter.printHexBinary(request.getText()) : StringUtils.EMPTY,
			        request.getTransactionID(), request.getPid(), request.getDcs(), request.getAskNetworkAck(), request.getVp());
		}

		throw new RuntimeException("Not implemented yet");
	}

	@POST
	@Path(SEConnectorContextPath.SEND_RAW_SMS_SCRIPT)
	@ApiOperation(value = "Send RAW SMS Script", response = Notification.class, position = 5)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Notification sendSMSRawScript(SendSMSRawScriptRequest request) {
		log.info(
		        "SendSMSRawScriptRequest:\n\tconversationId : {}\n\tmobileSubscription : {}\n\ttransactionId : {}\n\tudhi : {}\n\tdata : {}\n\tpid : {}\n\tdcs : {}\n\taskNetworkAck : {}\n\tvp : {}\n\tissuerOID",
		        request.getConversationId(), request.getMobileSubscription(), request.getTransactionId(), request.getUdhi(), (request.getData() != null
		                && request.getData().length > 0 ? DatatypeConverter.printHexBinary(request.getData()) : ""), request.getPid(), request.getDcs(),
		        request.getAskNetworkAck(), request.getVp(), request.getIssuerOID());
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * @param cve
	 * @param opSign
	 *            TODO
	 * @return
	 */
	private Notification handleRequestValidationError(ConstraintViolationException cve, OperationSign opSign) {
		String firstViolationMsg = cve.getConstraintViolations().iterator().next().getMessage();
		String validationMsg = String.format("%s execution failed by validation: %s", opSign.getSign(), firstViolationMsg);
		log.error(firstViolationMsg);
		return newNotificationFail(validationMsg, ResponseCode.INVALID_ARGUMENT);
	}

}