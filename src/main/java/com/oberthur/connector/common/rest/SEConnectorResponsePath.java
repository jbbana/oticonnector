package com.oberthur.connector.common.rest;

public interface SEConnectorResponsePath {

	String SLASH = "/";

	String PREPARE_COMMUNICATION_RESPONSE = SLASH + "prepareCommunicationResponse";
	String SEND_SCRIPT_RESPONSE = SLASH + "sendScriptResponse";
	String OUT_OF_SESSION_RESPONSE = SLASH + "outOfSessionResponse";
	String ADDITIONAL_PATH = SLASH + "connectorCallback";
	String SEND_TEXT_PATH = SLASH + "sendText";


	String APPLICATION_PATH = "rest";
}
