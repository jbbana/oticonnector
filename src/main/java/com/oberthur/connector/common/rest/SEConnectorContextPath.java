package com.oberthur.connector.common.rest;

public interface SEConnectorContextPath {

	String PREPARE_COMMUNICATION = "/prepareCommunication";
	String SEND_SCRIPT = "/sendScript";
	String DROP_COMMUNICATION = "/dropCommunication";

	String SEND_TEXT = "/sendText";
	String SEND_RAW_SMS_SCRIPT = "/sendRawSMSScript";
	String OOSR_RESPONSE = "/outOfSession";
}
