package com.oberthur.connector.common.asynchronous;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorServiceProducer {
    @Produces
    @ApplicationScoped
    public ExecutorService createExecutorService() {
        return Executors.newFixedThreadPool(4);
    }

    public void close(@Disposes final ExecutorService executorService) {
        executorService.shutdown();
    }

}
