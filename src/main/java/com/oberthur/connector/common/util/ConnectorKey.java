package com.oberthur.connector.common.util;

import static com.oberthur.connector.common.rest.SEConnectorResponsePath.ADDITIONAL_PATH;
import static com.oberthur.connector.common.rest.SEConnectorResponsePath.OUT_OF_SESSION_RESPONSE;
import static com.oberthur.connector.common.rest.SEConnectorResponsePath.PREPARE_COMMUNICATION_RESPONSE;
import static com.oberthur.connector.common.rest.SEConnectorResponsePath.SEND_SCRIPT_RESPONSE;

public interface ConnectorKey {

	String SEQUENCER_CALLBACK_KEY = "sequencer.callback.url";

	String SEQUENCER_SEND_SCRIPT_PATH = ADDITIONAL_PATH + SEND_SCRIPT_RESPONSE;
	String SEQUENCER_PREPARE_COMMUNICATION_PATH = ADDITIONAL_PATH + PREPARE_COMMUNICATION_RESPONSE;
	String SEQUENCER_OUT_OF_SESSION_PATH = ADDITIONAL_PATH + OUT_OF_SESSION_RESPONSE;

	//PREPARE_COMMUNICATION
	String PREPARE_COMMUNICATION_CONVERSATIONID = "prepare.communication.conversationId";
	String PREPARE_COMMUNICATION_APDU_SCRIPT_SIZE = "prepare.communication.apdu.script.size";
	String PREPARE_COMMUNICATION_APDUS_NUMBER = "prepare.communication.apdus.number";
	String PREPARE_COMMUNICATION_SCRIPT_FORMAT = "prepare.communication.script.format";
	String PREPARE_COMMUNICATION_APDURESPONSESCRIPTSIZE = "prepare.communication.apduResponseScriptSize";
	String PREPARE_COMMUNICATION_TIMETOLIVE = "prepare.communication.timeToLive";

	// SEND SCRIPT

	String CARD_READER_SEND_SCRIPT_RETURNED_STATUS_CODE = "card.reader.send.script.returned.status.code";

	String GP_PREFIX_TO = "gp.prefix.to";
	String GP_PREFIX_FROM_ADDRESS = "gp.prefix.from.address";
	String GP_PREFIX_MESSAGE_ID = "gp.prefix.message_id";
}
