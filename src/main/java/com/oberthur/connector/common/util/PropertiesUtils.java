package com.oberthur.connector.common.util;

import java.util.Properties;

public final class PropertiesUtils {

    private static final String PROPERTY_CONFIG_FILE = "configuration.properties";
    private static ClassLoader loader = Thread.currentThread().getContextClassLoader();
    private static final Properties properties = getProperties();

    private PropertiesUtils() {
    }

    private static Properties getProperties() {
        Properties configuration = new Properties();
        try {
            configuration.load(loader.getResourceAsStream(PROPERTY_CONFIG_FILE));
        } catch (Exception e) {
           //
        }
        return configuration;
    }

    public static String getValue(String key) {
        String value = System.getProperty(key);
        return value != null ? value : properties.getProperty(key);
    }
}
