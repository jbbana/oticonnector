/**
 * 
 */
package com.oberthur.connector.oti.exceptions;

import lombok.Getter;

/**
 * @author Illya Krakovskyy
 * 
 */
public class OTIAuthenticationException extends OTIException {

	public enum AuthenticationFailReason {
		NO_LOGIN_FOUND("Login not found"),
		EMPTY_LOGIN("No login specified"),
		EMPTY_PASSWD("No password specified"),
		LOGIN_PASSWD_MISMATCH("Login does not match the password"),
		SESSION_NOT_FOUND("No OTI session found to authenticate against"),
		NO_AUTH_DATA("No authentication data specified"),
		SESSION_TO_BE_PURGED("Session marked to be purged");

		private String reason;

		private AuthenticationFailReason(String reason) {
			this.reason = reason;
		}

        public String getReason() {
            return reason;
        }
	}

	/**
	 * Authentication failure reason
	 */
	@Getter
	private final AuthenticationFailReason failReason;

	/**
	 * 
	 */
	private static final long serialVersionUID = -1221847483229515878L;

	/**
	 * @param failReason
	 */
	public OTIAuthenticationException(AuthenticationFailReason failReason) {
        super(failReason.getReason());
		this.failReason = failReason;
	}

}
