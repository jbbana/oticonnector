package com.oberthur.connector.oti.exceptions;


public class OTIException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3059076993819020697L;

	public OTIException() {
		super();
	}

	public OTIException(String message) {
        super(message);
	}

    public OTIException(Throwable cause) {
		super(cause);
	}

	protected OTIException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
