package com.oberthur.connector.oti.exceptions;

public class MissingSessionException extends OTIException {

    public enum MissingExceptionReason {
        HTTP_SESSION_MISSED,
        SESSION_MISSED,
        SESSION_TIMED_OUT,
        NO_SESSION_FOR_SE_IDS;
    }

    /**
     * 
     */
    private static final long serialVersionUID = 2594827536902008273L;

    /**
     * 
     */
    private MissingExceptionReason reason;

    /**
     * @param message
     * @param reason
     */
    public MissingSessionException(String message, MissingExceptionReason reason) {
        super(message);
        this.reason = reason;
    }

    /**
     * @return the reason
     */
    public MissingExceptionReason getReason() {
        return reason;
    }
}
