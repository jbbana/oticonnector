/**
 * 
 */
package com.oberthur.connector.oti.exceptions;

/**
 * @author Illya Krakovskyy
 *
 */
public class SendAsyncResponseException extends OTIException {

    /**
     * 
     */
    private static final long serialVersionUID = 6582407484872214479L;

    /**
     * @author Illya Krakovskyy
     *
     */
    public enum CallbackInterfaceFailed {
        PREPARE_COMMUNICATION("prepareCommunicationResponse"),
        SEND_SCRIPT("sendScriptResponse"),
        OUT_OF_SESSION("outOfSessionResponse");
        private String interfaceName;

        /**
         * @param interfaceName
         */
        private CallbackInterfaceFailed(String interfaceName) {
            this.interfaceName = interfaceName;
        }

        /**
         * @return the interfaceName
         */
        public String getInterfaceName() {
            return interfaceName;
        }
    }

    /**
     * The interface failed
     */
    private CallbackInterfaceFailed interfaceFailed;

    /**
     * @return the interfaceFailed
     */
    public CallbackInterfaceFailed getInterfaceFailed() {
        return interfaceFailed;
    }

    /**
     * @param interfaceFailed
     * @param faileReason
     */
    public SendAsyncResponseException(CallbackInterfaceFailed interfaceFailed, String faileReason) {
        super(String.format("Sending async response to interface %s failed: %s", interfaceFailed.getInterfaceName()));
        this.interfaceFailed = interfaceFailed;
    }

}
