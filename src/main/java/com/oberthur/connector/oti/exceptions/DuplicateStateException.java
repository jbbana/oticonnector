/**
 * 
 */
package com.oberthur.connector.oti.exceptions;

/**
 * @author Illya Krakovskyy
 * 
 */
public class DuplicateStateException extends OTIException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5303186037654374957L;

	/**
	 * 
	 */
	public DuplicateStateException() {
	}

	/**
	 * @param message
	 * @param logError
	 */
	public DuplicateStateException(String message) {
        super(message);
	}

	/**
	 * @param cause
	 * @param logError
	 */
	public DuplicateStateException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 * @param logError
	 */
	public DuplicateStateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace,
	        boolean logError) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
