/**
 * 
 */
package com.oberthur.connector.oti.exceptions;

/**
 * @author Illya Krakovskyy
 *
 */
public class ConfigurationMissedException extends OTIException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 676505211984448287L;

    /**
     * Configuration parameters missed in OTIC configuration
     */
    private String[] missedConfigParams;

	/**
	 * 
	 */
	public ConfigurationMissedException() {
		super();
	}

    /**
     * @param missedConfigParams
     *            Configuration parameters missed in OTIC configuration
     */
    public ConfigurationMissedException(String... missedConfigParams) {
        super();
        this.missedConfigParams = missedConfigParams;
    }

    /**
     * @return the missedConfigParams
     */
    public String[] getMissedConfigParams() {
        return missedConfigParams;
    }

}
