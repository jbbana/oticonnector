/**
 *
 */
package com.oberthur.connector.oti.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;

/**
 * @author Illya Krakovskyy
 */
@Getter
public class AuthenticationDTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4736281825752592613L;

	/**
	 * @author Illya Krakovskyy
	 *         <p/>
	 *         DTO builder
	 */
	public static class DTOBuilder {
		/**
		 * DTO to build
		 */
		private final AuthenticationDTO built;

		/**
		 *
		 */
		public DTOBuilder() {
			built = new AuthenticationDTO();
		}

		/**
		 * Constructor populating the build
		 * 
		 * @param sePairs
		 * @param agentID
		 * @param login
		 * @param passwd
		 */
		public DTOBuilder(List<SEPair> sePairs, String agentID, String login, String passwd) {
			this();
			built.sePairs = sePairs;
			built.agentID = agentID;
			built.login = login;
			built.passwd = passwd;
		}

		public AuthenticationDTO build() {
			return built;
		}

	}

	/**
	 * List of pairs SE ID;SE ID Type
	 */
	private List<SEPair> sePairs;
	/**
	 * Agent ID
	 */
	private String agentID;
	/**
	 * HTTP login
	 */
	private String login;
	/**
	 * HTTP password
	 */
	private String passwd;

	/**
	 * Static builder
	 * 
	 * @param secureElementID
	 * @param secureElementType
	 * @param agentID
	 * @param login
	 * @param passwd
	 * @return
	 */
	public static DTOBuilder getDTOBuilderInstance(List<SEPair> sePairs, String agentID, String login, String passwd) {
		return new DTOBuilder(sePairs, agentID, login, passwd);
	}
}
