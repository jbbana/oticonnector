/**
 * 
 */
package com.oberthur.connector.oti.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;

/**
 * @author Illya Krakovskyy
 * 
 *         DTO holding data for Google Cloud Messageing
 * 
 */
@Getter
public class GoogleMessageDTO implements Serializable {

    /**
     * @author Illya Krakovskyy
     * 
     *         DTO builder class
     * 
     */
    public static class DTOBuilder {
        /**
         * DTO instance to build
         */
        private GoogleMessageDTO built;

        /**
         * DTO Builder
         * @param gcmAPIKey
         *            see {@link GoogleMessageDTO#gcmAPIKey}
         * @param gcmRegistrationID
         *            see {@link GoogleMessageDTO#gcmRegistrationID}
         * @param agentId see {@link GoogleMessageDTO#agentId}
         * @param httpsLogin
         *            see {@link GoogleMessageDTO#httpsLogin}
         * @param httpsPassword
         *            see {@link GoogleMessageDTO#httpsPassword}
         */
        public DTOBuilder(String gcmAPIKey, String gcmRegistrationID, String agentId, String httpsLogin, String httpsPassword) {
            this();
            built.gcmAPIKey = gcmAPIKey;
            built.gcmRegistrationID = gcmRegistrationID;
            built.agentId = agentId;
            built.httpsLogin = httpsLogin;
            built.httpsPassword = httpsPassword;
        }

        /**
         * Default builder
         */
        public DTOBuilder() {
            built = new GoogleMessageDTO();
        }

        /**
         * DTO build
         * 
         * @return DTO instance built
         */
        public GoogleMessageDTO build() {
            return built;
        }
    }

    /**
     * Serialization facility ID
     */
    private static final long serialVersionUID = 3462861875862840011L;

    /**
     * GCM API Key
     */
    private String gcmAPIKey;
    /**
     * GCM Registration ID
     */
    private String gcmRegistrationID;
    /**
     * Agent ID
     */
    private String agentId;
    /**
	 * 
	 */
    private String httpsLogin;
    /**
	 * 
	 */
    private String httpsPassword;

    /**
     * Static builder
     * @param gcmAPIKey
     *            see {@link GoogleMessageDTO#gcmAPIKey}
     * @param gcmRegistrationID
     *            see {@link GoogleMessageDTO#gcmRegistrationID}
     * @param agentId TODO
     * @param httpsLogin
     *            see {@link GoogleMessageDTO#httpsLogin}
     * @param httpsPassword
     *            see {@link GoogleMessageDTO#httpsPassword}
     * @return builder instance
     */
    public static DTOBuilder getDTOBuilderInstance(@NotNull @Size(min = 1) String gcmAPIKey, @NotNull @Size(min = 1) String gcmRegistrationID,
                                                   String agentId,
                                                   String httpsLogin,
                                                   String httpsPassword) {
        return new DTOBuilder(gcmAPIKey, gcmRegistrationID, agentId, httpsLogin, httpsPassword);

    }
}
