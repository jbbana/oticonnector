package com.oberthur.connector.oti.dto;

public enum SessionStatus {
    INITIATED,
    OPENED,
    TO_BE_PURGED
}
