package com.oberthur.connector.oti.dto;

import lombok.Getter;

import com.oberthur.connector.api.SecureElementType;

@Getter
public class SEPair {

	/**
	 * @param seID
	 * @param seIDType
	 */
	public SEPair(String seID, SecureElementType seIDType) {
		this.seID = seID;
		this.seIDType = seIDType;
	}

	/**
	 * Secure element ID
	 */
	private final String seID;
	/**
	 * Secure element ID type
	 */
	private final SecureElementType seIDType;
}