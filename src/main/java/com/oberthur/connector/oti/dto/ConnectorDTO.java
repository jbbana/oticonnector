package com.oberthur.connector.oti.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;

import org.apache.commons.lang3.ArrayUtils;

import com.oberthur.connector.api.SecureElementType;

@Getter
public class ConnectorDTO {

	/**
	 * 
	 */
	@NotNull(message = "Session id is null")
	@Size(min = 1, message = "Session id is empty")
	private String sessionId;
	/**
	 * 
	 */
	private Long transactionId;
	/**
	 * 
	 */
	@NotNull(message = "Purge Time is mandatory")
	private Integer purgeTime;
	/**
	 * 
	 */
	@NotNull(message = "Agent ID is mandatory")
	private String agentId;
	/**
	 * 
	 */
	@NotNull(message = "Secure elementd is mandatory")
	private SecureElementType secureElementType;
	/**
	 * 
	 */
	private String login;
	/**
	 * 
	 */
	private String password;

	/**
	 * 
	 */
	private byte[] aid;
	/**
	 * 
	 */
	private String targetSE;
	/**
	 * 
	 */
	private byte[] script;

	public static Builder getBuilderSessionInstance(String sessionID,
	                                                String secureElement,
	                                                SecureElementType secureElementType,
	                                                Integer purgeTime,
	                                                String agentId,
	                                                String login,
	                                                String password) {
		return new Builder(sessionID, secureElement, secureElementType, purgeTime, agentId, login, password);
	}

	/**
	 * A Builder class used to create new ConnectorDTO object.
	 */
	public static class Builder {
		private ConnectorDTO built;

		public Builder() {
			built = new ConnectorDTO();
		}

		public Builder(String targetSE, byte[] aid, byte[] script) {
			this();
			built.targetSE = targetSE;
			built.aid = ArrayUtils.clone(aid);
			built.script = ArrayUtils.clone(script);
		}

		public Builder(String sessionId, String secureElement, SecureElementType secureElementType, Integer purgeTime, String agentId,
		        String login, String password) {
			this();
			built.sessionId = sessionId;
			built.targetSE = secureElement;
			built.purgeTime = purgeTime;
			built.agentId = agentId;
			built.secureElementType = secureElementType;
			built.login = login;
			built.password = password;
		}

		/**
		 * Builds the new ConnectorDTO object
		 *
		 * @return - The created ConnectorDTO object
		 */
		public ConnectorDTO build() {
			return built;
		}
	}

}
