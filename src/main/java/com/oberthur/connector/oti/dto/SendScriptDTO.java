/**
 * 
 */
package com.oberthur.connector.oti.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Illya Krakovskyy
 *
 */
public class SendScriptDTO implements Serializable {

	public static class DTOBuilder {

		/**
		 * 
		 */
		private SendScriptDTO built;

		/**
		 * 
		 */
		public DTOBuilder() {
			built = new SendScriptDTO();
		}

		/**
		 * @param conversationId
		 * @param validityPeriod
		 * @param transactionId
		 * @param target
		 * @param script
		 */
		public DTOBuilder(BigInteger conversationId, Integer validityPeriod, Long transactionId, String target, List<byte[]> script) {
			this();
			built.conversationId = conversationId;
			built.validityPeriod = validityPeriod;
			built.transactionId = transactionId;
			built.target = target;
			built.script = script;
		}

		/**
		 * @return
		 */
		public SendScriptDTO build() {
			return built;
		}

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3952416994808666624L;

	/**
	 * 
	 */
	@NotNull(message = "Conversation Id is mandatory")
	private BigInteger conversationId;
	/**
	 * 
	 */
	private Integer validityPeriod;
	/**
	 * 
	 */
	@NotNull(message = "Transaction Id is mandatory")
	private Long transactionId;
	/**
	 * 
	 */
	@NotNull(message = "Target parameter is mandatory")
	@Size(min = 1, message = "Target parameter should not be empty string")
	private String target;
	/**
	 * 
	 */
	@NotNull(message = "script parameter is mandatory")
	@Size(min = 1, message = "Script should not be empty")
	private List<byte[]> script;

	/**
	 * Instantiate DTO builder
	 * 
	 * @param conversationId
	 * @param validityPeriod
	 * @param transactionId
	 * @param target
	 * @param script
	 * @return
	 */
	public static DTOBuilder getBuilderInstance(BigInteger conversationId,
	                                            Integer validityPeriod,
	                                            Long transactionId,
	                                            String target,
	                                            List<byte[]> script) {
		return new DTOBuilder(conversationId, validityPeriod, transactionId, target, script);
	}

	/**
	 * @return the conversationId
	 */
	public BigInteger getConversationId() {
		return conversationId;
	}

	/**
	 * @return the validityPeriod
	 */
	public Integer getValidityPeriod() {
		return validityPeriod;
	}

	/**
	 * @return the transactionId
	 */
	public Long getTransactionId() {
		return transactionId;
	}

	/**
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * @return the script
	 */
	public List<byte[]> getScript() {
		return script;
	}

}
