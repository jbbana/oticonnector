/**
 * 
 */
package com.oberthur.connector.oti.dto;

import java.io.Serializable;
import java.math.BigInteger;

import lombok.Getter;

import com.oberthur.connector.oti.http.ScriptStatus;

/**
 * @author Illya Krakovskyy
 * 
 */
@Getter
public class PoRRequestDTO implements Serializable {

	/**
	 * @author Illya Krakovskyy
	 * 
	 */
	public static class DTOBuilder {
		/**
		 * 
		 */
		private final PoRRequestDTO built;

		/**
		 * 
		 */
		private DTOBuilder() {
			built = new PoRRequestDTO();
		}

		/**
		 * @param agentId
		 * @param scriptStatus
		 * @param sessionID
		 * @param transactionID
		 * @param content
		 */
		public DTOBuilder(String agentId, ScriptStatus scriptStatus, BigInteger sessionID, Long transactionID, byte[] content) {
			this();
			built.agentId = agentId;
			built.scriptStatus = scriptStatus;
			built.sessionID = sessionID;
			built.transactionID = transactionID;
			built.content = content;
		}

		public PoRRequestDTO build() {
			return built;
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1216535068082584340L;

	/**
	 * 
	 */
	private String originatingURL;

	/**
     * 
     */
	private String agentId;
	/**
     * 
     */
	private ScriptStatus scriptStatus;
	/**
     * 
     */
	private BigInteger sessionID;
	/**
     * 
     */
	private Long transactionID;

	/**
	 * 
	 */
	private byte[] content;

	/**
	 * @param agentId
	 * @param scriptStatus
	 * @param sessionID
	 * @param transactionID
	 * @param content
	 * @return
	 */
	public static DTOBuilder getBuilderInstance(String agentId,
	                                            ScriptStatus scriptStatus,
	                                            BigInteger sessionID,
	                                            Long transactionID,
	                                            byte[] content) {
		return new DTOBuilder(agentId, scriptStatus, sessionID, transactionID, content);
	}

}
