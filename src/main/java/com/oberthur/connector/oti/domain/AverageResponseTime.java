package com.oberthur.connector.oti.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: AverageResponseTime
 *
 */
@Entity
@Table(name="AVERAGE_RESP_TIME")
public class AverageResponseTime implements Serializable, MonitoringStatistics {

	   
    /**
     * 
     */
    private static final long serialVersionUID = -7682862314197074561L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
	private Long id;

	@Column(name="AVG_RESP_TIME")
	private BigDecimal averageResponseTime = BigDecimal.ZERO;

    @Column(name = "HITS_COUNTER")
    private BigInteger hitsCounter = BigInteger.ZERO;

    @OneToOne
    @JoinColumn(name = "FK_INTERFACE")
    private Interfaces interfaceName;

	public AverageResponseTime() {
		super();
    }


	@Override
	public void refreshStatistics() {
		hitsCounter = BigInteger.ZERO;
		averageResponseTime = BigDecimal.ZERO;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public BigDecimal getAverageResponseTime() {
		return this.averageResponseTime;
	}

	public void setAverageResponseTime(BigDecimal averageResponseTime) {
		this.averageResponseTime = averageResponseTime;
	}

    /**
     * @return the interfaceName
     */
    public Interfaces getInterfaceName() {
        return interfaceName;
    }

    /**
     * @param interfaceName
     *            the interfaceName to set
     */
    public void setInterfaceName(Interfaces interfaceName) {
        this.interfaceName = interfaceName;
    }

    /**
     * @return the hitsCounter
     */
    public BigInteger getHitsCounter() {
        return hitsCounter;
    }

    /**
     * @param hitsCounter
     *            the hitsCounter to set
     */
    public void setHitsCounter(BigInteger hitsCounter) {
        this.hitsCounter = hitsCounter;
    }
   
}