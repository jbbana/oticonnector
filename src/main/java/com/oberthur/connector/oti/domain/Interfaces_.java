package com.oberthur.connector.oti.domain;

import com.oberthur.connector.oti.monitoring.InterfacesNames;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-06-22T10:33:41.292+0300")
@StaticMetamodel(Interfaces.class)
public class Interfaces_ {
	public static volatile SingularAttribute<Interfaces, Long> id;
	public static volatile SingularAttribute<Interfaces, InterfacesNames> interfaceName;
}
