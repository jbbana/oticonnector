package com.oberthur.connector.oti.domain;

import java.math.BigInteger;
import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-06-22T10:33:41.306+0300")
@StaticMetamodel(Requests.class)
public class Requests_ {
	public static volatile SingularAttribute<Requests, Long> id;
	public static volatile SingularAttribute<Requests, BigInteger> requestsNumber;
	public static volatile SingularAttribute<Requests, BigInteger> errorsNumber;
	public static volatile SingularAttribute<Requests, Interfaces> interfaceName;
	public static volatile SingularAttribute<Requests, Timestamp> lastNotificationTime;
}
