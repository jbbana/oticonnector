package com.oberthur.connector.oti.domain;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.oberthur.connector.api.SecureElementType;
import com.oberthur.connector.oti.dto.SessionStatus;

@Entity
@Table(name = "OTI_SESSION")
public class SessionOTI {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID")
	@Getter
	@Setter
	private BigInteger id;

	@Column(name = "SESSION_ID")
	@Getter
	@Setter
	private BigInteger sessionId;

	@Column(name = "SECURE_ELEMENT")
	@Getter
	@Setter
	private String secureElement;

	@Column(name = "SECURE_ELEMENT_TYPE")
	@Enumerated(EnumType.STRING)
	@Getter
	@Setter
	private SecureElementType secureElementType = SecureElementType.ICCID;

	@Column(name = "TRANSACTION_ID")
	@Getter
	@Setter
	private Long transactionId;

	@Column(name = "AGENT_ID")
	@Getter
	@Setter
	private String agentId;

	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	@Getter
	@Setter
	private SessionStatus status = SessionStatus.INITIATED;

	@Column(name = "LOGIN")
	@Getter
	@Setter
	private String login;

	@Column(name = "PASSWORD")
	@Getter
	@Setter
	private String password;

	@Column(name = "DATE_CREATED")
	@Getter
	@Setter
	private Timestamp createdDate;

	@Column(name = "DATE_UPDATED")
	@Getter
	@Setter
	private Timestamp updatedDate;

	@Column(name = "PURGE_DATE")
	@Getter
	@Setter
	private Timestamp purgeDate;

	@Column(name = "DATE_VALID")
	@Getter
	@Setter
	private Timestamp validDate;

	@PrePersist
	public void prePersist() {
		long currTime = new Date().getTime();
		createdDate = new Timestamp(currTime);
		updatedDate = new Timestamp(currTime);
	}

	@PreUpdate
	public void preUpdate() {
		updatedDate = new Timestamp(new Date().getTime());
	}
}
