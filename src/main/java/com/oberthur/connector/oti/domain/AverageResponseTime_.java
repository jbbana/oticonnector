package com.oberthur.connector.oti.domain;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-06-22T10:33:41.102+0300")
@StaticMetamodel(AverageResponseTime.class)
public class AverageResponseTime_ {
	public static volatile SingularAttribute<AverageResponseTime, Long> id;
	public static volatile SingularAttribute<AverageResponseTime, BigDecimal> averageResponseTime;
	public static volatile SingularAttribute<AverageResponseTime, BigInteger> hitsCounter;
	public static volatile SingularAttribute<AverageResponseTime, Interfaces> interfaceName;
}
