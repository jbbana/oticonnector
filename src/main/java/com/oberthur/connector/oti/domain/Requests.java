/**
 * 
 */
package com.oberthur.connector.oti.domain;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

/**
 * @author Illya Krakovskyy
 *
 */
@Entity
@Table(name = "REQUESTS")
public class Requests implements Serializable, MonitoringStatistics {

    /**
	 * 
	 */
	private static final long serialVersionUID = -4456029717178390759L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    /**
     * 
     */
    @Column(name = "REQUESTS_NUMBER")
    private BigInteger requestsNumber = BigInteger.ZERO;
    
    /**
     * 
     */
    @Column(name="ERRORS_NUMBER")
    private BigInteger errorsNumber = BigInteger.ZERO;
    /**
     * 
     */
    @OneToOne
    @JoinColumn(name = "FK_INTERFACE")
    private Interfaces interfaceName;
    /**
     * 
     */
    @Column(name = "LAST_NOTIF_TIME")
    private Timestamp lastNotificationTime;

    @PrePersist
    public void prePersist() {
        lastNotificationTime = new Timestamp(System.currentTimeMillis());
    }

    /* (non-Javadoc)
	 * @see com.oberthur.connector.oti.domain.MonitoringStatistics#refreshStatistics()
	 */
    @Override
	public void refreshStatistics() {
        this.errorsNumber = BigInteger.ZERO;
        this.requestsNumber = BigInteger.ZERO;
        this.lastNotificationTime = new Timestamp(System.currentTimeMillis());
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the requestsNumber
     */
    public BigInteger getRequestsNumber() {
        return requestsNumber;
    }

    /**
     * @param requestsNumber
     *            the requestsNumber to set
     */
    public void setRequestsNumber(BigInteger requestsNumber) {
        this.requestsNumber = requestsNumber;
    }

    /**
     * @return the errorsNumber
     */
    public BigInteger getErrorsNumber() {
        return errorsNumber;
    }

    /**
     * @param errorsNumber
     *            the errorsNumber to set
     */
    public void setErrorsNumber(BigInteger errorsNumber) {
        this.errorsNumber = errorsNumber;
    }

    /**
     * @return the interfaceName
     */
    public Interfaces getInterfaceName() {
        return interfaceName;
    }

    /**
     * @param interfaceName
     *            the interfaceName to set
     */
    public void setInterfaceName(Interfaces interfaceName) {
        this.interfaceName = interfaceName;
    }

    /**
     * @return the lastNotificationTime
     */
    public Timestamp getLastNotificationTime() {
        return lastNotificationTime;
    }

    /**
     * @param lastNotificationTime
     *            the lastNotificationTime to set
     */
    public void setLastNotificationTime(Timestamp lastNotificationTime) {
        this.lastNotificationTime = lastNotificationTime;
    }
}
