package com.oberthur.connector.oti.domain;

import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import com.oberthur.connector.api.SecureElementType;
import com.oberthur.connector.oti.dto.SessionStatus;

@StaticMetamodel(SessionOTI.class)
public class SessionOTI_ {

	public static volatile SingularAttribute<SessionOTI, BigInteger> id;
	public static volatile SingularAttribute<SessionOTI, Long> transactionId;
	public static volatile SingularAttribute<SessionOTI, BigInteger> sessionId;
	public static volatile SingularAttribute<SessionOTI, String> agentId;
	public static volatile SingularAttribute<SessionOTI, SessionStatus> status;
	public static volatile SingularAttribute<SessionOTI, Timestamp> updatedDate;
	public static volatile SingularAttribute<SessionOTI, String> secureElement;
	public static volatile SingularAttribute<SessionOTI, SecureElementType> secureElementType;
	public static volatile SingularAttribute<SessionOTI, String> login;
	public static volatile SingularAttribute<SessionOTI, String> password;
	public static volatile SingularAttribute<SessionOTI, Timestamp> createdDate;
	public static volatile SingularAttribute<SessionOTI, Timestamp> purgeDate;
	public static volatile SingularAttribute<SessionOTI, Timestamp> validDate;

}