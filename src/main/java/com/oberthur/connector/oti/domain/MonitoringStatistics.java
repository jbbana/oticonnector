package com.oberthur.connector.oti.domain;

/**
 * @author Illya Krakovsky
 * 
 * Super interface for monitoring statistics entities
 *
 */
public interface MonitoringStatistics {

	/**
	 * Reset statistics data 
	 */
	public void refreshStatistics();

}