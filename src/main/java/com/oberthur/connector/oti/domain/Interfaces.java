package com.oberthur.connector.oti.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.oberthur.connector.oti.monitoring.InterfacesNames;

/**
 * Entity implementation class for Entity: Interfaces
 *
 */
@Entity
@Table(name = "INTERFACES")
public class Interfaces implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 85433000340686405L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(unique = true, nullable = false, updatable = false, insertable = true, name = "INTERFACE_NAME")
	private InterfacesNames interfaceName;

	public Interfaces() {
		super();
	}   
	/**
     * @return the id
     */
    public Long getId() {
        return id;
    }
    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    public InterfacesNames getInterfaceName() {
		return this.interfaceName;
	}

	public void setInterfaceName(InterfacesNames interfaceName) {
		this.interfaceName = interfaceName;
	}
   
}
