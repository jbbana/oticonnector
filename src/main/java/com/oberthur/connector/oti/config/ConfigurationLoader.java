/**
 * 
 */
package com.oberthur.connector.oti.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.exceptions.OTIException;
import com.oberthur.connector.oti.http.OTIHttpConfigKeys;
import com.oberthur.connector.oti.http.bean.OTIControllerConfigKeys;
import com.oberthur.connector.oti.monitoring.Monitoring;
import com.oberthur.connector.oti.monitoring.OTICMonitoringConfigKeys;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvent;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents;
import com.oberthur.connector.oti.service.bean.OTIServiceConfigKeys;
import com.oberthur.connector.oti.service.notification.NotificationConfigKeys;
import com.oberthur.connector.oti.service.startup.tasks.TasksConfigKeys;

/**
 * @author Illya Krakovskyy
 *
 */
@RequestScoped
public class ConfigurationLoader {

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(ConfigurationLoader.class);

    private boolean validConfiguration = true;

    @Inject
    @Monitoring
    private Event<OTICMonitoringEvent> event;

    @PostConstruct
    public void validateConfiguration() throws OTIException {
        log.debug("Validate OTI Connector configuration");
        List<String> missedParams = new ArrayList<String>();

        Set<String> mandatoryConfigKeys = MandatoryConfigKeys.mandatoryConfigKeys();
        for (String configKey : mandatoryConfigKeys) {
            if (System.getProperty(configKey) == null) {
                missedParams.add(configKey);
            }
        }

        if (!missedParams.isEmpty()) {
            String missedParamsStr = Arrays.toString(missedParams.toArray(new String[missedParams.size()]));
            event.fire(new OTICMonitoringEvent(OTICMonitoringEvents.CONFIG_LOAD_FAILED, missedParamsStr));

            validConfiguration = false;
        }
    }

    /**
     * Loading OTI Connector configuration
     * 
     * @throws BackingStoreException
     */
    public void loadConfiguration() {
        log.debug("Loading OTI connector configuration");

        try {
            loadConfigItem(OTIHttpConfigKeys.values());
            loadConfigItem(OTIControllerConfigKeys.values());
            loadConfigItem(OTIServiceConfigKeys.values());
            loadConfigItem(TasksConfigKeys.values());
            loadConfigItem(OTICMonitoringConfigKeys.values());
            loadConfigItem(NotificationConfigKeys.values());
        } catch (BackingStoreException bse) {
            log.error("Configuration storing failure: {}", bse.getMessage(), bse);
            throw new IllegalStateException(bse);
        }

        if (validConfiguration) {
            event.fire(new OTICMonitoringEvent(OTICMonitoringEvents.CONFIG_LOAD_SUCCESS));
        }
    }

    /**
     * Populates OTIC config from startup properties
     * 
     * @param otiConfigKey
     *            config key
     * @throws BackingStoreException
     */
    private void loadConfigItem(OTIConfigKeys[] otiConfigKeys) throws BackingStoreException {
        Preferences configNode = Preferences.userNodeForPackage(otiConfigKeys[0].getClass());
        configNode.clear();
        for (OTIConfigKeys otiConfigKey : otiConfigKeys) {
            String configKey = otiConfigKey.getConfigKey();

            String configValue = System.getProperty(configKey);
            if (configValue != null) {
                configNode.put(configKey, configValue);
            }
        }
        configNode.sync();
    }
}
