/**
 * 
 */
package com.oberthur.connector.oti.config;

import java.math.BigInteger;

/**
 * @author Illya Krakovskyy
 *
 */
public interface OTIConfigKeys {

    /**
     * @return config key name
     */
    String getConfigKey();

    /**
     * @return default value for the key specified
     */
    String getDefaultValue();

    /**
     * @return numerical representation of default value. Should return null if there is no numeric default value at configuration item
     */
    BigInteger getNumericDefaultValue();

}
