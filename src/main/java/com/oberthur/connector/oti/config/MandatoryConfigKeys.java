/**
 * 
 */
package com.oberthur.connector.oti.config;

import java.util.HashSet;
import java.util.Set;

import com.oberthur.connector.oti.http.OTIHttpConfigKeys;
import com.oberthur.connector.oti.http.bean.OTIControllerConfigKeys;
import com.oberthur.connector.oti.monitoring.OTICMonitoringConfigKeys;
import com.oberthur.connector.oti.service.bean.OTIServiceConfigKeys;
import com.oberthur.connector.oti.service.notification.NotificationConfigKeys;
import com.oberthur.connector.oti.service.startup.tasks.TasksConfigKeys;

/**
 * @author Illya Krakovskyy
 *
 */
public enum MandatoryConfigKeys {
    HTTP_MANDATORIES(OTIHttpConfigKeys.BASE_URL, OTIHttpConfigKeys.ORIGINATING_URL),
    JOBS_MANDATORIES(TasksConfigKeys.values()),
    SERVICE_MANDATORIES(OTIServiceConfigKeys.DEFAULT_VALIDITY_PERIOD, OTIServiceConfigKeys.DROP_IMMEDIATELY),
    CONTROLLER_MANDATORIES(OTIControllerConfigKeys.values()),
    MONITORING_MANDATORIES(OTICMonitoringConfigKeys.MONITORING_SNMP_HOST, OTICMonitoringConfigKeys.MONITORING_SNMP_PORT, OTICMonitoringConfigKeys.MONITORING_NOTIFICATION_FREQUENCY),
    NOTIFICATION_MANDATORIES(NotificationConfigKeys.NOTIFICATION_ENABLED);
    /**
     * Mandatory config items
     */
    private OTIConfigKeys[] configKeys;

    /**
     * @param configKeys
     */
    private MandatoryConfigKeys(OTIConfigKeys... configKeys) {
        this.configKeys = configKeys;
    }

    /**
     * @return configKeys
     */
    public OTIConfigKeys[] getCoonfigKeys() {
        return configKeys;
    }

    /**
     * List all configuration keys names assumed mandatory
     * 
     * @return
     */
    public static Set<String> mandatoryConfigKeys() {
        HashSet<String> configKeys = new HashSet<String>();
        for (MandatoryConfigKeys mandatoryConfigKeys : MandatoryConfigKeys.values()) {
            OTIConfigKeys[] coonfigKeys = mandatoryConfigKeys.getCoonfigKeys();
            for (OTIConfigKeys otiConfigKey : coonfigKeys) {
                configKeys.add(otiConfigKey.getConfigKey());
            }
        }

        return configKeys;
    }
}
