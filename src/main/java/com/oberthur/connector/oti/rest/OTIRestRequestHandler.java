package com.oberthur.connector.oti.rest;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Path;

import com.oberthur.connector.api.ConnectorService;
import com.oberthur.connector.common.rest.AbstractRestRequestHandler;
import com.oberthur.connector.oti.service.OTIConnectorServiceBean;

@RequestScoped
@Path(AbstractRestRequestHandler.ADDITIONAL_PATH)
public class OTIRestRequestHandler extends AbstractRestRequestHandler {

	private ConnectorService connectorService;

	public OTIRestRequestHandler() {
	}

	public OTIRestRequestHandler(OTIConnectorServiceBean connectorServiceBean) {
		this.connectorService = connectorServiceBean;
	}

	@Override
	protected ConnectorService getConnectorService() {
		return connectorService;
	}
}
