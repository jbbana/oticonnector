package com.oberthur.connector.oti.rest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class OTIRestLogFilter implements ContainerRequestFilter, WriterInterceptor {

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(OTIRestLogFilter.class);


    /*
     * (non-Javadoc)
     * 
     * @see javax.ws.rs.container.ContainerRequestFilter#filter(javax.ws.rs.container.ContainerRequestContext)
     */
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        byte[] buffer = new byte[4096];
        int n = 0;
        InputStream entityStream = requestContext.getEntityStream();
        while (-1 != (n = entityStream.read(buffer))) {
            baos.write(buffer, 0, n);
        }
        baos.flush();
        byte[] byteArray = baos.toByteArray();

        requestContext.setEntityStream(new ByteArrayInputStream(byteArray));

        log.debug("REST incoming request to {}, content: {}", requestContext.getUriInfo().getAbsolutePath(), new String(byteArray, Charset.forName("UTF-8")));
    }


    /*
     * (non-Javadoc)
     * 
     * @see javax.ws.rs.ext.WriterInterceptor#aroundWriteTo(javax.ws.rs.ext.WriterInterceptorContext)
     */
    @Override
    public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
        OutputStream outputStream = context.getOutputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        context.setOutputStream(baos);
        try {
            context.proceed();
        } finally {
            log.debug("REST response body: {}", baos.toString("UTF-8"));
            baos.writeTo(outputStream);
            context.setOutputStream(outputStream);
        }
    }

}
