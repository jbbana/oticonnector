/**
 * 
 */
package com.oberthur.connector.oti.rest;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.oberthur.connector.common.rest.SEConnectorResponsePath;
import com.oberthur.connector.oti.service.OTIConnectorServiceBean;

/**
 * @author Illya Krakovskyy
 *
 */
@ApplicationPath(SEConnectorResponsePath.APPLICATION_PATH)
public class ApplicationConfig extends Application {

	private Set<Object> singletons = new HashSet<Object>();

	public ApplicationConfig() {
	}

	@Inject
	public ApplicationConfig(OTIConnectorServiceBean otiConnectorServiceBean) {
		singletons.add(new OTIRestRequestHandler(otiConnectorServiceBean));
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
