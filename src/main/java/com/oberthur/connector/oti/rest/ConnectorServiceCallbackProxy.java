/**
 * 
 */
package com.oberthur.connector.oti.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.oberthur.connector.api.xml.bind.notification.Notification;
import com.oberthur.connector.api.xml.bind.response.OutOfSessionResponse;
import com.oberthur.connector.api.xml.bind.response.PrepareCommunicationResponse;
import com.oberthur.connector.api.xml.bind.response.SendScriptResponse;
import com.oberthur.connector.common.rest.callback.ConnectorServiceCallbackBean;
import com.oberthur.connector.oti.interceptor.monitoring.AsyncInterfacesMonitoring;
import com.oberthur.connector.oti.interceptor.monitoring.AsyncInterfacesMonitoring.AsyncInterfaceNames;

/**
 * @author Illya Krakovskyy
 *
 *         Proxy class to wrap calls to callback interface
 */
@RequestScoped
public class ConnectorServiceCallbackProxy {

    @Inject
    private ConnectorServiceCallbackBean callback;

    /**
     * @param response
     * @return
     */
    @AsyncInterfacesMonitoring(asyncInterface = AsyncInterfaceNames.PREPARE_COMMUNICATION_ASYNC_RESPONSE)
	public Notification prepareCommunicationResponse(PrepareCommunicationResponse response) {
        return callback.prepareCommunicationResponse(response);
	}

    /**
     * @param response
     * @return
     */
    @AsyncInterfacesMonitoring(asyncInterface = AsyncInterfaceNames.OUT_OF_SESSION_ASYNC_RESPONSE)
	public Notification outOfSessionResponse(OutOfSessionResponse response) {
        return callback.outOfSessionResponse(response);
	}

    /**
     * @param response
     * @return
     */
    @AsyncInterfacesMonitoring(asyncInterface = AsyncInterfaceNames.SEND_SCRIPT_ASYNC_RESPONSE)
	public Notification sendScriptResponse(SendScriptResponse response) {
        return callback.sendScriptResponse(response);
	}

}
