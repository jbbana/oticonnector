package com.oberthur.connector.oti.ram.state;

import java.math.BigInteger;

import com.oberthur.connector.api.SecureElementType;

public class AgentSessionState {

    private enum AgentSessionStatus {
        IN_PROGRESS,
        RELEASED,
        BLOCKED,
        TIMED_OUT,
        OUT_OF_SESSION,
        DISCARDED,
        SENT
    }

    /**
     * Current status of agent session
     */
    private AgentSessionStatus agentStatus = AgentSessionStatus.IN_PROGRESS;

    /**
     * 
     */
    private String seID;

    /**
     * 
     */
    private SecureElementType seIdType = SecureElementType.ICCID;
    /**
     * Application ID
     */
    private byte[] applicationId = new byte[0];

    /**
     * 
     */
    private BigInteger sessionId;

    /**
     * 
     */
    private Long transactionId;

    /**
     * Script to send
     */
    private byte[] script = new byte[0];

    /**
     * @return true if session state marked "out of session"
     */
    public boolean isOutOfSession() {
        return AgentSessionStatus.OUT_OF_SESSION == this.agentStatus;
    }

    /**
     * Mark session state "out of session"
     */
    public void markOutOfSession() {
        setAgentStatus(AgentSessionStatus.OUT_OF_SESSION);
    }

    /**
     * @return true if session state marked "timed out"
     */
    public boolean isTimedOut() {
        return AgentSessionStatus.TIMED_OUT == this.agentStatus;
    }

    /**
     * Mark session stated as "timed out"
     */
    public void markTimedOut() {
        setAgentStatus(AgentSessionStatus.TIMED_OUT);
    }

    /**
     * @return true if session state marked "released"
     */
    public boolean isReleased() {
        return AgentSessionStatus.RELEASED == this.agentStatus;
    }

    /**
     * Mark session state released
     */
    public void markReleased() {
        setAgentStatus(AgentSessionStatus.RELEASED);
    }

    /**
     * @return true if session state marked "discarded" (after drop
     *         communication command)
     */
    public boolean isDiscarded() {
        return AgentSessionStatus.DISCARDED == this.agentStatus;
    }

    /**
     * Mark session state discarded
     */
    public void markDiscarded() {
        setAgentStatus(AgentSessionStatus.DISCARDED);
    }

    /**
     * @return true if session state marked "sent"
     */
    public boolean isSent() {
        return AgentSessionStatus.SENT == this.agentStatus;
    }

    /**
     * Mark session state "sent"
     */
    public void markSent() {
        setAgentStatus(AgentSessionStatus.SENT);
    }

    /**
     * @return the seID
     */
    public String getSeID() {
        return seID;
    }

    /**
     * @param seID
     *            the seID to set
     */
    public void setSeID(String seID) {
        this.seID = seID;
    }

    /**
     * @return the seIdType
     */
    public SecureElementType getSeIdType() {
        return seIdType;
    }

    /**
     * @param seIdType
     *            the seIdType to set
     */
    public void setSeIdType(SecureElementType seIdType) {
        this.seIdType = seIdType;
    }

    /**
     * @return the applicationId
     */
    public byte[] getApplicationId() {
        return applicationId;
    }

    /**
     * @param applicationId
     *            the applicationId to set
     */
    public void setApplicationId(byte[] applicationId) {
        this.applicationId = applicationId;
    }

    /**
     * @return the sessionId
     */
    public BigInteger getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId
     *            the sessionId to set
     */
    public void setSessionId(BigInteger sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the transactionId
     */
    public Long getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId
     *            the transactionId to set
     */
    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return the script
     */
    public byte[] getScript() {
        return script;
    }

    /**
     * @param script
     *            the script to set
     */
    public void setScript(byte[] script) {
        this.script = script;
    }

    /**
     * @param agentStatus
     *            the agentStatus to set
     */
    private void setAgentStatus(AgentSessionStatus agentStatus) {
        this.agentStatus = agentStatus;
    }

}