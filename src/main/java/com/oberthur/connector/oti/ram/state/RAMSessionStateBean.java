package com.oberthur.connector.oti.ram.state;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * RAM Requests registry
 */
@Singleton
@Slf4j
public class RAMSessionStateBean {

	@Getter
	private final Map<BigInteger, AgentSessionState> postSessionStates = new HashMap<>();

	@Getter
	private final Map<BigInteger, AgentSessionState> porSessionStates = new HashMap<>();

	public RAMSessionStateBean() {
	}

	/**
	 * @param stateKey
	 * @param isPoR
	 * @return
	 */
	public boolean containsState(BigInteger stateKey, boolean isPoR) {
		return isPoR ? porSessionStates.get(stateKey) != null : postSessionStates.get(stateKey) != null;
	}

	/**
	 * @param sessionID
	 * @return
	 */
	public Map<BigInteger, AgentSessionState> getSessionStateMap(BigInteger sessionID) {
		return getSessionStateMap(getPorSessionStates().get(sessionID) != null);
	}

	/**
	 * @param isPoR
	 * @return
	 */
	public Map<BigInteger, AgentSessionState> getSessionStateMap(boolean isPoR) {
		return isPoR ? getPorSessionStates() : getPostSessionStates();
	}

	/**
	 * @param sessionID
	 */
	public void putPOSTState(BigInteger sessionID) {
		log.debug("Creating state for POST Session with OTI Session ID {}", sessionID);
		postSessionStates.put(sessionID, new AgentSessionState());
	}

	/**
	 * @param sessionID
	 * @return
	 */
	public AgentSessionState putPoRState(BigInteger sessionID) {
		log.debug("Creating state for PoR Session with OTI Session ID {}", sessionID);
		porSessionStates.put(sessionID, new AgentSessionState());
		return porSessionStates.get(sessionID);
	}
}
