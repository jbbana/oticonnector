package com.oberthur.connector.oti.ram.requests;

import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_FROM_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_RESUME_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_SCRIPT_STATUS;
import static org.apache.commons.lang3.StringUtils.removeStart;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.oberthur.connector.oti.http.GPKeys;
import com.oberthur.connector.oti.http.OTIHttpUtils;
import com.oberthur.connector.oti.http.ScriptStatus;

public class RAMRequest {

	private final HttpServletRequest req;
	private final HttpServletResponse resp;
	private final byte[] content;
	private String targetSE;
	private String agentId;

	private RAMRequest(HttpServletRequest req, HttpServletResponse resp) {
		this.req = req;
		this.resp = resp;

		this.content = OTIHttpUtils.retrieveScriptParam(req);
	}

	public static RAMRequest getInstance(HttpServletRequest req, HttpServletResponse resp) {
		final RAMRequest ramRequest = new RAMRequest(req, resp);
		ramRequest.agentId = ramRequest.getRequestHeader(X_ADMIN_FROM_HEADER.getGpKey());
		return ramRequest;
	}

	public boolean isPrepareCommunication() {
		return ArrayUtils.isEmpty(getContent()) && StringUtils.isEmpty(getRequestValue(X_ADMIN_SCRIPT_STATUS.getGpKey()))
		        && StringUtils.isEmpty(getRequestValue(X_ADMIN_RESUME_HEADER.getGpKey()));
	}

	public HttpServletRequest getHttpRequest() {
		return req;
	}

	public HttpServletResponse getHttpResponse() {
		return resp;
	}

	public byte[] getContent() {
		return content;
	}

	public void setResponseStatus(int statusCode) {
		resp.setStatus(statusCode);
	}

	public void setResponseContentLength(byte[] script) {
		resp.setContentLength(script.length);
	}

	public void setResponseContentType(String contentType) {
		resp.setContentType(contentType);
	}

	public void addResponseHeader(String key, String val) {
		resp.setHeader(key, val);
	}

	public int getRequestContentLength() {
		return req.getContentLength();
	}

	public String getRequestHeader(String value) {
		String param = req.getHeader(value);
		return param;
	}

	public String getResponseHeader(String value) {
		return resp.getHeader(value);
	}

	public String getValue(String key, String prefix) {
		return removeStart(getRequestHeader(key), prefix);
	}

	public String getRequestValue(String key) {
		return req.getHeader(key);
	}

	public String getTargetSE() {
		return targetSE;
	}

	public String getAgentId() {
		return agentId;
	}

	public ScriptStatus getScriptStatus() {
		return ScriptStatus.getScriptStatus(getRequestValue(GPKeys.X_ADMIN_SCRIPT_STATUS.getGpKey()));
	}

	public String getConnectorUrl() {
		return req.getRequestURL().toString();
	}
}
