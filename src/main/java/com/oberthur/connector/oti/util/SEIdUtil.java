package com.oberthur.connector.oti.util;

import com.oberthur.connector.api.SecureElementType;
import org.apache.commons.lang.StringUtils;

/**
 * Created by mikam on 16.07.2015.
 */
public class SEIdUtil {

    public static String getSeId(String seId, SecureElementType secureElementType, boolean isCPLCSubstr){
        if(secureElementType == SecureElementType.CPLC && isCPLCSubstr){
            return StringUtils.substring(seId, 0, 36);
        }
        return seId;
    }
}
