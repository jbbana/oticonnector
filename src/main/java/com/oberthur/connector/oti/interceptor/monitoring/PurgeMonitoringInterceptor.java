/**
 * 
 */
package com.oberthur.connector.oti.interceptor.monitoring;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.oberthur.connector.oti.interceptor.monitoring.PurgeMonitoring.PurgeType;
import com.oberthur.connector.oti.monitoring.Monitoring;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvent;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents;
import com.oberthur.connector.oti.service.startup.tasks.CleanTask;
import com.oberthur.connector.oti.service.startup.tasks.PurgeTask;
import com.oberthur.connector.oti.service.startup.tasks.TasksConfigKeys;

/**
 * @author Illya Krakovskyy
 *
 */
@Interceptor
@PurgeMonitoring
public class PurgeMonitoringInterceptor {

    @Inject
    @Monitoring
    private Event<OTICMonitoringEvent> event;

    @AroundInvoke
    public Object monitorPurge(InvocationContext ictx) throws Exception {

        PurgeMonitoring annotation = ictx.getMethod().getAnnotation(PurgeMonitoring.class);
        PurgeType[] annotationParams = annotation.purgeType();

        Object proceed = ictx.proceed();

        if (AnnotationParamsValidator.validateParams(annotation.annotationType().getSimpleName(), annotationParams)) {
            Object target = ictx.getTarget();

            PurgeType purgeType = annotationParams[0];

            OTICMonitoringEvent monitoringEvent;
            if (PurgeType.PURGE == purgeType) {
                PurgeTask purgeTask = (PurgeTask) target;
                String[] purgedSEIds = purgeTask.getPurgedSEIds();
                monitoringEvent = new OTICMonitoringEvent(OTICMonitoringEvents.SESSIONS_PURGED, purgedSEIds.length, TimeUnit.DAYS.toHours(1), Arrays.toString(purgedSEIds));
            } else {
                CleanTask cleanTask = (CleanTask) target;
                String[] cleanedSEIDs = cleanTask.getCleanedSEIDs();

                int cleanPeriodHours = Preferences.userNodeForPackage(CleanTask.class).getInt(TasksConfigKeys.CLEAN_PERIOD.getConfigKey(),
                        TasksConfigKeys.CLEAN_PERIOD.getNumericDefaultValue().intValue());
                long cleaningPeriod = TimeUnit.HOURS.toMillis(cleanPeriodHours);

                monitoringEvent = new OTICMonitoringEvent(OTICMonitoringEvents.SESSIONS_CLEANED, cleanedSEIDs.length, cleaningPeriod, Arrays.toString(cleanedSEIDs));
            }

            event.fire(monitoringEvent);
        }

        return proceed;
    }

}
