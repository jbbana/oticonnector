/**
 * 
 */
package com.oberthur.connector.oti.interceptor.monitoring;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.PersistenceException;

import com.oberthur.connector.oti.monitoring.Monitoring;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvent;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents;

/**
 * @author Illya Krakovskyy
 *
 */
@Interceptor
@DataAccessMonitoring
public class DataAccessMonitoringInterceptor {

    @Inject
    @Monitoring
    private Event<OTICMonitoringEvent> event;

    @AroundInvoke
    public Object monitorDataAccess(InvocationContext ictx) throws Exception {
        OTICMonitoringEvent monitoringEvent = new OTICMonitoringEvent(OTICMonitoringEvents.DATA_ACCESS_SUCCESS);
        boolean sendEvent = true;
        try {
            return ictx.proceed();
        } catch (Exception e) {
            if (PersistenceException.class.getName().equals(e.getClass().getName())) {
                monitoringEvent = new OTICMonitoringEvent(OTICMonitoringEvents.DATA_ACCESS_FAILED, e.getMessage());
            } else {
                sendEvent = false;
            }
            throw e;
        } finally {
            if (sendEvent) {
                event.fire(monitoringEvent);
            }
        }
    }
}
