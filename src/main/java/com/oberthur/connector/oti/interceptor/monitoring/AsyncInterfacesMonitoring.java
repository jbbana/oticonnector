/**
 * 
 */
package com.oberthur.connector.oti.interceptor.monitoring;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;

/**
 * @author Illya Krakovskyy
 *
 */
@InterceptorBinding
@Inherited
@Target({ TYPE, METHOD })
@Retention(RUNTIME)
public @interface AsyncInterfacesMonitoring {
    public enum AsyncInterfaceNames {
        PREPARE_COMMUNICATION_ASYNC_RESPONSE,
        SEND_SCRIPT_ASYNC_RESPONSE,
        OUT_OF_SESSION_ASYNC_RESPONSE
    };

    @Nonbinding
    public AsyncInterfaceNames[] asyncInterface() default {};
}
