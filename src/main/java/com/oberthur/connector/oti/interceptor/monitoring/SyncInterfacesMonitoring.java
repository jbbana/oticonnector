/**
 * 
 */
package com.oberthur.connector.oti.interceptor.monitoring;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;

import com.oberthur.connector.oti.monitoring.InterfacesNames;

/**
 * @author Illya Krakovskyy
 *
 */
@InterceptorBinding
@Inherited
@Target({ TYPE, METHOD })
@Retention(RUNTIME)
public @interface SyncInterfacesMonitoring {
    /**
     * @author Illya Krakovskyy
     *
     */
    public enum SyncInterfaceName {
        CALLER_PREPARE_COMMUNICATION(InterfacesNames.CALLER_PREPARE_COMMUNICATION),
        CALLER_SEND_SCRIPT(InterfacesNames.CALLER_SEND_SCRIPT),
        CALLER_DROP_COMMUNICATION(InterfacesNames.CALLER_DROP_COMMUNICATION),
        DEVICE_REQUEST(InterfacesNames.DEVICE_REQUEST),
        DEVICE_RESPONSE(InterfacesNames.DEVICE_RESPONSE);
        
        /**
         * 
         */
        private InterfacesNames interfaceName;

        /**
         * @return the interfacesName
         */
        public InterfacesNames getInterfaceName() {
            return interfaceName;
        }

        /**
         * @param interfaceName
         */
        private SyncInterfaceName(InterfacesNames interfaceName) {
            this.interfaceName = interfaceName;
        }
    };

    @Nonbinding
    public SyncInterfaceName[] interfaceName() default {};
}
