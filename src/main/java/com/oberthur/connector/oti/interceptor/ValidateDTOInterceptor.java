/**
 * 
 */
package com.oberthur.connector.oti.interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.dto.ConnectorDTO;
import com.oberthur.connector.oti.dto.SendScriptDTO;

/**
 * @author Illya Krakovskyy
 *
 */
@Interceptor
@ValidateDTO
public class ValidateDTOInterceptor {
	/**
	 * SLF4j logging facility
	 */
	private static final Logger log = LoggerFactory.getLogger(ValidateRequestInterceptor.class);

	/**
	 * 
	 */
	public ValidateDTOInterceptor() {
	}

	/**
	 * @param ic
	 * @return
	 * @throws Exception
	 */
	@AroundInvoke
	public Object validateDTO(InvocationContext ic) throws Exception {
		Object[] parameters = ic.getParameters();
		if (parameters.length != 1) {
			throw new IllegalArgumentException("Expected single DTO as parameter");
		}

		Object dto = parameters[0];
		if (!ConnectorDTO.class.isInstance(dto) && !SendScriptDTO.class.isInstance(dto)) {
			throw new IllegalArgumentException(String.format("Unknown type of method parameter: %s", dto.getClass().getName()));
		}

		log.debug(String.format("Validating DTO input parameter of type %1$s in method %2$s.%3$s()", dto.getClass().getSimpleName(), ic
		        .getMethod().getDeclaringClass().getName(), ic.getMethod().getName()));
		ValidationHelper.validate(dto);

		return ic.proceed();
	}

}
