/**
 * 
 */
package com.oberthur.connector.oti.interceptor;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;

/**
 * @author Illya Krakovskyy
 *
 */
@InterceptorBinding
@Inherited
@Target({ TYPE, METHOD })
@Retention(RUNTIME)
@Documented
public @interface ScheduleSession {
    public enum ScheduleType {
        REGISTER_PURGE,
        REGISTER_CLEAN,
        UNREGISTER_PURGE,
        UNREGISTER_CLEAN
    }

    @Nonbinding
    public ScheduleType[] scheduleType() default {};

}
