/**
 * 
 */
package com.oberthur.connector.oti.interceptor;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * @author Illya Krakovskyy
 *
 */
public class ValidationHelper {

	/**
	 * @param param
	 * @throws IllegalArgumentException
	 */
	public static <T> void validate(T param) throws IllegalArgumentException {
		ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
		Validator validator = vf.getValidator();
		Set<ConstraintViolation<T>> violations = validator.validate(param);

		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}
	}

}
