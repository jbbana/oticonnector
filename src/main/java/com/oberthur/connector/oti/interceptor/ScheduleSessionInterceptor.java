/**
 * 
 */
package com.oberthur.connector.oti.interceptor;

import java.math.BigInteger;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.interceptor.ScheduleSession.ScheduleType;
import com.oberthur.connector.oti.service.startup.purge.PurgeSessionsScheduler;

/**
 * @author Illya Krakovskyy
 *
 */
@Interceptor
@ScheduleSession
public class ScheduleSessionInterceptor {

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(ScheduleSessionInterceptor.class);

    @Inject
    private PurgeSessionsScheduler scheduler;

    /**
     * @param ic
     * @return
     * @throws Exception
     */
    @AroundInvoke
    public Object scheduleSession(InvocationContext ic) throws Exception {
        Object proceed = ic.proceed();

        ScheduleSession annotation = ic.getMethod().getAnnotation(ScheduleSession.class);
        if (annotation.scheduleType().length == 1) {
            ScheduleType scheduleType = annotation.scheduleType()[0];
            switch (scheduleType) {
                case REGISTER_CLEAN:
                case REGISTER_PURGE:
                    registerForSchedule(proceed, scheduleType);
                    break;
                case UNREGISTER_CLEAN:
                case UNREGISTER_PURGE:
                    if (proceed instanceof BigInteger) {
                        scheduler.removeFromScheduled((BigInteger) proceed, ScheduleType.UNREGISTER_PURGE == scheduleType);
                    } else {
                        log.error("Expected session id of BigInteger type but obtained {}, skipping", proceed.getClass().getName());
                    }
                    break;
            }
        } else {
            log.error("Wrong usage of interceptor, no more and no less than one scheduleType should be defined");
        }

        return proceed;
    }

    /**
     * @param proceed
     * @param scheduleType
     */
    private void registerForSchedule(Object proceed, ScheduleType scheduleType) {
        if (proceed instanceof SessionOTI) {
            SessionOTI session = (SessionOTI) proceed;
            if (ScheduleType.REGISTER_PURGE == scheduleType) {
                scheduler.scheduleForPurge(session.getId(), session.getPurgeDate().getTime());
            } else if (ScheduleType.REGISTER_CLEAN == scheduleType) {
                scheduler.scheduleForClean(session.getId(), session.getValidDate().getTime());
            } else {
                log.error("Expected either REGISTER_PURGE or REGISTER_CLEAN schedule type but obtained {}, skipping", scheduleType.toString());
            }
        } else {
            log.error("Expected SessionOTI type but obtained {}, skipping", proceed.getClass().getName());
        }
    }

}
