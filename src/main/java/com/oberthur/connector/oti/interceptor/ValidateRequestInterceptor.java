/**
 * 
 */
package com.oberthur.connector.oti.interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.api.xml.bind.request.PrepareCommunicationRequest;
import com.oberthur.connector.api.xml.bind.request.SendScriptRequest;

/**
 * @author Illya Krakovskyy
 *
 */
@Interceptor
@ValidateRequest
public class ValidateRequestInterceptor {
	/**
	 * SLF4j logging facility
	 */
	private static final Logger log = LoggerFactory.getLogger(ValidateRequestInterceptor.class);

	/**
	 * 
	 */
	public ValidateRequestInterceptor() {
	}

	/**
	 * AroundInvoke method validating JAX-RS requests objects treat as method
	 * paraneters
	 * 
	 * @param ic
	 *            CDI Invocation contexts
	 * @return
	 * @throws Exception
	 *             throws exception if validation fails.
	 */
	@AroundInvoke
	public Object validateRequest(InvocationContext ic) throws Exception {
		Object[] parameters = ic.getParameters();
		if (parameters.length != 1) {
			throw new IllegalArgumentException("REST validator expects single parameter treating as JAX-RS request object");
		}

		Object restRequest = parameters[0];

		if (!PrepareCommunicationRequest.class.isInstance(restRequest) && !SendScriptRequest.class.isInstance(restRequest)) {
			throw new IllegalArgumentException(String.format("Unknown type of method parameter: %s", restRequest.getClass().getName()));
		}

		log.debug(String.format("Validating input parameter of type %1$s in method %2$s", restRequest.getClass().getSimpleName(), ic
		        .getMethod().getName()));

		ValidationHelper.validate(restRequest);

		return ic.proceed();
	}

}
