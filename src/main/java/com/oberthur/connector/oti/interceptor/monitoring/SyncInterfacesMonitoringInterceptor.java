/**
 * 
 */
package com.oberthur.connector.oti.interceptor.monitoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.api.xml.bind.notification.Notification;
import com.oberthur.connector.oti.domain.AverageResponseTime;
import com.oberthur.connector.oti.domain.AverageResponseTime_;
import com.oberthur.connector.oti.domain.Interfaces;
import com.oberthur.connector.oti.domain.Interfaces_;
import com.oberthur.connector.oti.domain.Requests;
import com.oberthur.connector.oti.domain.Requests_;
import com.oberthur.connector.oti.interceptor.monitoring.SyncInterfacesMonitoring.SyncInterfaceName;
import com.oberthur.connector.oti.monitoring.InterfacesNames;
import com.oberthur.connector.oti.monitoring.Monitoring;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvent;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;

/**
 * @author Illya Krakovskyy
 *
 */
@Interceptor
@SyncInterfacesMonitoring
public class SyncInterfacesMonitoringInterceptor implements Serializable {
	
	    /**
     * 
     */
    private static final long serialVersionUID = 8496958755963443374L;

    /**
     * Logging facility
     */
	public static final Logger log = LoggerFactory.getLogger(SyncInterfacesMonitoringInterceptor.class);

    @Inject
    @Monitoring
    private Event<OTICMonitoringEvent> event;

    @Inject
    private OTIPersistenceUtil persistenceUtil;

    @AroundInvoke
    public Object monitorSyncInterface(InvocationContext ictx) throws Exception {
        long startTime = System.currentTimeMillis();

        Object proceed = ictx.proceed();

        try {
	        if (Notification.class.isInstance(proceed)) {

	            Notification notification = (Notification) proceed;
	            long responseTime = System.currentTimeMillis() - startTime;

	            boolean isError = Notification.Status.FAIL == notification.getStatus();

	            EntityManager entityManager = persistenceUtil.getEntityManager();
	            SyncInterfacesMonitoring annotation = ictx.getMethod().getAnnotation(SyncInterfacesMonitoring.class);
	            SyncInterfaceName[] params = annotation.interfaceName();
	            if (AnnotationParamsValidator.validateParams(annotation.annotationType().getSimpleName(), params)) {
	                InterfacesNames interfaceName = params[0].getInterfaceName();

	                EntityTransaction transaction = entityManager.getTransaction();

	                CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
	                
	                transaction.begin();

	                populateRequestsNumber(isError, entityManager, interfaceName, criteriaBuilder);

	                populateAverageResponseTime(responseTime, entityManager, interfaceName, criteriaBuilder);

	                entityManager.clear();
	                transaction.commit();

	                entityManager.close();

	            }
	        }
        } catch (Exception e) {
        	log.error("Exception occured while intercepting: {}", e.getMessage(), e);
        } 
        return proceed;
    }

    /**
     * @param isError
     * @param entityManager
     * @param interfaceName
     * @param criteriaBuilder
     */
    private void populateRequestsNumber(boolean isError, EntityManager entityManager, InterfacesNames interfaceName, CriteriaBuilder criteriaBuilder) {
        CriteriaQuery<Requests> criteriaQuery = criteriaBuilder.createQuery(Requests.class);
        Root<Requests> root = criteriaQuery.from(Requests.class);

        Subquery<Interfaces> subquery = criteriaQuery.subquery(Interfaces.class);

        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get(Requests_.interfaceName), interfacesSubSelect(interfaceName, criteriaBuilder, subquery)));

        TypedQuery<Requests> theQuery = entityManager.createQuery(criteriaQuery);
        Requests requests;
        try {
            requests = theQuery.getSingleResult();
        } catch (NoResultException nre) {

        	requests = new Requests();            
            requests.setInterfaceName(findInterface(entityManager, interfaceName, criteriaBuilder));            
            
            entityManager.persist(requests);
        }

        if (!isError) {
            requests.setRequestsNumber(requests.getRequestsNumber().add(BigInteger.ONE));
        } else {
            requests.setErrorsNumber(requests.getErrorsNumber().add(BigInteger.ONE));
        }
        entityManager.flush();

        log.debug("Incoming requests statistics added for interface {}", interfaceName.toString());
    }

	/**
     * @param responseTime
     * @param entityManager
     * @param interfaceName
     * @param criteriaBuilder
     */
    private void populateAverageResponseTime(long responseTime, EntityManager entityManager, InterfacesNames interfaceName, CriteriaBuilder criteriaBuilder) {
        CriteriaQuery<AverageResponseTime> avgQuery = criteriaBuilder.createQuery(AverageResponseTime.class);
        Root<AverageResponseTime> avgRoot = avgQuery.from(AverageResponseTime.class);

        Subquery<Interfaces> sqInterfaces = avgQuery.subquery(Interfaces.class);

        avgQuery.select(avgRoot).where(
                criteriaBuilder.equal(avgRoot.get(AverageResponseTime_.interfaceName), interfacesSubSelect(interfaceName, criteriaBuilder, sqInterfaces)));

        TypedQuery<AverageResponseTime> avgTypedQuery = entityManager.createQuery(avgQuery);

        AverageResponseTime avgResponseTime;
        try {
            avgResponseTime = avgTypedQuery.getSingleResult();
        } catch (NoResultException e) {
            avgResponseTime = new AverageResponseTime();
            avgResponseTime.setInterfaceName(findInterface(entityManager, interfaceName, criteriaBuilder));
            
            entityManager.persist(avgResponseTime);            
        }

        BigInteger hitsCounter = avgResponseTime.getHitsCounter();
        BigInteger newHitsCounter = hitsCounter.add(BigInteger.ONE);
        if (hitsCounter.equals(BigInteger.ZERO)) {
            avgResponseTime.setAverageResponseTime(BigDecimal.valueOf(responseTime));
        } else {
            BigDecimal oldAverage = avgResponseTime.getAverageResponseTime();

            BigDecimal newAverage = oldAverage.multiply(BigDecimal.valueOf(hitsCounter.longValue())).add(BigDecimal.valueOf(responseTime))
                    .divide(BigDecimal.valueOf(newHitsCounter.longValue()), RoundingMode.HALF_UP);
            avgResponseTime.setAverageResponseTime(newAverage);
        }
        avgResponseTime.setHitsCounter(newHitsCounter);

        entityManager.flush();

        log.debug("Average response time calculated for interface {}", interfaceName);
    }

    /**
	 * @param entityManager
	 * @param interfaceName
	 * @param criteriaBuilder
	 * @return
	 */
	private Interfaces findInterface(EntityManager entityManager, InterfacesNames interfaceName, CriteriaBuilder criteriaBuilder) {
	    CriteriaQuery<Interfaces> interfacesQuery = criteriaBuilder.createQuery(Interfaces.class);
	    Root<Interfaces> interfacesRoot = interfacesQuery.from(Interfaces.class);
	    interfacesQuery.select(interfacesRoot).where(criteriaBuilder.equal(interfacesRoot.get(Interfaces_.interfaceName), interfaceName));
	    TypedQuery<Interfaces> intQuery = entityManager.createQuery(interfacesQuery);
	    return intQuery.getSingleResult();
	}

	/**
     * @param interfaceName
     * @param criteriaBuilder
     * @param subquery
     * @return
     */
    private Subquery<Interfaces> interfacesSubSelect(InterfacesNames interfaceName, CriteriaBuilder criteriaBuilder, Subquery<Interfaces> subquery) {
        Root<Interfaces> fromInterfaces = subquery.from(Interfaces.class);
        return subquery.select(fromInterfaces).where(criteriaBuilder.equal(fromInterfaces.get(Interfaces_.interfaceName), interfaceName));
    }

}
