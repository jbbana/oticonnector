package com.oberthur.connector.oti.interceptor;

import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.dto.SessionStatus;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;
import com.oberthur.connector.oti.service.notification.NotificationService;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 * Interceptor for the session initiation,
 * sends notification to the Notification Server about new session
 *
 * @author Michal Kaminski, OT R&D Poland
 */
@Interceptor
@ManageSession(status = SessionStatus.INITIATED)
public class SessionCreateInterceptor {

    @Inject
    NotificationService notificationService;

    @Inject
    private OTIPersistenceUtil persistenceUtil;

    @AroundInvoke
    public Object createSession(InvocationContext ic) throws Exception {
        Object proceed = ic.proceed();
        if (proceed instanceof SessionOTI) {
            SessionOTI session = (SessionOTI) proceed;
            notificationService.sendSessionStartNotification(session.getSecureElement(), session.getSecureElementType(), "" + session.getSessionId());
        }
        return proceed;
    }
}
