/**
 * 
 */
package com.oberthur.connector.oti.interceptor.monitoring;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Illya Krakovskyy
 *
 */
public class AnnotationParamsValidator {

    public static final Logger log = LoggerFactory.getLogger(AnnotationParamsValidator.class);

    /**
     * @param annotationName
     * 
     * @param annotationParams
     * @return
     */
    public static boolean validateParams(String annotationName, Object[] annotationParams) {
        if (annotationParams.length > 1) {
            log.error("Annotation {} parameterized in wrong way: the only parameter expected, but {}", annotationName, Arrays.toString(annotationParams));
        }
        return annotationParams.length == 1;
    }
}
