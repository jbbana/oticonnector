/**
 * 
 */
package com.oberthur.connector.oti.interceptor.monitoring;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.oberthur.connector.api.xml.bind.notification.Notification;
import com.oberthur.connector.oti.interceptor.monitoring.AsyncInterfacesMonitoring.AsyncInterfaceNames;
import com.oberthur.connector.oti.monitoring.Monitoring;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvent;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents;

/**
 * @author Illya Krakovskyy
 *
 */
@Interceptor
@AsyncInterfacesMonitoring
public class AsyncInterfaceMonitoringInterceptor {

    @Inject
    @Monitoring
    private Event<OTICMonitoringEvent> event;

    public Object monitorAsyncInterface(InvocationContext ictx) throws Exception {
        AsyncInterfacesMonitoring annotation = ictx.getMethod().getAnnotation(AsyncInterfacesMonitoring.class);

        Notification notification = (Notification) ictx.proceed(); // notification expected, otherwise CCE throwing expected

        if (AnnotationParamsValidator.validateParams(annotation.annotationType().getSimpleName(), annotation.asyncInterface())) {
            AsyncInterfaceNames interfaceName = annotation.asyncInterface()[0];
            event.fire(new OTICMonitoringEvent(Notification.Status.OK == notification.getStatus() ? OTICMonitoringEvents.REST_CALLBACK_SUCCESS
                    : OTICMonitoringEvents.REST_CALLBACK_FAILED, interfaceName.name()));
        }

        return notification;
    }

}
