package com.oberthur.connector.oti.interceptor;

import com.oberthur.connector.oti.dto.SessionStatus;

import javax.interceptor.InterceptorBinding;
import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;



@InterceptorBinding
@Inherited
@Target({ TYPE, METHOD })
@Retention(RUNTIME)
@Documented

/**
 * Session supervisor annotation to add some logic
 * to supervise the session lifecycle
 *
 * @author Michał Kamiński, OT R&D
 */
public @interface ManageSession {
    SessionStatus status();
}
