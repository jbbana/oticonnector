/**
 *
 */
package com.oberthur.connector.oti.interceptor;

import java.lang.reflect.Method;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.exceptions.OTIException;

/**
 * @author Illya Krakovskyy
 */
@Interceptor
@MethodLogged
public class MethodLogger {

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(MethodLogger.class);

	/**
     *
     */
	public MethodLogger() {
	}

	/**
	 * Method logger
	 *
	 * @param ic
	 *            invocation context
	 * @return
	 * @throws Exception
	 */
	@AroundInvoke
	public Object aroundInvoke(InvocationContext ic) throws Exception {
		Method executingMethod = ic.getMethod();

		String canonicalName = executingMethod.getDeclaringClass().getCanonicalName();
        String methodName = executingMethod.getName();

        log.debug("Executing method name is: {}.{}", canonicalName, methodName);
		try {
            return ic.proceed();
        } catch (Exception e) {
            if (!OTIException.class.isInstance(e)) {
                log.error("Exception occured while executing method {}.{}", canonicalName, methodName, e);
            }
            throw e;
        }
	}
}
