/**
 * 
 */
package com.oberthur.connector.oti.http;

import java.math.BigInteger;

import com.oberthur.connector.oti.config.OTIConfigKeys;

/**
 * @author Illya Krakovskyy HTTP part of OTI connector configuration keys
 */
public enum OTIHttpConfigKeys implements OTIConfigKeys {
    BASE_URL("tsm.https.base.url", "/se-connector-oti/por"),
    ORIGINATING_URL("tsm.originating.url", "http://127.0.0.1:8080/se-connector-oti/rest"),
    NODE_ID("tsm.node.id", "node01");

    /**
     * 
     */
    private String configKey;

    /**
     * 
     */
    private String defaultValue;

    /**
     * @param configKey
     * @param defaultValue
     */
    private OTIHttpConfigKeys(String configKey, String defaultValue) {
        this.configKey = configKey;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getConfigKey() {
        return configKey;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public BigInteger getNumericDefaultValue() {
        return null;
    }

}
