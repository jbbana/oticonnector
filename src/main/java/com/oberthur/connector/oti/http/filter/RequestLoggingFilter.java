package com.oberthur.connector.oti.http.filter;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebFilter(filterName = "requestLoggingFilter")
public class RequestLoggingFilter implements Filter {

	private static final String NEW_LINE = "\n";
	private static final String SEMICOLON = " : ";
	private static final String TAB = "\t";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		//
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if (log.isDebugEnabled()) {
			HttpServletRequest req = (HttpServletRequest) request;

			logRequest(req, req.getParameterNames(), new StringBuilder("Parameters:\n"), false);

			logRequest(req, req.getHeaderNames(), new StringBuilder("Headers:\n"), true);
		}

		chain.doFilter(request, response);
	}

	/**
	 * @param req
	 * @param parameters
	 * @param paramsBuilder
	 * @param headers
	 *            TODO
	 */
	private void logRequest(HttpServletRequest req, Enumeration<String> parameters, StringBuilder paramsBuilder, boolean headers) {
		while (parameters.hasMoreElements()) {
			String param = parameters.nextElement();
			paramsBuilder.append(TAB).append(param).append(SEMICOLON).append(headers ? req.getHeader(param) : req.getParameter(param))
			        .append(NEW_LINE);
		}
		log.debug(paramsBuilder.toString());

	}

	@Override
	public void destroy() {
		//
	}
}
