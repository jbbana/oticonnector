package com.oberthur.connector.oti.http.controller;

import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_PROTOCOL;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_PROTOCOL_HEADER;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.dto.PoRRequestDTO;
import com.oberthur.connector.oti.exceptions.OTIException;
import com.oberthur.connector.oti.exceptions.SendAsyncResponseException;
import com.oberthur.connector.oti.http.GPKeys;
import com.oberthur.connector.oti.http.OTIHttpUtils;
import com.oberthur.connector.oti.http.ScriptStatus;
import com.oberthur.connector.oti.http.bean.OTIControllerBean;
import com.oberthur.connector.oti.http.session.OTIAgentSession;
import com.oberthur.connector.oti.interceptor.monitoring.SyncInterfacesMonitoring;
import com.oberthur.connector.oti.interceptor.monitoring.SyncInterfacesMonitoring.SyncInterfaceName;
import com.oberthur.connector.oti.ram.state.AgentSessionState;

/**
 * Servlet implementation class PoRController
 */

@WebServlet(name = "por", urlPatterns = "/por")
@SessionScoped
@SyncInterfacesMonitoring(interfaceName = SyncInterfaceName.DEVICE_REQUEST)
public class OTIPoRController extends HttpServlet {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(OTIPoRController.class);

    @Inject
    private OTIControllerBean otiController;

    @Inject
    private OTIAgentSession agentSession;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public OTIPoRController() {
        super();
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("Call porResponse");

        Map<String, String[]> parameterMap = request.getParameterMap();

        long sessionID = Long.parseLong(parameterMap.get(GPKeys.SESSION_ID_PARAM.getGpKey())[0]);
        long transactionID = Long.parseLong(parameterMap.get(GPKeys.TRANSACTION_ID_PARAM.getGpKey())[0]);

        String agentId = request.getHeader(GPKeys.X_ADMIN_FROM_HEADER.getGpKey());
        String scriptStatusStr = request.getHeader(GPKeys.X_ADMIN_SCRIPT_STATUS.getGpKey());
        ScriptStatus scriptStatus = ScriptStatus.getScriptStatus(scriptStatusStr);

        byte[] content = new byte[request.getContentLength()];
        int numberOfBytesRead = request.getInputStream().read(content);

        boolean isEmptyContent = numberOfBytesRead == -1;
        boolean emptyAndOK = isEmptyContent && ScriptStatus.OK == scriptStatus;
        boolean notEmptyandNotOK = !isEmptyContent && ScriptStatus.OK != scriptStatus;

        if (emptyAndOK || notEmptyandNotOK) {
            response.setHeader(X_ADMIN_PROTOCOL_HEADER.getGpKey(), X_ADMIN_PROTOCOL.getGpKey());
            String errMsg = new StringBuilder("PoR failed: ").append(emptyAndOK ? "empty content and script status OK" : "not empty content and not script status OK").toString();
            log.error(errMsg);
            response.sendError(SC_BAD_REQUEST, errMsg);
        } else {

            try {
                PoRRequestDTO porRequest = PoRRequestDTO.getBuilderInstance(agentId, scriptStatus, BigInteger.valueOf(sessionID), transactionID, content).build();

                BigInteger otiSessionID = otiController.porResponse(porRequest);

                AgentSessionState releasedPoR = agentSession.pollPoRState(otiSessionID);

                try {
                    if (releasedPoR.isReleased()) {
                        OTIHttpUtils.sendScript(response, releasedPoR, otiSessionID);
                        releasedPoR.markSent();
                    } else {
                        OTIHttpUtils.dropCommunication(response);

                        // if session state discarded, end of lifecycle here
                        agentSession.removePoRSessionState(otiSessionID);
                    }

                } catch (IOException ioe) {
                    log.debug("PoR: Servlet response output stream has expired", ioe);
                    releasedPoR.markTimedOut();
                }
            } catch (SendAsyncResponseException sare) {
                log.debug("Async send script failed. {}", sare.getMessage());
            } catch (OTIException e) {
                log.debug("PoR failed: {}, drop communication", e.getMessage());
                OTIHttpUtils.dropCommunication(response);
            } catch (Exception e) {
                log.error("PoR failed: {}", e.getMessage());
                throw new ServletException(e);
            }
        }
    }
}
