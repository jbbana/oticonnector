/**
 *
 */
package com.oberthur.connector.oti.http;

import static com.oberthur.apdugenerator.utils.Utils.byteArrayToHexString;
import static com.oberthur.connector.oti.http.GPKeys.CONTENT_LENGTH_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.CONTENT_TYPE_CLOSE_RESPONSE;
import static com.oberthur.connector.oti.http.GPKeys.CONTENT_TYPE_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_NEXT_URI_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_PROTOCOL;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_PROTOCOL_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_SE_LIST_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_TARGETED_APPLICATION_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_TARGETED_SE;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_TARGETED_SE_HEADER;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_NO_CONTENT;
import static javax.servlet.http.HttpServletResponse.SC_OK;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.prefs.Preferences;
import java.util.regex.Pattern;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.api.SecureElementType;
import com.oberthur.connector.oti.dto.SEPair;
import com.oberthur.connector.oti.ram.state.AgentSessionState;

/**
 * @author Illya Krakovskyy
 */

public class OTIHttpUtils {

    private static final Logger log = LoggerFactory.getLogger(OTIHttpUtils.class);

	private static final String SE_PAIR_IDENTIFIER = "//se-id";
	private static final String SE_PAIRS_DELIMITER = ";";
	private static final int RID_LENGTH = 5;

	public static boolean validateSEList(String seListHeader) {

        log.debug("Validating X-Admin-SE-List header value {}", seListHeader);

		final String cplc_regexp = SE_PAIR_IDENTIFIER + "/\\D+/\\w+";
		final String iccid_regexp = SE_PAIR_IDENTIFIER + "/\\D+/[0-9A-Fa-f]+";

		boolean validSEList = false;
		if (seListHeader != null && seListHeader.trim().length() > 0) {
			String[] split = seListHeader.split(SE_PAIRS_DELIMITER);
			for (String s : split) {
				String pattern = s.startsWith(SE_PAIR_IDENTIFIER + "/" + SecureElementType.ICCID.toString()) ? iccid_regexp
				        : cplc_regexp;
				validSEList = Pattern.matches(pattern, s);
			}
		} else {
			log.warn("{} is missing or empty", X_ADMIN_SE_LIST_HEADER.getGpKey());
		}
		return validSEList;
	}

	public static List<SEPair> parseSEList(String seListHeader) {
		ArrayList<SEPair> sePairs = new ArrayList<>();
		if (validateSEList(seListHeader)) {
			String[] sePairsSplit = seListHeader.split(SE_PAIRS_DELIMITER);
			for (String sePair : sePairsSplit) {
				if (sePair.startsWith(SE_PAIR_IDENTIFIER)) {
					String[] seId = sePair.substring(SE_PAIR_IDENTIFIER.length()).split("/");
					String seIdTypeStr = seId[1];
					try {
						SecureElementType seIdType = SecureElementType.valueOf(seIdTypeStr);
						String seIdValue = seId[2];
						sePairs.add(new SEPair(seIdValue, seIdType));
					} catch (IllegalArgumentException iae) {
						log.warn(String.format("Unknown secure element ID type: %s", seIdTypeStr));
					}
				}
			}
		}
		return sePairs;
	}

	public static byte[] retrieveScriptParam(HttpServletRequest request) {
        Object scriptObj = request.getAttribute("script");
		if (scriptObj != null) {
			return (byte[]) scriptObj;
		}
		return new byte[0];
	}

	/**
	 * @param response
	 * @throws IOException
	 */
	public static void dropCommunication(HttpServletResponse response) throws IOException {
		response.addHeader(X_ADMIN_PROTOCOL_HEADER.getGpKey(), X_ADMIN_PROTOCOL.getGpKey());
		response.setContentType(CONTENT_TYPE_CLOSE_RESPONSE.getGpKey());
		response.setStatus(SC_NO_CONTENT);
		response.setContentLength(0);
	}

	/**
	 * @param response
	 * @param sessionState
	 * @param sessionId
	 * @throws IOException
	 */
	public static void sendScript(HttpServletResponse response, AgentSessionState sessionState, BigInteger sessionId) throws IOException {
		log.debug("Attempting to send script back to Mobile Agent");
		response.setContentType(CONTENT_TYPE_CLOSE_RESPONSE.getGpKey());
		response.setHeader(X_ADMIN_PROTOCOL_HEADER.getGpKey(), X_ADMIN_PROTOCOL.getGpKey());

		byte[] applicationId = sessionState.getApplicationId();
		int aidLength = applicationId.length;

		int scriptLength = sessionState.getScript().length;

		boolean isContent = scriptLength > 0 && aidLength > 0;

		if (isContent) {
			String targetSE = X_ADMIN_TARGETED_SE.getGpKey() + sessionState.getSeIdType().toString() + "/" + sessionState.getSeID();

            Preferences prefsNode = Preferences.userNodeForPackage(OTIHttpUtils.class);
            String baseUrl = prefsNode.get(OTIHttpConfigKeys.BASE_URL.getConfigKey(), OTIHttpConfigKeys.BASE_URL.getDefaultValue());
            String node = prefsNode.get(OTIHttpConfigKeys.NODE_ID.getConfigKey(), OTIHttpConfigKeys.NODE_ID.getDefaultValue());

            String adminNextURI = String.format("%1$s?nodeId=%2$s&sessionId=%3$s&transactionId=%4$s", baseUrl, node, sessionId, sessionState.getTransactionId());

			response.setHeader(X_ADMIN_NEXT_URI_HEADER.getGpKey(), adminNextURI);

			response.setHeader(X_ADMIN_TARGETED_SE_HEADER.getGpKey(), targetSE);

			if (aidLength >= RID_LENGTH) {
				StringBuilder sBuilder = new StringBuilder("//aid");
				byte[] ridArray = Arrays.copyOfRange(applicationId, 0, 5);
				sBuilder.append('/').append(byteArrayToHexString(ridArray)).append('/');

				if (aidLength > RID_LENGTH) {
					byte[] pixArray = Arrays.copyOfRange(applicationId, 5, aidLength);
					sBuilder.append(byteArrayToHexString(pixArray));
				}

				response.setHeader(X_ADMIN_TARGETED_APPLICATION_HEADER.getGpKey(), sBuilder.toString());
			}
			response.setContentLength(sessionState.getScript().length);

			response.setStatus(SC_OK);

			final String msg_param_tmpl = "%s: {}\n";

			String script = byteArrayToHexString(sessionState.getScript());

			log.debug(
			        new StringBuilder("SendScriptResponse to mobile agent:\n ")
			                .append(String.format(msg_param_tmpl, X_ADMIN_PROTOCOL_HEADER.getGpKey()))
			                .append(String.format(msg_param_tmpl, X_ADMIN_NEXT_URI_HEADER.getGpKey()))
			                .append(String.format(msg_param_tmpl, X_ADMIN_TARGETED_SE_HEADER.getGpKey()))
			                .append(String.format(msg_param_tmpl, X_ADMIN_TARGETED_APPLICATION_HEADER.getGpKey()))
			                .append(String.format(msg_param_tmpl, CONTENT_TYPE_HEADER.getGpKey()))
			                .append(String.format(msg_param_tmpl, CONTENT_LENGTH_HEADER.getGpKey()))
			                .append(String.format(msg_param_tmpl, "Script")).append(String.format(msg_param_tmpl, "Status Code"))
			                .toString(), response.getHeader(X_ADMIN_PROTOCOL_HEADER.getGpKey()),
			        response.getHeader(X_ADMIN_NEXT_URI_HEADER.getGpKey()), response.getHeader(X_ADMIN_TARGETED_SE_HEADER.getGpKey()),
			        response.getHeader(X_ADMIN_TARGETED_APPLICATION_HEADER.getGpKey()), GPKeys.CONTENT_TYPE_CLOSE_RESPONSE.getGpKey(),
			        scriptLength, script, new StringBuilder(String.valueOf(SC_OK)));
		} else {
			log.debug("Response 204 sent back to mobile agent");
			response.setContentLength(0);
			response.setStatus(SC_BAD_REQUEST);
		}

		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(sessionState.getScript());

	}
}
