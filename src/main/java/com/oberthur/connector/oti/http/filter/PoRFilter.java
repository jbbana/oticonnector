package com.oberthur.connector.oti.http.filter;

import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_FROM_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_PROTOCOL_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_SCRIPT_STATUS;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import com.oberthur.connector.oti.http.GPKeys;
import com.oberthur.connector.oti.http.ScriptStatus;

/**
 * Servlet Filter implementation class PoRFiletr
 */
@Slf4j
@WebFilter(filterName = "poRFilter", urlPatterns = "/por")
public class PoRFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public PoRFilter() {
	}

	/**
	 * @see Filter#destroy()
	 */
	@Override
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		String scriptStatus = httpRequest.getHeader(X_ADMIN_SCRIPT_STATUS.getGpKey());

		String sessionId = httpRequest.getParameter(GPKeys.SESSION_ID_PARAM.getGpKey());
		String transactionId = httpRequest.getParameter(GPKeys.TRANSACTION_ID_PARAM.getGpKey());
		String nodeId = httpRequest.getParameter(GPKeys.NODE_ID_PARAM.getGpKey());

		boolean xAdminProtocolFailed = httpRequest.getHeader(X_ADMIN_PROTOCOL_HEADER.getGpKey()) == null;
		boolean xAdminFromFailed = httpRequest.getHeader(X_ADMIN_FROM_HEADER.getGpKey()) == null;
		boolean scriptStatusFailed = scriptStatus == null || !ScriptStatus.isValidScriptStatus(scriptStatus);
		boolean sessionIdFailed = sessionId == null || !Pattern.matches("\\d+", sessionId);
		boolean transactionIdFailed = transactionId == null || !Pattern.matches("\\d+", transactionId);
		boolean nodeIdMissed = nodeId == null || nodeId.trim().isEmpty();

		boolean invalidReq = xAdminProtocolFailed || xAdminFromFailed || scriptStatusFailed || sessionIdFailed || nodeIdMissed
		        || transactionIdFailed;

		if (invalidReq) {
			StringBuilder errBuilder = new StringBuilder("PoR request validation failed: ");

			if (xAdminProtocolFailed) {
				errBuilder.append(X_ADMIN_PROTOCOL_HEADER.getGpKey()).append(" header is missing; ");
			} else if (xAdminFromFailed) {
				errBuilder.append(X_ADMIN_FROM_HEADER.getGpKey()).append(" header is missing; ");
			} else if (scriptStatusFailed) {
				errBuilder.append(X_ADMIN_SCRIPT_STATUS.getGpKey()).append(" is missing; ");
			} else if (sessionIdFailed) {
				errBuilder.append("Parameter sessionId missed or invalid");
			} else if (nodeIdMissed) {
				errBuilder.append("Parameter nodeId missed");
			} else {
				errBuilder.append("Parameter transactionId missed or invalid");
			}

			String errMsg = errBuilder.toString();
			log.debug(errMsg);
			httpResponse.sendError(SC_BAD_REQUEST, errMsg);
		} else {
			chain.doFilter(request, response);

		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	@Override
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
