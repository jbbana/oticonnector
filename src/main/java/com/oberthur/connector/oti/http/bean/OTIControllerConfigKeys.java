/**
 * 
 */
package com.oberthur.connector.oti.http.bean;

import java.math.BigInteger;

import com.oberthur.connector.oti.config.OTIConfigKeys;

/**
 * @author Illya Krakovskyy
 *
 */
public enum OTIControllerConfigKeys implements OTIConfigKeys {
    OUT_OF_SESSION_NOTIFICATION_EXPECTED("tsm.outofsession.notification.expected", "false"),
    ORIGINATING_URL("tsm.originating.url", "http://172.16.108.178:8080/se-connector-oti/rest"),
    CPLC_18BYTES_IDENT("tsm.cplc.18bytes.ident", "true");

    private String configKey;
    /**
     * 
     */
    private String defaultValue;

    /**
     * @param configKey
     * @param defaultValue
     */
    private OTIControllerConfigKeys(String configKey, String defaultValue) {
        this.configKey = configKey;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getConfigKey() {
        return configKey;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public BigInteger getNumericDefaultValue() {
        return null;
    }

}
