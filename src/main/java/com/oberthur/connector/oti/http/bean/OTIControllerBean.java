/**
 * 
 */
package com.oberthur.connector.oti.http.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.prefs.Preferences;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.apdugenerator.exception.ParseException;
import com.oberthur.apdugenerator.expandedmode.ExpandedModeResponse;
import com.oberthur.apdugenerator.response.RapduStatusCode;
import com.oberthur.apdugenerator.utils.Utils;
import com.oberthur.connector.api.OutOfSessionParamEnum;
import com.oberthur.connector.api.RequestedScriptFormatEnum;
import com.oberthur.connector.api.ReturnedStatus;
import com.oberthur.connector.api.SecureElementType;
import com.oberthur.connector.api.xml.bind.notification.Notification;
import com.oberthur.connector.api.xml.bind.response.OutOfSessionResponse;
import com.oberthur.connector.api.xml.bind.response.PrepareCommunicationResponse;
import com.oberthur.connector.api.xml.bind.response.SendScriptResponse;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.domain.SessionOTI_;
import com.oberthur.connector.oti.dto.AuthenticationDTO;
import com.oberthur.connector.oti.dto.PoRRequestDTO;
import com.oberthur.connector.oti.dto.SEPair;
import com.oberthur.connector.oti.dto.SessionStatus;
import com.oberthur.connector.oti.exceptions.DuplicateStateException;
import com.oberthur.connector.oti.exceptions.MissingSessionException;
import com.oberthur.connector.oti.exceptions.MissingSessionException.MissingExceptionReason;
import com.oberthur.connector.oti.exceptions.OTIAuthenticationException;
import com.oberthur.connector.oti.exceptions.OTIAuthenticationException.AuthenticationFailReason;
import com.oberthur.connector.oti.exceptions.OTIException;
import com.oberthur.connector.oti.exceptions.SendAsyncResponseException;
import com.oberthur.connector.oti.exceptions.SendAsyncResponseException.CallbackInterfaceFailed;
import com.oberthur.connector.oti.http.session.OTIAgentSession;
import com.oberthur.connector.oti.interceptor.MethodLogged;
import com.oberthur.connector.oti.interceptor.ScheduleSession;
import com.oberthur.connector.oti.interceptor.ScheduleSession.ScheduleType;
import com.oberthur.connector.oti.interceptor.monitoring.DataAccessMonitoring;
import com.oberthur.connector.oti.ram.state.AgentSessionState;
import com.oberthur.connector.oti.rest.ConnectorServiceCallbackProxy;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;

/**
 * @author Illya Krakovskyy
 *
 *         HTTP controllers backend bean
 */
@RequestScoped
@MethodLogged
@DataAccessMonitoring
public class OTIControllerBean implements Serializable {

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(OTIControllerBean.class);

    /**
	 * 
	 */
    private static final long serialVersionUID = -5689331847695377383L;

    @Inject
    private OTIPersistenceUtil persistenceUtil;

    @Inject
    private OTIAgentSession agentSession;

    @Inject
    private ConnectorServiceCallbackProxy callback;

    private EntityManager entityManager;

    /**
	 * 
	 */
    @PostConstruct
    public void initPersistence() {
        if (this.entityManager == null || !this.entityManager.isOpen()) {
            this.entityManager = persistenceUtil.getEntityManager();
        }
    }

    /**
	 * 
	 */
    @PreDestroy
    public void closePersistence() {
        if (this.entityManager != null && this.entityManager.isOpen()) {
            this.entityManager.close();
        }
    }

    /**
     * @param authDTO
     * @throws OTIAuthenticationException
     */
    public void authenticate(AuthenticationDTO authDTO) throws OTIAuthenticationException {

        String login = authDTO.getLogin();
        String passwd = authDTO.getPasswd();
        String agentID = authDTO.getAgentID();
        List<SEPair> sePairs = authDTO.getSePairs();

        SessionOTI session;
        try {
            session = findOTISession(agentID, sePairs);
        } catch (NoResultException nre) {
            throw new OTIAuthenticationException(AuthenticationFailReason.SESSION_NOT_FOUND);
        }

        if (session != null && !(login.equals(session.getLogin()) && passwd.equals(session.getPassword()))) {
            throw new OTIAuthenticationException(AuthenticationFailReason.LOGIN_PASSWD_MISMATCH);
        }
        if (session != null && SessionStatus.TO_BE_PURGED == session.getStatus()) {
            throw new OTIAuthenticationException(AuthenticationFailReason.SESSION_TO_BE_PURGED);
        }
    }

    /**
     * @param sePairs
     * @param agentID
     * @return
     * @throws DuplicateStateException
     * @throws SendAsyncResponseException
     * @throws MissingSessionException
     * @throws OTIException
     */
    @ScheduleSession(scheduleType = ScheduleType.UNREGISTER_PURGE)
    public BigInteger prepareCommunicationRequest(List<SEPair> sePairs, String agentID) throws DuplicateStateException, SendAsyncResponseException, MissingSessionException {
        EntityTransaction transaction = entityManager.getTransaction();
        try {

            SessionOTI sessionOTI = findOTISession(agentID, sePairs);

            BigInteger otiSessionId = sessionOTI.getId();
            if (agentSession.getRamSessions().get(otiSessionId) != null) {
                throw new DuplicateStateException(String.format("HTTP RAM Session with OTI Session Id %s already exists in HTTP session states", otiSessionId.toString()));
            }

            transaction.begin();

            SessionStatus status = sessionOTI.getStatus();
            boolean isInitiated = SessionStatus.INITIATED == status;
            if (isInitiated) {
                sessionOTI.setStatus(SessionStatus.OPENED);
                sessionOTI.setUpdatedDate(new Timestamp(System.currentTimeMillis()));

                if (sessionOTI.getPurgeDate().before(new Date())) {
                    log.info("maxResponseDelay was smaller that the period of Mobile Agent regular PULL requesting."
                            + " Purge Procedure Period was long enough to receive HTTP POST RAM request from Mobile Agent before Purge procedure is started."
                            + " Thus the Session is opened, and will not be purged");
                }
                sessionOTI.setPurgeDate(null);
            }

            entityManager.flush();
            entityManager.clear();

            transaction.commit();

            if (isInitiated) {
                String sessionID = String.valueOf(sessionOTI.getSessionId());
                BigInteger conversationID = sessionOTI.getSessionId();

                PrepareCommunicationResponse response = new PrepareCommunicationResponse(sessionID);
                response.setConversationId(conversationID);
                response.setScriptFormat(RequestedScriptFormatEnum.EXPANDED);
                response.setSessionStatus(ReturnedStatus.OK);

                log.debug("Sending prepareCommunication response to sequencer: " + "\nSession ID: {}" + "\nConversation ID: {}" + "\nUsed bearer: {}" + "\nScript format: {}",
                        sessionID, conversationID.toString(), response.getUsedBearer(), response.getScriptFormat().toString());
                Notification pcrNotification = callback.prepareCommunicationResponse(response);
                if (Notification.Status.FAIL == pcrNotification.getStatus()) {
                    throw new SendAsyncResponseException(CallbackInterfaceFailed.PREPARE_COMMUNICATION, pcrNotification.getReason());
                }
            }

            agentSession.putRAMSessionState(otiSessionId);

            return otiSessionId;
        } catch (NoResultException nre) {
            String msg = String.format("Session not found for security element pairs %s and/or %s", sePairs.toString(), agentID);
            log.debug(msg);
            throw new MissingSessionException(msg, MissingExceptionReason.NO_SESSION_FOR_SE_IDS);
        }
    }

    /**
     * @param porRequest
     * @return
     * @throws DuplicateStateException
     * @throws OTIException
     * @throws ParseException
     */
    @ScheduleSession(scheduleType = ScheduleType.UNREGISTER_CLEAN)
    public BigInteger porResponse(PoRRequestDTO porRequest) throws OTIException {
        EntityTransaction transaction = entityManager.getTransaction();

        BigInteger otiSessionID = porRequest.getSessionID();
        if (agentSession.getPorSessions().get(otiSessionID) != null) {
            throw new DuplicateStateException(String.format("HTTP PoR session with OTI Session Id %s already exists in HTTP session states", otiSessionID.toString()));
        }

        Long transactionId = porRequest.getTransactionID();
        byte[] content = porRequest.getContent();
        String agentId = porRequest.getAgentId();

        log.debug("Async Response from agent: \nSession Id: {}\nPoR: {}", otiSessionID, Utils.byteArrayToHexString(content));

        try {
            SessionOTI sessionOTI = findOTISessionById(otiSessionID);

            if (transactionId.equals(sessionOTI.getTransactionId())) {

                AgentSessionState porSessionState = agentSession.putPoRSessionState(otiSessionID);

                if (content.length == 0) {
                    SendScriptResponse response = fillResponse(sessionOTI, porRequest.getScriptStatus().getReturnedStatus());
                    callback.sendScriptResponse(response);
                } else {
                    SendScriptResponse response = fillResponse(sessionOTI, content);

                    transaction.begin();
                    if (SessionStatus.TO_BE_PURGED == sessionOTI.getStatus()) {
                        entityManager.remove(sessionOTI);
                        porSessionState.markOutOfSession();
                    } else {
                        sessionOTI.setTransactionId(null);
                        sessionOTI.setValidDate(null);
                    }

                    entityManager.flush();
                    entityManager.clear();

                    transaction.commit();

                    Notification ssrNotification = callback.sendScriptResponse(response);
                    if (Notification.Status.FAIL == ssrNotification.getStatus()) {
                        throw new SendAsyncResponseException(CallbackInterfaceFailed.SEND_SCRIPT, ssrNotification.getReason());
                    }
                }

            } else {
                String msg = String.format("Transaction ID mismatch for session id %s ", otiSessionID.toString());
                log.debug(msg);
                throw new OTIException(msg);
            }

            return otiSessionID;
        } catch (NoResultException nre) {
            String oosStr = Preferences.userNodeForPackage(this.getClass()).get(OTIControllerConfigKeys.OUT_OF_SESSION_NOTIFICATION_EXPECTED.getConfigKey(),
                    OTIControllerConfigKeys.OUT_OF_SESSION_NOTIFICATION_EXPECTED.getDefaultValue());
            if (Boolean.parseBoolean(oosStr)) {
                sendOutOfSessionNotification(content, agentId);
            }

            throw new MissingSessionException(String.format("Session not found for session Id %s", otiSessionID), MissingExceptionReason.SESSION_MISSED);
        }
    }

    /**
     * @param agentID
     * @param sePairs
     * @return
     */
    private SessionOTI findOTISession(String agentID, List<SEPair> sePairs) throws NoResultException {
        boolean isCPLCSubstr = Preferences.userNodeForPackage(getClass()).getBoolean(OTIControllerConfigKeys.CPLC_18BYTES_IDENT.getConfigKey(),
                Boolean.parseBoolean(OTIControllerConfigKeys.CPLC_18BYTES_IDENT.getDefaultValue()));

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<SessionOTI> criteriaQuery = criteriaBuilder.createQuery(SessionOTI.class);
        Root<SessionOTI> sessionRoot = criteriaQuery.from(SessionOTI.class);

        List<Predicate> sePredicates = new ArrayList<>();

        // Assuming validation already made, so either SE list or Agent ID
        // exists in DTO
        boolean isOnSEFailed = false;
        if (!sePairs.isEmpty()) {
            for (SEPair sePair : sePairs) {
                SecureElementType seIDType = sePair.getSeIDType();
                String seID = sePair.getSeID();

                boolean isTruncTo18Bytes = isCPLCSubstr && SecureElementType.CPLC == seIDType && seID.length() > 36;

                Expression<String> seExpr = isTruncTo18Bytes
                        ? criteriaBuilder.substring(sessionRoot.get(SessionOTI_.secureElement), 1, 36)
                        : sessionRoot.get(SessionOTI_.secureElement);
                String seVal = isTruncTo18Bytes ? seID.substring(0, 36) : seID;

                Predicate sePredicate = criteriaBuilder.and(criteriaBuilder.equal(seExpr, seVal), criteriaBuilder.equal(sessionRoot.get(SessionOTI_.secureElementType), seIDType));
                sePredicates.add(sePredicate);
            }
            try {
                // OTI-963: if more than one session returned by pairs
                // {se_id:se_id_type}, bring the first one.
                return entityManager.createQuery(criteriaQuery.select(sessionRoot).where(criteriaBuilder.or(sePredicates.toArray(new Predicate[sePredicates.size()]))))
                        .setFirstResult(0).setMaxResults(1).getSingleResult();
            } catch (NoResultException nre) {
                // Session not found by SE ID/SE Type pairs, use agent ID
                // instead
                isOnSEFailed = true;
            }
        }

        if (agentID != null && (isOnSEFailed || sePairs.isEmpty())) {
            try {
                return entityManager.createQuery(criteriaQuery.where(criteriaBuilder.equal(sessionRoot.get(SessionOTI_.agentId), agentID))).getSingleResult();
            } catch (NoResultException nre) {
                log.debug("No session found for agent ID " + agentID);
                throw nre;
            }
        } else {
            return null;
        }
    }

    /**
     * Finds an OTI session entity by ID
     * 
     * @param id
     *            - OTI session ID
     * @return OTI session
     */
    private SessionOTI findOTISessionById(final BigInteger id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<SessionOTI> criteriaQuery = criteriaBuilder.createQuery(SessionOTI.class);
        Root<SessionOTI> sessionRoot = criteriaQuery.from(SessionOTI.class);
        try {
            return entityManager.createQuery(criteriaQuery.select(sessionRoot).where(criteriaBuilder.equal(sessionRoot.get(SessionOTI_.id), id))).getSingleResult();
        } catch (NoResultException nre) {
            log.debug("No session found for ID " + id);
            throw nre;
        }
    }

    /**
     * @param sessionOTI
     * @param content
     * @return
     * @throws ParseException
     */
    private SendScriptResponse fillResponse(SessionOTI sessionOTI, byte[] content) throws OTIException {
        SendScriptResponse response = new SendScriptResponse(sessionOTI.getTransactionId());
        response.setConversationId(sessionOTI.getSessionId());

        List<byte[]> responseAPDU = getResponsesList(content);

        byte[] sw = getSw(responseAPDU);
        response.setFailedAPDUIndex(Arrays.equals(sw, Utils.shortToBytes(RapduStatusCode.CODE_9000.getCode())) ? null : responseAPDU.size());
        response.setSw(sw);
        List<byte[]> resp = new ArrayList<>(1);
        resp.add(content);
        response.setResponseAPDU(resp);
        response.setDeliveryStatus(ReturnedStatus.OK);
        return response;
    }

    /**
     * @param sessionOTI
     * @param status
     * @return
     */
    private SendScriptResponse fillResponse(SessionOTI sessionOTI, ReturnedStatus status) {
        SendScriptResponse response = new SendScriptResponse(sessionOTI.getTransactionId());
        response.setConversationId(sessionOTI.getSessionId());
        response.setDeliveryStatus(status);
        return response;
    }

    /**
     * @param content
     * @param agentId
     * @throws ParseException
     */
    private void sendOutOfSessionNotification(byte[] content, String agentId) throws OTIException {
        log.debug("Sending out-of-session notification");
        List<byte[]> listOfResponses = getResponsesList(content);

        String originatingUrl = Preferences.userNodeForPackage(getClass()).get(OTIControllerConfigKeys.ORIGINATING_URL.getConfigKey(),
                OTIControllerConfigKeys.ORIGINATING_URL.getDefaultValue());

        OutOfSessionResponse theResponse = new OutOfSessionResponse(originatingUrl, listOfResponses.get(0));
        theResponse.setSourceId(Collections.singletonMap(OutOfSessionParamEnum.AGENTID, agentId));
        callback.outOfSessionResponse(theResponse);
    }

    /**
     * Get list of responses from APDU expanded mode parser
     * 
     * @param content
     * @return
     * @throws ParseException
     */
    private List<byte[]> getResponsesList(byte[] content) throws OTIException {
        try {
            ExpandedModeResponse.Parser expandedResponse = new ExpandedModeResponse.Parser(content);
            List<byte[]> listOfResponses = expandedResponse.getListOfResponses();
            return listOfResponses;
        } catch (ParseException e) {
            throw new OTIException(e);
        }
    }

    private static byte[] getSw(List<byte[]> responseAPDU) {
        if (responseAPDU.isEmpty() && responseAPDU.size() < 1) {
            return null;
        }
        byte[] lastResponse = responseAPDU.get(responseAPDU.size() - 1);
        return lastResponse == null || lastResponse.length < 2 ? null : new byte[] { lastResponse[lastResponse.length - 2], lastResponse[lastResponse.length - 1] };
    }
}
