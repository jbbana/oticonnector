package com.oberthur.connector.oti.http;

import com.oberthur.connector.api.ReturnedStatus;

public enum ScriptStatus {

	OK("ok", ReturnedStatus.OK),
	UNKNOWN_APPLICATION("unknown-application", ReturnedStatus.RAM_HTTP_UNKNOWN_APPLICATION),
	NOT_A_SECURITY_DOMAIN("not-a-security-domain", ReturnedStatus.RAM_HTTP_NOT_A_SECURITY_DOMAIN),
	SECURITY_ERROR("security-error", ReturnedStatus.RAM_HTTP_SECURITY_ERROR),
	UNAVAILABLE_SE("unavailable-se", ReturnedStatus.SE_RAM_HTTP_UNAVAILABLE_SE);

	private final String value;
	private final ReturnedStatus status;

	private ScriptStatus(String value, ReturnedStatus status) {
		this.value = value;
		this.status = status;
	}

	public String getValue() {
		return value;
	}

	public ReturnedStatus getReturnedStatus() {
		return status;
	}

	public static ScriptStatus getScriptStatus(String value) {
		for (ScriptStatus scriptStatus : values()) {
			if (scriptStatus.value.equals(value)) {
				return scriptStatus;
			}
		}
		throw new IllegalArgumentException("Unknown status with value: " + value);
	}

	public static boolean isValidScriptStatus(String value) {
		for (ScriptStatus scriptStatus : values()) {
			if (scriptStatus.value.equals(value)) {
				return true;
			}
		}
		return false;
	}
}
