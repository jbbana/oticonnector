/**
 * 
 */
package com.oberthur.connector.oti.http.session;

import static com.oberthur.connector.oti.exceptions.MissingSessionException.MissingExceptionReason.HTTP_SESSION_MISSED;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import javax.enterprise.context.ApplicationScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.exceptions.MissingSessionException;
import com.oberthur.connector.oti.exceptions.OTIException;
import com.oberthur.connector.oti.ram.state.AgentSessionState;

/**
 * @author Illya Krakovskyy
 * 
 *         Singleton containing "locked" HTTP sessions from Mobile Agent to OTI
 *         Connector
 *
 */
@ApplicationScoped
public class OTIAgentSession implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4836327916878054273L;

    /**
     * Slf4j logger
     */
    private static final Logger log = LoggerFactory.getLogger(OTIAgentSession.class);

    /**
     * PoR sessions states
     */
    private Map<BigInteger, AgentSessionState> porSessions = new ConcurrentHashMap<BigInteger, AgentSessionState>();
    /**
     * RAM sessions states
     */
    private Map<BigInteger, AgentSessionState> ramSessions = new ConcurrentHashMap<BigInteger, AgentSessionState>();

    /**
     * Checks whether the session state given exists at all for both RAM/PoR
     * session states
     * 
     * @param sessionId
     * @return true if any of session states contains <code>sessionId</code>
     */
    public boolean agentSessionStateExists(BigInteger sessionId) {
        return ramSessions.containsKey(sessionId) || porSessions.containsKey(sessionId);
    }

    /**
     * Get agent session state by its unique id, from RAM or PoR session states,
     * depending which session states map contains the id
     * 
     * @param sessionId
     *            @return @throws
     */
    public AgentSessionState getAgentSessionState(BigInteger sessionId) throws MissingSessionException {
        AgentSessionState agentSessionState = ramSessions.containsKey(sessionId) ? ramSessions.get(sessionId)
                : porSessions.get(sessionId);
        if (agentSessionState == null) {
            throw new MissingSessionException(
                    String.format("Agent session state with session id %s not found within session states", sessionId),
                    HTTP_SESSION_MISSED);
        } else {
            return agentSessionState;
        }
    }

    /**
     * @param sessionId
     * @return
     * @throws MissingSessionException
     */
    public AgentSessionState pollForSentState(BigInteger sessionId) throws MissingSessionException {
        boolean isRam = ramSessions.containsKey(sessionId);
        Map<BigInteger, AgentSessionState> sessionStates = isRam ? ramSessions : porSessions;

        while (sessionStates.get(sessionId) != null) {
            AgentSessionState agentSessionState = sessionStates.get(sessionId);
            if (agentSessionState.isSent() || agentSessionState.isTimedOut()) {
                return agentSessionState;
            }
        }

        throw new MissingSessionException(
                String.format("Agent session with ID %s does not exist in %s sessions map", sessionId, isRam ? "RAM" : "PoR"),
                HTTP_SESSION_MISSED);
    }

    /**
     * @param sessionId
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public AgentSessionState pollRAMState(BigInteger sessionId) throws OTIException {
        return getReleasedState(sessionId, ramSessions);
    }

    /**
     * @param sessionId
     * @return
     * @throws OTIException
     */
    public AgentSessionState pollPoRState(BigInteger sessionId) throws OTIException {
        return getReleasedState(sessionId, porSessions);
    }

    /**
     * @param sessionId
     * @return
     */
    public AgentSessionState putRAMSessionState(BigInteger sessionId) {
        ramSessions.put(sessionId, new AgentSessionState());
        return ramSessions.get(sessionId);
    }

    /**
     * @param sessionId
     * @return
     */
    public AgentSessionState putPoRSessionState(BigInteger sessionId) {
        porSessions.put(sessionId, new AgentSessionState());
        return porSessions.get(sessionId);
    }

    /**
     * Remove session state designated with <code>sessionId</code>
     * 
     * @param sessionId
     */
    public void removeSessionState(BigInteger sessionId) {
        if (ramSessions.containsKey(sessionId)) {
            removeRAMSessionState(sessionId);
        } else {
            removePoRSessionState(sessionId);
        }
    }

    /**
     * @param sessionId
     * @return
     */
    public AgentSessionState removeRAMSessionState(BigInteger sessionId) {
        log.debug("Removing RAM session with ID {}", sessionId);
        return ramSessions.remove(sessionId);
    }

    /**
     * @param sessionId
     * @return
     */
    public AgentSessionState removePoRSessionState(BigInteger sessionId) {
        log.debug("Removing PoR session with ID {}", sessionId);
        return porSessions.remove(sessionId);
    }

    /**
     * @return the porSessions
     */
    public Map<BigInteger, AgentSessionState> getPorSessions() {
        return porSessions;
    }

    /**
     * @return the ramSessions
     */
    public Map<BigInteger, AgentSessionState> getRamSessions() {
        return ramSessions;
    }

    /**
     * @param sessionId
     * @param sessionStates
     * @return
     * @throws OTIException
     */
    private AgentSessionState getReleasedState(final BigInteger sessionId, final Map<BigInteger, AgentSessionState> sessionStates)
            throws OTIException {
        while (sessionStates.get(sessionId) != null) {
            if (sessionStates.get(sessionId).isReleased() || sessionStates.get(sessionId).isDiscarded()) {
                log.debug("Session with ID {}: poll is over", sessionId);
                return sessionStates.get(sessionId);
            }
        }
        throw new MissingSessionException(String.format("Agent session with ID %s does not exist in session states map", sessionId),
                HTTP_SESSION_MISSED);
    }
}
