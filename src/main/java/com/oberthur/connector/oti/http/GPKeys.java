package com.oberthur.connector.oti.http;

public enum GPKeys {
	NODE_ID_PARAM("nodeId"),
	SESSION_ID_PARAM("sessionId"),
	TRANSACTION_ID_PARAM("transactionId"),

	X_ADMIN_PROTOCOL("globalplatform-remote-admin/1.1"),
	X_ADMIN_TARGETED_SE("//se-id/"),

	CONTENT_TYPE_REQUEST("application/vnd.globalplatform.card-content-mgt-response;version=1.0"),
	CONTENT_TYPE_CLOSE_RESPONSE("application/vnd.globalplatform.card-content-mgt;version=1.0"),

	X_ADMIN_PROTOCOL_HEADER("X-Admin-Protocol"),
	X_ADMIN_SE_LIST_HEADER("X-Admin-SE-List"),
	X_ADMIN_TARGETED_SE_HEADER("X-Admin-Targeted-SE"),
	X_ADMIN_NEXT_URI_HEADER("X-Admin-Next-URI"),
	CONTENT_TYPE_HEADER("Content-Type"),
	CONTENT_LENGTH_HEADER("Content-Length"),
	X_ADMIN_TARGETED_APPLICATION_HEADER("X-Admin-Targeted-Application"),
	X_ADMIN_SCRIPT_STATUS("X-Admin-Script-Status"),
	X_ADMIN_FROM_HEADER("X-Admin-From"),
	X_ADMIN_RESUME_HEADER("X-Admin-Resume"),
	AUTHORIZATION_HEADER("Authorization");

	private String gpKey;

	private GPKeys(String gpKey) {
		this.gpKey = gpKey;
	}

	public String getGpKey() {
		return gpKey;
	}
}