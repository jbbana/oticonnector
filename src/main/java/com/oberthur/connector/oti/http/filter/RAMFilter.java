package com.oberthur.connector.oti.http.filter;

import static com.oberthur.connector.oti.http.GPKeys.AUTHORIZATION_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_FROM_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_PROTOCOL;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_PROTOCOL_HEADER;
import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_SE_LIST_HEADER;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;
import static javax.servlet.http.HttpServletResponse.SC_NO_CONTENT;
import static javax.servlet.http.HttpServletResponse.SC_PRECONDITION_FAILED;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.dto.AuthenticationDTO;
import com.oberthur.connector.oti.dto.SEPair;
import com.oberthur.connector.oti.exceptions.OTIAuthenticationException;
import com.oberthur.connector.oti.exceptions.OTIAuthenticationException.AuthenticationFailReason;
import com.oberthur.connector.oti.http.OTIHttpUtils;
import com.oberthur.connector.oti.http.bean.OTIControllerBean;

/**
 * Servlet Filter implementation class RAMAuthFilter
 */
@SessionScoped
@WebFilter(filterName = "ramFilter", urlPatterns = "/ram")
public class RAMFilter implements Filter, Serializable {

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(RAMFilter.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 3062661110248983956L;

	@Inject
	private OTIControllerBean otiControllerBean;

	private static final String BASIC_AUTHENTICATION_PREFIX = "Basic ";

	public RAMFilter() {
	}

	@Override
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		// Validate headers

		String seListHeader = req.getHeader(X_ADMIN_SE_LIST_HEADER.getGpKey());
		String agentID = req.getHeader(X_ADMIN_FROM_HEADER.getGpKey());
		String authData = req.getHeader(AUTHORIZATION_HEADER.getGpKey());

		boolean authFailed = authData == null;
		boolean agentIDFailed = agentID == null;
		boolean xAdminProtocolFailed = req.getHeader(X_ADMIN_PROTOCOL_HEADER.getGpKey()) == null;

		boolean seListFailed = seListHeader != null && !OTIHttpUtils.validateSEList(seListHeader);

		boolean validationFailed = authFailed || xAdminProtocolFailed || agentIDFailed || seListFailed;

		if (!validationFailed) {
			List<SEPair> sePairs = OTIHttpUtils.parseSEList(seListHeader);

			if (authData != null && authData.startsWith(BASIC_AUTHENTICATION_PREFIX)) {
				String encodedLoginPasswd = authData.substring(BASIC_AUTHENTICATION_PREFIX.length());
				String decoded = new String(DatatypeConverter.parseBase64Binary(encodedLoginPasswd));
				String[] loginPasswdEncoded = decoded.split(":");
				if (loginPasswdEncoded.length == 2) {
					String login = loginPasswdEncoded[0];
					String passwd = loginPasswdEncoded[1];
					AuthenticationDTO authDTO = AuthenticationDTO.getDTOBuilderInstance(sePairs, agentID, login, passwd).build();

					try {
						otiControllerBean.authenticate(authDTO);
						chain.doFilter(request, response);
					} catch (OTIAuthenticationException authEx) {
						resp.addHeader(X_ADMIN_PROTOCOL_HEADER.getGpKey(), X_ADMIN_PROTOCOL.getGpKey());
						AuthenticationFailReason failReason = authEx.getFailReason();
						String msgReason = failReason.getReason();
						log.debug(String.format("Authentication failed: %s", msgReason));
						switch (failReason) {
							case LOGIN_PASSWD_MISMATCH:
								resp.sendError(SC_FORBIDDEN, msgReason);
								break;
							case SESSION_NOT_FOUND:
							case SESSION_TO_BE_PURGED:
								resp.sendError(SC_NO_CONTENT, msgReason);
								break;

							default:
								throw new ServletException(authEx);
						}
					}
				} else {
					resp.sendError(SC_BAD_REQUEST);
					resp.addHeader(X_ADMIN_PROTOCOL_HEADER.getGpKey(), X_ADMIN_PROTOCOL.getGpKey());
				}
			}
		} else {

			int httpErrorCode = SC_BAD_REQUEST;
			StringBuilder errBuilder = new StringBuilder("RAM request validation failed: ");

			if (authFailed) {
				errBuilder.append(" authorization header is missing");
			} else if (xAdminProtocolFailed) {
				errBuilder.append(X_ADMIN_PROTOCOL_HEADER.getGpKey()).append(" header is missing; ");
			} else if (agentIDFailed) {
				errBuilder.append(X_ADMIN_FROM_HEADER.getGpKey()).append(" header is missing; ");
			} else {
				errBuilder.append(X_ADMIN_SE_LIST_HEADER.getGpKey()).append(" is invalid: ").append(seListHeader);
				httpErrorCode = SC_PRECONDITION_FAILED;
			}

			resp.addHeader(X_ADMIN_PROTOCOL_HEADER.getGpKey(), X_ADMIN_PROTOCOL.getGpKey());

			String errMsg = errBuilder.toString();
			log.debug(errMsg);
			resp.sendError(httpErrorCode, errMsg);
		}

	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
	}
}
