package com.oberthur.connector.oti.http.controller;

import static com.oberthur.connector.oti.http.GPKeys.X_ADMIN_SE_LIST_HEADER;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.dto.SEPair;
import com.oberthur.connector.oti.exceptions.OTIException;
import com.oberthur.connector.oti.exceptions.SendAsyncResponseException;
import com.oberthur.connector.oti.http.GPKeys;
import com.oberthur.connector.oti.http.OTIHttpUtils;
import com.oberthur.connector.oti.http.bean.OTIControllerBean;
import com.oberthur.connector.oti.http.session.OTIAgentSession;
import com.oberthur.connector.oti.interceptor.monitoring.SyncInterfacesMonitoring;
import com.oberthur.connector.oti.interceptor.monitoring.SyncInterfacesMonitoring.SyncInterfaceName;
import com.oberthur.connector.oti.ram.state.AgentSessionState;

@WebServlet(name = "ram", urlPatterns = "/ram")
@SessionScoped
@SyncInterfacesMonitoring(interfaceName = SyncInterfaceName.DEVICE_REQUEST)
public class OTIRAMController extends HttpServlet {
    /**
	 * 
	 */
    private static final long serialVersionUID = -9145957619485949731L;

    /**
	 * 
	 */
    private static final Logger log = LoggerFactory.getLogger(OTIRAMController.class);

    @Inject
    private OTIAgentSession agentSession;

    @Inject
    private OTIControllerBean otiControllerBean;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        log.debug("Call prepareCommunication");

        String agentId = request.getHeader(GPKeys.X_ADMIN_FROM_HEADER.getGpKey());
        String seList = request.getHeader(X_ADMIN_SE_LIST_HEADER.getGpKey());

        List<SEPair> sePairs = OTIHttpUtils.parseSEList(seList);

        try {
            log.info("Received prepare communication request from agent: secure elements: {}; agentId: {}", seList, agentId);

            BigInteger sessionID = otiControllerBean.prepareCommunicationRequest(sePairs, agentId);

            log.debug("Starting polling RAM agent session ID {}", sessionID);
            AgentSessionState releasedSession = agentSession.pollRAMState(sessionID);

            try {
                if (releasedSession.isReleased()) {
                    OTIHttpUtils.sendScript(response, releasedSession, sessionID);
                    releasedSession.markSent();
                } else {
                    OTIHttpUtils.dropCommunication(response);

                    // if session state discarded, end of lifecycle here
                    agentSession.removeRAMSessionState(sessionID);
                }

            } catch (IOException ioe) {
                log.debug("RAM: Servlet response output stream has expired", ioe);
                releasedSession.markTimedOut();
            }

        } catch (SendAsyncResponseException sare) {
            log.debug("Prepare communication async send failed. {}", sare.getMessage());
        } catch (OTIException e) {
            log.debug("RAM failed: {}", e.getMessage());
            OTIHttpUtils.dropCommunication(response);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ServletException(e);
        }
    }
}