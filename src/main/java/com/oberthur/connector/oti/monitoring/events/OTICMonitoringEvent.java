/**
 * 
 */
package com.oberthur.connector.oti.monitoring.events;

import com.oberthur.monitoring.events.MonitoringEvent;

/**
 * @author Illya Krakovskyy
 *
 */
public class OTICMonitoringEvent extends MonitoringEvent {

	/**
	 * The monitoring event
	 */
	private OTICMonitoringEvents enclosingEvent;

	/**
	 * The event message constructed with enclosing event formatted message
	 * {@link OTICMonitoringEvents#formattedMessage} parameterized while calling
	 * the class constructor
	 */
	private String monitoringEventsMessage;

	/**
	 * @param enclosingEvent
	 * @param messageFormatParams
	 */
	public OTICMonitoringEvent(OTICMonitoringEvents enclosingEvent, Object... messageFormatParams) {
		this.enclosingEvent = enclosingEvent;

		boolean haveParams = messageFormatParams != null && messageFormatParams.length > 0;
		String formattedMessage = enclosingEvent.getFormattedMessage();

		this.monitoringEventsMessage = haveParams ? String.format(formattedMessage, messageFormatParams) : formattedMessage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.oberthur.monitoring.events.MonitoringEvent#getEventName()
	 */
	@Override
	public String getEventName() {
        return enclosingEvent.getEventName();
    }

	/**
	 * @return the enclosingEvent
	 */
	public OTICMonitoringEvents getEnclosingEvent() {
		return enclosingEvent;
	}

    /**
     * @return the monitoringEventsMessage
     */
    public String getMonitoringEventsMessage() {
        return monitoringEventsMessage;
    }

}
