/**
 * 
 */
package com.oberthur.connector.oti.monitoring;

import java.math.BigInteger;

import com.oberthur.connector.oti.config.OTIConfigKeys;

/**
 * @author Illya Krakovskyy
 * 
 *         Monitoring config keys
 *
 */
public enum OTICMonitoringConfigKeys implements OTIConfigKeys {
    MONITORING_SNMP_HOST("monitoring.snmp.host", "localhost"),
    MONITORING_SNMP_PORT("monitoring.snmp.port", "161"),
    MONITORING_SNMP_RETRIES("monitoring.snmp.retries", "3"),
    MONITORING_SNMP_TIMEOUT("monitoring.snmp.timeout", "5000"),
    MONITORING_NOTIFICATION_FREQUENCY("monitoring.notification.frequency", "1");

    /**
     * @param keyName
     *            configuration key name
     * @param defaultValue
     *            Configuration item of key <code>keyName</code> default value
     */
    private OTICMonitoringConfigKeys(String keyName, String defaultValue) {
        this.configKey = keyName;
        this.defaultValue = defaultValue;
    }

    /**
     * Configuration key name
     */
    private String configKey;

    /**
     * Configuration item default value
     */
    private String defaultValue;

    /**
     * @return the defaultValue
     */
    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public String getConfigKey() {
        return configKey;
    }

    @Override
    public BigInteger getNumericDefaultValue() {
        try {
            return BigInteger.valueOf(Long.parseLong(defaultValue));
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
