/**
 * 
 */
package com.oberthur.connector.oti.monitoring;

import static com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents.CONFIG_LOAD_FAILED;
import static com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents.CONFIG_LOAD_SUCCESS;
import static com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents.DATA_ACCESS_FAILED;
import static com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents.DATA_ACCESS_SUCCESS;
import static com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents.REST_CALLBACK_FAILED;
import static com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents.REST_CALLBACK_SUCCESS;

import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents;
import com.oberthur.monitoring.MonitoringEventType;

/**
 * @author Illya Krakovskyy
 * 
 *         Defines clear-alerts pairs, i.e. what CLEAR event clean up ALERT
 *         event
 *
 */
public enum ClearAlerts {
    CONFIGURATION_FAIL_CLEAR(CONFIG_LOAD_FAILED, CONFIG_LOAD_SUCCESS),
    DATA_ACCESS_FAIL_CLEAR(DATA_ACCESS_FAILED, DATA_ACCESS_SUCCESS),
    REST_CALLBACK_FAIL_CLEAR(REST_CALLBACK_FAILED, REST_CALLBACK_SUCCESS);

    /**
     * ALERT event in clear-alert pair
     */
    private OTICMonitoringEvents alertEvent;
    /**
     * CLEAR event in clear-alert pair
     */
    private OTICMonitoringEvents clearEvent;

    /**
     * @param alertEvent
     * @param clearEvent
     */
    private ClearAlerts(OTICMonitoringEvents alertEvent, OTICMonitoringEvents clearEvent) {
        this.alertEvent = alertEvent;
        this.clearEvent = clearEvent;
    }

    /**
     * @param clearEvent
     * @return
     */
    public static OTICMonitoringEvents getAlertByClear(OTICMonitoringEvents clearEvent) {
        return getEventByEvent(clearEvent);
    }

    /**
     * @param alertEvent
     * @return
     */
    public static OTICMonitoringEvents getClearByAlert(OTICMonitoringEvents alertEvent) {
        return getEventByEvent(alertEvent);
    }

    /**
     * @param alertEvent
     * @return
     */
    public static boolean containsAlert(OTICMonitoringEvents alertEvent) {
        if (alertEvent.getEventType() == MonitoringEventType.ALERT) {
            for (ClearAlerts enumVal : ClearAlerts.values()) {
                if (enumVal.getAlertEvent() == alertEvent) {
                    return true;
                }
            }
            return false;
        }

        throw new IllegalArgumentException(String.format("Event %s is not of ALERT type", alertEvent.toString()));
    }

    /**
     * @return the alertEvent
     */
    public OTICMonitoringEvents getAlertEvent() {
        return alertEvent;
    }

    /**
     * @return the clearEvent
     */
    public OTICMonitoringEvents getClearEvent() {
        return clearEvent;
    }

    private static OTICMonitoringEvents getEventByEvent(OTICMonitoringEvents event) {
        MonitoringEventType eventType = event.getEventType();
        boolean isCorrectType = MonitoringEventType.NOTIFICATION != eventType;
        if (isCorrectType) {
            boolean isClear = MonitoringEventType.CLEAR == eventType;
            for (ClearAlerts enumVal : ClearAlerts.values()) {
                OTICMonitoringEvents theClearEvent = enumVal.getClearEvent();
                OTICMonitoringEvents theAlertEvent = enumVal.getAlertEvent();

                OTICMonitoringEvents theEvent = isClear ? theClearEvent : theAlertEvent;

                if (theEvent == event) {
                    return isClear ? theAlertEvent : theClearEvent;
                }
            }
        }

        throw new IllegalArgumentException(String.format(isCorrectType ? "Event %1$s with type %2$s not found in clear/alert pairs" : "Event %1$s should not be of %2$s type",
                event.toString(), eventType.toString()));
    }
}
