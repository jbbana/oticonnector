/**
 * 
 */
package com.oberthur.connector.oti.monitoring;

import static com.oberthur.connector.oti.monitoring.OTICMonitoringConfigKeys.MONITORING_SNMP_HOST;
import static com.oberthur.connector.oti.monitoring.OTICMonitoringConfigKeys.MONITORING_SNMP_PORT;
import static com.oberthur.connector.oti.monitoring.OTICMonitoringConfigKeys.MONITORING_SNMP_RETRIES;
import static com.oberthur.connector.oti.monitoring.OTICMonitoringConfigKeys.MONITORING_SNMP_TIMEOUT;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.prefs.Preferences;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvent;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents;
import com.oberthur.connector.oti.service.startup.Startup;
import com.oberthur.monitoring.MonitoringEventType;
import com.oberthur.monitoring.events.MonitoringEvent;
import com.oberthur.monitoring.events.MonitoringEventHandler;
import com.oberthur.monitoring.events.MonitoringEventHandlingException;
import com.oberthur.monitoring.events.MonitoringEventHandlingException.ExceptionType;
import com.oberthur.monitoring.snmp.SNMPConfig;
import com.oberthur.monitoring.snmp.SNMPEvent;
import com.oberthur.monitoring.snmp.SNMPException;
import com.oberthur.monitoring.snmp.SNMPTrapSender;

/**
 * @author Illya Krakovskyy
 *
 */
@Startup
@ApplicationScoped
public class OTICMonitoringEventHandler extends MonitoringEventHandler {

	/**
	 * Logging facility
	 */
	private static final Logger log = LoggerFactory.getLogger(OTICMonitoringEventHandler.class);

	/**
	 * 
	 */
	private long startupTimestamp;

	/**
	 * SNMP trap sender
	 */
	@Inject
	private SNMPTrapSender trapSender;

	@Inject
	private OTICMonitoringEventsData eventsDataHolder;

	@PostConstruct
	public void startUp() {
		log.debug("Populating start timestamp");
		startupTimestamp = System.currentTimeMillis();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.oberthur.monitoring.events.MonitoringEventHandler#handleEvent(com
	 * .oberthur.monitoring.events.MonitoringEvent)
	 */
	@Override
	public <T extends MonitoringEvent> void handleEvent(@Observes @Monitoring T monitoringEvent) throws MonitoringEventHandlingException {
		if (OTICMonitoringEvent.class.isInstance(monitoringEvent)) {
			OTICMonitoringEvent oticEvent = (OTICMonitoringEvent) monitoringEvent;
			OTICMonitoringEvents enclosingEvent = oticEvent.getEnclosingEvent();
			String oid = enclosingEvent.getOid();
			MonitoringEventType eventType = enclosingEvent.getEventType();

            boolean isFreshAlert = MonitoringEventType.ALERT == eventType && !eventsDataHolder.containsAlert(oid);
            boolean isFreshClear = MonitoringEventType.CLEAR == eventType && !eventsDataHolder.containsClear(oid);

            boolean okToHandle = isFreshAlert || isFreshClear || MonitoringEventType.NOTIFICATION == eventType;

            if (okToHandle) {
                log.debug("Handling monitoring event {} with type {}, severity {} and message '{}'", enclosingEvent.toString(), eventType.toString(), enclosingEvent.getSeverity()
                        .toString(), oticEvent.getMonitoringEventsMessage());
                try {
                    SNMPEvent snmpEvent = new SNMPEvent(enclosingEvent.getOid(), enclosingEvent.getEventName(), eventType, enclosingEvent.getSeverity(), "OTI Connector",
                            oticEvent.getMonitoringEventsMessage());

                    SNMPConfig snmpConfig = populateSNMPConfig();

                    trapSender.send(snmpConfig, snmpEvent);
                } catch (NumberFormatException | UnknownHostException e) {
                    throw new MonitoringEventHandlingException(ExceptionType.CONFIG_LOADING_ERROR, String.format("Monitoring configuration loading failed: %s:%s", e.getClass()
                            .getSimpleName(), e.getMessage()));
                } catch (SNMPException snmpe) {
                    log.error("Failed to send event as SNMP trap: {} ", snmpe.getMessage());
                } finally {
                    if (isFreshAlert) {
                        eventsDataHolder.addToAlerts(enclosingEvent);
                    }
                    if (isFreshClear) {
                        eventsDataHolder.clearAlerts(enclosingEvent);
                    }
                }
            } else {
                log.debug("Skipping monitoring event {} with type {} and severity {} as it was sent already", enclosingEvent.toString(), eventType.toString(), enclosingEvent
                        .getSeverity().toString());
            }
		} else {
			throw new IllegalArgumentException(String.format("Monitoring event %s is not of OTICMonitoringEvent type", monitoringEvent.getClass().getName()));
		}
	}

	/**
	 * Populates SNMP configuration with OTIC monitoring configuration
	 * 
	 * @return SNMP config
	 * @throws UnknownHostException
	 *             host specified failed to resolve
	 * @throws NumberFormatException
	 *             wrong values for numeric values used
	 */
	private SNMPConfig populateSNMPConfig() throws UnknownHostException, NumberFormatException {
		Preferences monitoringPrefs = Preferences.userNodeForPackage(OTICMonitoringEventHandler.class);
        String snmpHostVal = monitoringPrefs.get(MONITORING_SNMP_HOST.getConfigKey(), OTICMonitoringConfigKeys.MONITORING_SNMP_HOST.getDefaultValue());

		InetAddress snmpHost = InetAddress.getByName(snmpHostVal);

        int snmpPort = monitoringPrefs.getInt(MONITORING_SNMP_PORT.getConfigKey(), Integer.parseInt(MONITORING_SNMP_PORT.getDefaultValue()));
        int retries = monitoringPrefs.getInt(MONITORING_SNMP_RETRIES.getConfigKey(), Integer.parseInt(MONITORING_SNMP_RETRIES.getDefaultValue()));
        int timeout = monitoringPrefs.getInt(MONITORING_SNMP_RETRIES.getConfigKey(), Integer.parseInt(MONITORING_SNMP_TIMEOUT.getDefaultValue()));

		SNMPConfig snmpConfig = new SNMPConfig(snmpHost, snmpPort, retries, timeout);
		return snmpConfig;
	}
}