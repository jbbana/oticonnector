/**
 * 
 */
package com.oberthur.connector.oti.monitoring;

import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;

import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents;
import com.oberthur.monitoring.MonitoringEventType;

/**
 * @author Illya Krakovskyy
 *
 */
@ApplicationScoped
public class OTICMonitoringEventsData {
	
    /**
     * 
     */
    private Set<OTICMonitoringEvents> alerts = new HashSet<OTICMonitoringEvents>();

    /**
     * 
     */
    private Set<OTICMonitoringEvents> clears = new HashSet<OTICMonitoringEvents>();
    
    /**
     * Adds event to alert events set
     * @param alertEvent
     */
    public void addToAlerts(OTICMonitoringEvents alertEvent){
    	MonitoringEventType eventType = alertEvent.getEventType();
		if (eventType == MonitoringEventType.ALERT) {
	        alerts.add(alertEvent);
        } else {
        	throw new IllegalArgumentException(String.format("Monitoring Event %s is not of ALERT type but %s", alertEvent.toString(), eventType));
        }
    }

	/**
	 * @param clearEvent
	 */
	public void clearAlerts(OTICMonitoringEvents clearEvent) {
		MonitoringEventType eventType = clearEvent.getEventType();
		if (eventType == MonitoringEventType.CLEAR) {
	        OTICMonitoringEvents alertEvent = ClearAlerts.getAlertByClear(clearEvent);
	        alerts.remove(alertEvent);
	        clears.add(clearEvent);
        } else {
        	throw new IllegalArgumentException(String.format("Monitoring event %s is not of CLEAR type but %s", clearEvent.toString(), eventType));
        }
	}
	
	/**
	 * @param eventOid
	 * @return
	 */
	public boolean containsAlert(String eventOid){
		return containsEvent(eventOid, true);
	}
	
	/**
	 * @param eventOid
	 * @return
	 */
	public boolean containsClear(String eventOid){
		return containsEvent(eventOid, false);
	}
	
	/**
	 * @return the alerts
	 */
	public Set<OTICMonitoringEvents> getAlerts() {
		return alerts;
	}

	/**
	 * @param eventOID
	 * @param isAlert
	 * @return
	 */
	private boolean containsEvent(String eventOID, boolean isAlert) {
		OTICMonitoringEvents event = OTICMonitoringEvents.getEventByOid(eventOID);
		return isAlert ? alerts.contains(event) : clears.contains(event);
	}
}
