/**
 * 
 */
package com.oberthur.connector.oti.monitoring;

/**
 * @author Illya Krakovskyy
 *
 */
public enum InterfacesNames {
    PREPARE_COMMUNICATION_ASYNC_RESPONSE,
    SEND_SCRIPT_ASYNC_RESPONSE,
    CALLER_PREPARE_COMMUNICATION,
    CALLER_SEND_SCRIPT,
    CALLER_DROP_COMMUNICATION,
    DEVICE_REQUEST,
    DEVICE_RESPONSE;
}
