/**
 * 
 */
package com.oberthur.connector.oti.monitoring.events;

import static com.oberthur.monitoring.MonitoringEventSeverity.CRITICAL;
import static com.oberthur.monitoring.MonitoringEventSeverity.INFO;
import static com.oberthur.monitoring.MonitoringEventType.ALERT;
import static com.oberthur.monitoring.MonitoringEventType.CLEAR;
import static com.oberthur.monitoring.MonitoringEventType.NOTIFICATION;

import com.oberthur.monitoring.MonitoringEventSeverity;
import com.oberthur.monitoring.MonitoringEventType;
import com.oberthur.monitoring.MonitoringEvents;

/**
 * @author Illya Krakovskyy
 * 
 *         OTI Connector monitoring events
 *
 */
public enum OTICMonitoringEvents implements MonitoringEvents {
    /**
     * 
     */
    OTIC_START("101.1", "OTI Connector start", NOTIFICATION, INFO, "OTI Connector started"),
    /**
     * 
     */
    OTIC_STOP("101.2", "Module stop", NOTIFICATION, INFO, "OTI Connector stopped"),
    /**
     * 
     */
    CONFIG_LOAD_FAILED("101.4", "Configuration loading failure", ALERT, CRITICAL, "Configuration loading failure.%s parameters not available"),
    /**
     * 
     */
    CONFIG_LOAD_SUCCESS("101.5", "Configuration loading success", CLEAR, INFO, "Configuration loading success"),
    /**
     * 
     */
    DATA_ACCESS_FAILED("101.6", "Data Access Failure", ALERT, CRITICAL, "Data access failure. Cause: %s"),
    /**
     * 
     */
    DATA_ACCESS_SUCCESS("101.7", "Data Access Success", CLEAR, INFO, "Data access success"),
    /**
     * 
     */
    DEFAULT_CONFIG_LOADED("101.8", "Default configuration loading", NOTIFICATION, INFO, "Default values of configuration parameters were loaded"),
    /**
     * 
     */
    REST_CALLBACK_FAILED("101.11", "REST callback not responding", ALERT, CRITICAL, "REST callback not responding"),
    /**
     * 
     */
    REST_CALLBACK_SUCCESS("101.12", "REST callback responding", CLEAR, INFO, "REST callback responding"),
    /**
     * 
     */
    SESSIONS_PURGED("101.51", "N sessions purged", NOTIFICATION, INFO, "%d purged sessions have been purged for last %d hours for following SEs: %s"),
    /**
     * 
     */
    SESSIONS_CLEANED("101.52", "N requests cleaned", NOTIFICATION, INFO, "%d cleaned sessions have been purged for last %d hours for following SEs: %s"),
    /**
     * 
     */
    INCOMING_REQUESTS("101.103", "Number of incoming requests per second per interface", NOTIFICATION, INFO, "%d incoming requests to %s per second"),
    /**
     * 
     */
    INCOMING_ERRORS(
            "101.105",
            "Number of incoming messages answered with internal errors per interface per second",
            NOTIFICATION,
            INFO,
            "%d incoming messages answered with internal errors by %s per second"),
    /**
     * 
     */
    INCOMING_AVG_RESPONSE_TIME(
            "101.106",
            "Average response time on synchronous incoming requests per interface",
            NOTIFICATION,
            INFO,
            "%d milliseconds - average response time on sycnhronous imcvoming requests on %s interface");

    /**
     * The event object identifier
     */
    private String oid;
    /**
     * The event name
     */
    private String eventName;
    /**
     * The event type
     */
    private MonitoringEventType eventType;
    /**
     * The event severity
     */
    private MonitoringEventSeverity severity;
    /**
     * The event message, parameterized with standard Java formatting placeholders
     */
    private String formattedMessage;

    /**
     * @param oid
     * @param eventName
     * @param eventType
     * @param severity
     * @param formattedMessage
     */
    private OTICMonitoringEvents(String oid, String eventName, MonitoringEventType eventType, MonitoringEventSeverity severity, String formattedMessage) {
        this.oid = oid;
        this.eventName = eventName;
        this.eventType = eventType;
        this.severity = severity;
        this.formattedMessage = formattedMessage;
    }

    /**
     * Checks whether the monitoring event is "statistics" event, one of {OTICMonitoringEvents.INCOMING_AVG_RESPONSE_TIME, OTICMonitoringEvents.INCOMING_REQUESTS,
     * OTICMonitoringEvents.INCOMING_ERRORS}
     * 
     * @param monitoringEvent
     * @return
     */
    public static boolean isStatisticsEvent(OTICMonitoringEvents monitoringEvent) {
        return OTICMonitoringEvents.INCOMING_AVG_RESPONSE_TIME == monitoringEvent || OTICMonitoringEvents.INCOMING_REQUESTS == monitoringEvent
                || OTICMonitoringEvents.INCOMING_ERRORS == monitoringEvent;
    }
    
    /**
     * Find monitoring event by its oid
     * @param eventOid event oid
     * @return monitoring event with oid specified
     */
    public static OTICMonitoringEvents getEventByOid(String eventOid){
    	for (OTICMonitoringEvents monitoringEvent : OTICMonitoringEvents.values()) {
	        if (monitoringEvent.getOid().equals(eventOid)) {
	            return monitoringEvent;
            }
        }
    	
    	throw new IllegalArgumentException(String.format("Unknown oid %s, no monitoring evewnt found", eventOid));
    }

    @Override
    public String getEventName() {
        return eventName;
    }

    @Override
    public MonitoringEventType getEventType() {
        return eventType;
    }

    @Override
    public String getFormattedMessage() {
        return formattedMessage;
    }

    @Override
    public String getOid() {
        return oid;
    }

    @Override
    public MonitoringEventSeverity getSeverity() {
        return severity;
    }
}
