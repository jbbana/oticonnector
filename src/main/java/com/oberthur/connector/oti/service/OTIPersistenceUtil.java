/**
 * 
 */
package com.oberthur.connector.oti.service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author Illya Krakovskyy
 *
 */
@ApplicationScoped
public class OTIPersistenceUtil {

	private EntityManagerFactory entityManagerFactory;

	/**
	 * 
	 */
	@PostConstruct
	public void initPersistence() {
		if (entityManagerFactory == null || !entityManagerFactory.isOpen()) {
			entityManagerFactory = Persistence.createEntityManagerFactory("oti-connector");
		}
	}

	@PreDestroy
	public void closePersistence() {
		if (entityManagerFactory != null && entityManagerFactory.isOpen()) {
			entityManagerFactory.close();
		}
	}

	public EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
}
