package com.oberthur.connector.oti.service.notification;

/**
 * Created by mikam on 25.06.2015.
 */

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Custom payload exchanged in between parties.
 *
 * @author Michal Kaminski, OT R&D Poland
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class NotificationPayload {

    @JsonProperty("type")
    private String type;

    @JsonProperty("value")
    private Object value;

    public NotificationPayload(String type, Object value) {
        this.type = type;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotificationPayload payload = (NotificationPayload) o;

        return new EqualsBuilder()
                .append(type, payload.type)
                .append(value, payload.value)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(type)
                .append(value)
                .toHashCode();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
