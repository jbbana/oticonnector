/**
 * 
 */
package com.oberthur.connector.oti.service.startup.purge;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.domain.SessionOTI_;
import com.oberthur.connector.oti.dto.SessionStatus;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;

/**
 * @author Illya Krakovskyy
 *
 */
public class ScheduledCallable {

    protected BigInteger sessionID;

    protected EntityManager entityManager;

    /**
     * @param sessionID
     * @param persistenceUtil
     */
    protected ScheduledCallable(BigInteger sessionID, OTIPersistenceUtil persistenceUtil) {
        this.sessionID = sessionID;
        this.entityManager = persistenceUtil.getEntityManager();
    }

    /**
     * @param isPurge
     *            TODO
     * @return
     */
    protected SessionOTI findSession(boolean isPurge) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<SessionOTI> criteriaQuery = criteriaBuilder.createQuery(SessionOTI.class);
        Root<SessionOTI> sessionRoot = criteriaQuery.from(SessionOTI.class);

        // Check whether "condemned to purge" session is in INITIATED status
        Predicate purgePredicate = criteriaBuilder.and(criteriaBuilder.equal(sessionRoot.get(SessionOTI_.id), sessionID),
                criteriaBuilder.equal(sessionRoot.get(SessionOTI_.status), SessionStatus.INITIATED),
                criteriaBuilder.lessThan(sessionRoot.get(SessionOTI_.purgeDate), new Date()));

        Predicate cleanPredicate = criteriaBuilder.and(criteriaBuilder.equal(sessionRoot.get(SessionOTI_.id), sessionID),
                criteriaBuilder.isNotNull(sessionRoot.get(SessionOTI_.transactionId)),
                sessionRoot.get(SessionOTI_.status).in(SessionStatus.OPENED, SessionStatus.TO_BE_PURGED),
                criteriaBuilder.lessThanOrEqualTo(sessionRoot.get(SessionOTI_.validDate), new Date(System.currentTimeMillis())));

        criteriaQuery.select(sessionRoot).where(isPurge ? purgePredicate : cleanPredicate);
        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

}
