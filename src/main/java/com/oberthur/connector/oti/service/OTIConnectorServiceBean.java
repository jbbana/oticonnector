package com.oberthur.connector.oti.service;

import static com.oberthur.apdugenerator.utils.Utils.hexStringToByteArray;
import static com.oberthur.connector.api.xml.bind.notification.Notification.newNotificationFail;
import static com.oberthur.connector.api.xml.bind.notification.Notification.newNotificationOk;

import java.math.BigInteger;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.apdugenerator.utils.Utils;
import com.oberthur.connector.api.ConnectorService;
import com.oberthur.connector.api.SecureElementType;
import com.oberthur.connector.api.xml.bind.notification.Notification;
import com.oberthur.connector.api.xml.bind.notification.ResponseCode;
import com.oberthur.connector.api.xml.bind.request.PrepareCommunicationRequest;
import com.oberthur.connector.api.xml.bind.request.SendScriptRequest;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.dto.ConnectorDTO;
import com.oberthur.connector.oti.dto.SendScriptDTO;
import com.oberthur.connector.oti.exceptions.MissingSessionException;
import com.oberthur.connector.oti.exceptions.MissingSessionException.MissingExceptionReason;
import com.oberthur.connector.oti.interceptor.MethodLogged;
import com.oberthur.connector.oti.interceptor.ValidateRequest;
import com.oberthur.connector.oti.interceptor.monitoring.SyncInterfacesMonitoring;
import com.oberthur.connector.oti.interceptor.monitoring.SyncInterfacesMonitoring.SyncInterfaceName;
import com.oberthur.connector.oti.service.bean.OTIServiceBean;

@MethodLogged
@RequestScoped
@Default
public class OTIConnectorServiceBean implements ConnectorService {

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(OTIConnectorServiceBean.class);

    @Inject
    private OTIServiceBean otiService;

    @SyncInterfacesMonitoring(interfaceName = SyncInterfaceName.CALLER_DROP_COMMUNICATION)
    @Override
    public Notification disconnect(BigInteger sessionId, String partnerOID) {
        try {
            otiService.dropCommunication(sessionId, partnerOID);
        } catch (MissingSessionException e) {
            return newNotificationFail(String.format("The session with Session Id = %s is not opened (or doesn't exist)", sessionId),
                    ResponseCode.INVALID_ARGUMENT);
        } catch (Exception e) {
            return newNotificationFail(e.getMessage());
        }
        return newNotificationOk();
    }

    @Override
    @ValidateRequest
    @SyncInterfacesMonitoring(interfaceName = SyncInterfaceName.CALLER_PREPARE_COMMUNICATION)
    public Notification prepareCommunicationRequest(PrepareCommunicationRequest request) {
        try {

            String targetIdentifier = request.getTargetIdentifier();
            int aidLength = Utils.hexStringToByteArray(targetIdentifier).length;
            if (!(aidLength > 5 && aidLength < 16)) {
                return newNotificationFail("Target identifier length should be more than 5 bytes and less than 16 bytes",
                        ResponseCode.INVALID_ARGUMENT);
            }

            Integer purgeTime = request.getMaxResponseDelay();
            if (purgeTime == null) {
                int defaultPurgeTimeMins = Preferences.userNodeForPackage(getClass()).getInt(
                        OTIConnectorConfigKeys.PURGE_TIME.getConfigKey(),
                        OTIConnectorConfigKeys.PURGE_TIME.getNumericDefaultValue().intValue());
                long purgeTimeSeconds = TimeUnit.MINUTES.toSeconds(defaultPurgeTimeMins);
                purgeTime = Integer.valueOf(BigInteger.valueOf(purgeTimeSeconds).intValue());

            } else if (purgeTime < 0) {
                return newNotificationFail(String.format("Purge Time %s can't be negative", purgeTime.toString()),
                        ResponseCode.INVALID_ARGUMENT);
            }

            log.debug("Prepare communication request received from sequencer: " + "\nSession ID: {}" + "\nSecure element: {}"
                    + "\nPurge time: {}", request.getSessionID(), request.getSecureElement(), String.valueOf(purgeTime));

            SecureElementType secureElementType = request.getSecureElementType() != null ? request.getSecureElementType()
                    : SecureElementType.ICCID;

            ConnectorDTO dto = ConnectorDTO.getBuilderSessionInstance(request.getSessionID(), request.getSecureElement(),
                    secureElementType, purgeTime, request.getAgentId(), request.getLogin(), request.getPassword()).build();

            if (otiService.isSessionExist(request.getSecureElement(), secureElementType)) {
                String dupMsg = String.format("Session with secure element %s already exists", request.getSecureElement());
                log.debug(dupMsg);
                return newNotificationFail(dupMsg, ResponseCode.SESSION_EXISTS);
            }

            SessionOTI newSession = otiService.prepareCommunicationInitiate(dto);
            log.debug("SessionID={}; Session persisted successfully", newSession.getSessionId().toString());

            return newNotificationOk();
        } catch (Exception e) {
            return newNotificationFail(e.getMessage());
        }
    }

    @ValidateRequest
    @SyncInterfacesMonitoring(interfaceName = SyncInterfaceName.CALLER_SEND_SCRIPT)
    @Override
    public Notification sendScriptRequest(SendScriptRequest request) {
        log.debug(
                "Send script request received from sequencer: " + "\nConversation ID: {}" + "\nValidity period: {}"
                        + "\nTransaction ID: {}",
                String.valueOf(request.getConversationId()), String.valueOf(request.getValidityPeriod()),
                String.valueOf(request.getTransactionId()));

        SendScriptDTO sendScriptDTO = SendScriptDTO.getBuilderInstance(request.getConversationId(), request.getValidityPeriod(),
                request.getTransactionId(), request.getTarget(), request.getScript()).build();

        String target = sendScriptDTO.getTarget();
        byte[] aid = target != null ? hexStringToByteArray(target) : new byte[0];
        if (aid.length < 5 || aid.length > 16) {
            String errMsg = String.format("AID length is invalid: %d", aid.length);
            log.error(errMsg);
            return newNotificationFail(errMsg, ResponseCode.INVALID_ARGUMENT);
        }

        try {
            otiService.sendScriptResponse(sendScriptDTO);
            return newNotificationOk();
        } catch (MissingSessionException e) {
            MissingExceptionReason reason = e.getReason();

            String failReason = e.getMessage();
            ResponseCode responseCode = MissingExceptionReason.SESSION_TIMED_OUT == reason ? ResponseCode.TIMEOUT_DROP
                    : ResponseCode.NO_SEND_SCRIPT_SESSION;
            if (failReason == null || failReason.trim().length() == 0) {
                switch (reason) {
                    case HTTP_SESSION_MISSED:
                        failReason = String.format("The session with ConversationId %s does not exist within HTTP sessions kept alive",
                                sendScriptDTO.getConversationId());
                        break;
                    case SESSION_MISSED:
                        failReason = String.format("The session with ConversationId %s is not opened (or doesn't exist)",
                                sendScriptDTO.getConversationId());
                        break;
                    case SESSION_TIMED_OUT:
                        failReason = "Connection with Mobile Agent was dropped because of timeout";
                        break;
                    default:
                        break;
                }
            }
            return newNotificationFail(failReason, responseCode);

        } catch (Exception e) {
            return newNotificationFail(e.getMessage());
        }

    }
}
