/**
 * 
 */
package com.oberthur.connector.oti.service.startup.notify;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.monitoring.Monitoring;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvent;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents;

/**
 * @author Illya Krakovskyy
 *
 */
@RequestScoped
public class StartStopNotifier {

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(StartStopNotifier.class);

    @Inject
    @Monitoring
    private Event<OTICMonitoringEvent> event;

    /**
     * Send start event
     */
    public void notifyStart() {
        log.debug("OTI connector assumed started");
        event.fire(new OTICMonitoringEvent(OTICMonitoringEvents.OTIC_START));
    }

    /**
     * Send stop event
     */
    public void notifyStop() {
        log.debug("OTI COnnector assumed stopped");
        event.fire(new OTICMonitoringEvent(OTICMonitoringEvents.OTIC_STOP));
    }

}
