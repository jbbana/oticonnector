/**
 * 
 */
package com.oberthur.connector.oti.service.startup.purge;

import java.math.BigInteger;
import java.util.concurrent.Callable;

import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.common.rest.callback.ConnectorServiceCallbackBean;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;

/**
 * @author Illya Krakovskyy
 *
 */
public class CleanCallable extends ScheduledCallable implements Callable<BigInteger> {

    /**
     * 
     */
    private static final Logger log = LoggerFactory.getLogger(CleanCallable.class);

    /**
     * REST callback to caller
     */
    private ConnectorServiceCallbackBean callback;

    /**
     * @param sessionID
     * @param persistenceUtil
     */
    public CleanCallable(BigInteger sessionID, OTIPersistenceUtil persistenceUtil, ConnectorServiceCallbackBean callback) {
        super(sessionID, persistenceUtil);
        this.callback = callback;
    }

    @Override
    public BigInteger call() throws Exception {
        log.debug("Executing session clean");
        try {
            SessionOTI sessionToClean = findSession(false);
            BigInteger id = sessionToClean.getId();

            PurgeHelper.handleSessions(false, entityManager, callback, sessionToClean);

            return id;
        } catch (NoResultException nre) {
            log.info("Session with unique ID {} not found , could be updated earlier or removed by another task", this.sessionID);
            return null;
        } catch (Exception e) {
            log.error("Exception occured while attempting to clean session", e);
            throw e;
        }
    }

}
