/**
 * 
 */
package com.oberthur.connector.oti.service.startup.tasks;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.api.ReturnedStatus;
import com.oberthur.connector.api.xml.bind.response.SendScriptResponse;
import com.oberthur.connector.common.rest.callback.ConnectorServiceCallbackBean;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.domain.SessionOTI_;
import com.oberthur.connector.oti.dto.SessionStatus;
import com.oberthur.connector.oti.interceptor.monitoring.DataAccessMonitoring;
import com.oberthur.connector.oti.interceptor.monitoring.PurgeMonitoring;
import com.oberthur.connector.oti.interceptor.monitoring.PurgeMonitoring.PurgeType;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;

/**
 * @author Illya Krakovskyy
 * 
 */
public class CleanTask extends TimerTask {

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(CleanTask.class);

	/**
	 * Connector to Sequencer REST service
	 */
	@Inject
	private ConnectorServiceCallbackBean callback;

	@Inject
	private OTIPersistenceUtil persistenceUtil;

    /**
     * Number of sessions cleaned. Assumed read by intercepter
     */
    private List<String> cleanedSEIDs = new ArrayList<String>();

	/**
	 * @see java.util.TimerTask#run()
	 */
	@Override
    @DataAccessMonitoring
    @PurgeMonitoring(purgeType = PurgeType.CLEAN)
	public void run() {
		log.debug("Clean procedure execution");
		EntityManager em = persistenceUtil.getEntityManager();
		EntityTransaction transaction = em.getTransaction();
		try {
			List<SendScriptResponse> sessionResponses = new ArrayList<SendScriptResponse>();

			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<SessionOTI> criteriaQuery = criteriaBuilder.createQuery(SessionOTI.class);
			Root<SessionOTI> sessionRoot = criteriaQuery.from(SessionOTI.class);

			criteriaQuery.select(sessionRoot).where(
					criteriaBuilder.and(
							criteriaBuilder.isNotNull(sessionRoot.get(SessionOTI_.transactionId)),
							criteriaBuilder.lessThanOrEqualTo(sessionRoot.get(SessionOTI_.validDate),
									new Date(System.currentTimeMillis())),
									sessionRoot.get(SessionOTI_.status).in(SessionStatus.OPENED, SessionStatus.TO_BE_PURGED)));

			TypedQuery<SessionOTI> query = em.createQuery(criteriaQuery);
			List<SessionOTI> resultList = query.getResultList();
			transaction.begin();
			for (SessionOTI session : resultList) {
				SendScriptResponse sendScriptResponse = new SendScriptResponse(session.getTransactionId().longValue());
				sendScriptResponse.setDeliveryStatus(ReturnedStatus.TIME_OUT);
				sendScriptResponse.setConversationId(BigInteger.valueOf(session.getSessionId().longValue()));

				sessionResponses.add(sendScriptResponse);

				session.setTransactionId(null);
				session.setValidDate(null);

                cleanedSEIDs.add(session.getSecureElement());

				SessionStatus status = session.getStatus();
				if (SessionStatus.TO_BE_PURGED == status) {
					em.remove(session);
				}
			}

			em.flush();
			em.clear();

			for (SendScriptResponse sendScriptResponse : sessionResponses) {
				callback.sendScriptResponse(sendScriptResponse);
			}

            log.debug("{} sessions cleaned", cleanedSEIDs.size());

			transaction.commit();
		} catch (Exception e) {
			log.error("Cleanup procedure failed: " + e.getMessage());
			if (transaction.isActive()) {
				transaction.rollback();
			}
		} finally {
			em.close();
		}
	}

    /**
     * @return the cleanedSEIDs as array
     */
    public String[] getCleanedSEIDs() {
        return cleanedSEIDs.toArray(new String[cleanedSEIDs.size()]);
    }
}
