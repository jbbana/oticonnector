/**
 * 
 */
package com.oberthur.connector.oti.service.startup;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterDeploymentValidation;
import javax.enterprise.inject.spi.Annotated;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.ProcessBean;

/**
 * @author Illya Krakovskyy
 *
 */
public class StartupExtension implements Extension {
    /**
	 * 
	 */
    private List<Bean<?>> eagerBeansList = new ArrayList<Bean<?>>();

    /**
     * @param event
     */
    public <T> void collect(@Observes ProcessBean<T> event) {
        Annotated annotated = event.getAnnotated();
        if (annotated.isAnnotationPresent(Startup.class) && (annotated.isAnnotationPresent(ApplicationScoped.class))) {
            Bean<T> bean = event.getBean();
            eagerBeansList.add(bean);
        }
    }

    /**
     * @param event
     * @param beanManager
     */
    public void load(@Observes AfterDeploymentValidation event, BeanManager beanManager) {

        for (Bean<?> bean : eagerBeansList) {
            // toString() is required to instantiate the bean through CDI
            beanManager.getReference(bean, bean.getBeanClass(), beanManager.createCreationalContext(bean)).toString();
        }
    }

}
