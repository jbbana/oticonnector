/**
 * 
 */
package com.oberthur.connector.oti.service.startup.purge;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.api.ConnectorServiceCallback;
import com.oberthur.connector.api.ReturnedStatus;
import com.oberthur.connector.api.xml.bind.response.PrepareCommunicationResponse;
import com.oberthur.connector.api.xml.bind.response.SendScriptResponse;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.dto.SessionStatus;

/**
 * @author Illya Krakovskyy
 *
 */
public class PurgeHelper {

    /**
     * 
     */
    private static final Logger log = LoggerFactory.getLogger(PurgeHelper.class);

    /**
     * @param isPurge
     * @param entityManager
     * @param callback
     * @param sessionsToHandle
     */
    public static void handleSessions(boolean isPurge, EntityManager entityManager, ConnectorServiceCallback callback, SessionOTI... sessionsToHandle) {

        EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();

        if (isPurge) {
            List<PrepareCommunicationResponse> sessionResponses = new ArrayList<>();
            for (SessionOTI sessionOTI : sessionsToHandle) {
                Timestamp purgeDate = sessionOTI.getPurgeDate();
                PrepareCommunicationResponse response = new PrepareCommunicationResponse(sessionOTI.getSessionId().toString());
                response.setTimeToLive(Long.valueOf(sessionOTI.getPurgeDate().getTime() - System.currentTimeMillis()).intValue());
                response.setSessionStatus(ReturnedStatus.TIME_OUT);
                sessionResponses.add(response);
                log.info("Purged SessionOTI[{}, {}, {}, {}, {}, {}, {}, {}]", sessionOTI.getId(), sessionOTI.getSecureElement(), sessionOTI.getSessionId(), sessionOTI.getStatus(),
                        sessionOTI.getTransactionId(), sessionOTI.getCreatedDate(), sessionOTI.getUpdatedDate(), purgeDate);
                entityManager.remove(sessionOTI);

            }

            for (PrepareCommunicationResponse response : sessionResponses) {
                callback.prepareCommunicationResponse(response);
            }
            sessionResponses.clear();
        } else {
            List<SendScriptResponse> sessionResponses = new ArrayList<SendScriptResponse>();
            for (SessionOTI sessionOTI : sessionsToHandle) {

                log.info("Clean up SessionOTI[{}, {}, {}, {}, {}, {}, {}, {}, {}]", sessionOTI.getId(), sessionOTI.getSecureElement(), sessionOTI.getSessionId(),
                        sessionOTI.getStatus(), sessionOTI.getTransactionId(), sessionOTI.getStatus().toString(), sessionOTI.getCreatedDate(), sessionOTI.getUpdatedDate(),
                        sessionOTI.getValidDate());

                SendScriptResponse sendScriptResponse = new SendScriptResponse(sessionOTI.getTransactionId().longValue());
                sendScriptResponse.setDeliveryStatus(ReturnedStatus.TIME_OUT);
                sendScriptResponse.setConversationId(BigInteger.valueOf(sessionOTI.getSessionId().longValue()));

                sessionResponses.add(sendScriptResponse);

                sessionOTI.setTransactionId(null);
                sessionOTI.setValidDate(null);

                if (SessionStatus.TO_BE_PURGED == sessionOTI.getStatus()) {
                    entityManager.remove(sessionOTI);
                }
            }
        }

        entityManager.flush();
        entityManager.clear();

        transaction.commit();
    }
}
