/**
 * 
 */
package com.oberthur.connector.oti.service.bean;

import java.math.BigInteger;

import com.oberthur.connector.oti.config.OTIConfigKeys;

/**
 * @author Illya Krakovskyy
 *
 */
public enum OTIServiceConfigKeys implements OTIConfigKeys {
    DEFAULT_VALIDITY_PERIOD("tsm.default.validity.period", "3600"),
    DROP_IMMEDIATELY("tsm.drop.immediately", "true"),
    LOGIN("tsm.login", "login"),
    PASSWORD("tsm.password", "password");

    /**
     * 
     */
    private String configKey;
    /**
     * 
     */
    private String defaultValue;

    /**
     * @param configKey
     * @param defaultValue
     */
    private OTIServiceConfigKeys(String configKey, String defaultValue) {
        this.configKey = configKey;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getConfigKey() {
        return configKey;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public BigInteger getNumericDefaultValue() {
        return BigInteger.valueOf(Long.parseLong(defaultValue));
    }

}
