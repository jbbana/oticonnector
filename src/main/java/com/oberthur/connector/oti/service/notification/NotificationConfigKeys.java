/**
 * 
 */
package com.oberthur.connector.oti.service.notification;

import java.math.BigInteger;

import com.oberthur.connector.oti.config.OTIConfigKeys;

/**
 * @author Illya Krakovskyy
 * 
 *         Configuration keys for notification
 *
 */
public enum NotificationConfigKeys implements OTIConfigKeys {
    NOTIFICATION_ENABLED("notification.enabled", "false"),
    NOTIFICATION_URL("notification.url", "http://localhost:9091/notifications"),
    NOTIFICATION_SUBJECT_ID("notification.subjectId", "123"),
    NOTIFICATION_TTL("notification.ttl", "1"),
    NOTIFICATION_OTI_URL("notification.oti.url", "http://localhost:8080/se-connector-oti/ram");

    private String configKey;

    private String defaultValue;

    /**
     * @param configKey
     * @param defaultValue
     */
    private NotificationConfigKeys(String configKey, String defaultValue) {
        this.configKey = configKey;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getConfigKey() {
        return configKey;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public BigInteger getNumericDefaultValue() {
        try {
            return BigInteger.valueOf(Long.parseLong(defaultValue));
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
