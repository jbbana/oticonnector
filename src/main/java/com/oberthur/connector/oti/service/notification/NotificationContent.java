package com.oberthur.connector.oti.service.notification;

/**
 *
 * Base interface for the notification payload content,
 * send to the notification server.
 *
 * @author Michał Kamiński, OT R&D Poland
 *
 */
public interface NotificationContent {
    public String notificationType();
}
