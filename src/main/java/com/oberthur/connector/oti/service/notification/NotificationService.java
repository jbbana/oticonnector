package com.oberthur.connector.oti.service.notification;

import com.oberthur.connector.api.SecureElementType;

/**
 * Notification service
 *
 * @author Michal Kaminski, OT R&D Poland
 */
public interface NotificationService {
    public void sendSessionStartNotification(final String seId, final SecureElementType seType, String sessionId);
}
