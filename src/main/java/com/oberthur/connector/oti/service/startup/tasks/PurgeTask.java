/**
 * 
 */
package com.oberthur.connector.oti.service.startup.tasks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.api.ReturnedStatus;
import com.oberthur.connector.api.xml.bind.response.PrepareCommunicationResponse;
import com.oberthur.connector.common.rest.callback.ConnectorServiceCallbackBean;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.domain.SessionOTI_;
import com.oberthur.connector.oti.dto.SessionStatus;
import com.oberthur.connector.oti.interceptor.monitoring.DataAccessMonitoring;
import com.oberthur.connector.oti.interceptor.monitoring.PurgeMonitoring;
import com.oberthur.connector.oti.interceptor.monitoring.PurgeMonitoring.PurgeType;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;

/**
 * @author Illya Krakovskyy
 *
 */
public class PurgeTask extends TimerTask {

    /**
     * Logger facility
     */
    private static final Logger log = LoggerFactory.getLogger(PurgeTask.class);

	/**
	 * Connector to Sequencer REST service
	 */
	@Inject
	private ConnectorServiceCallbackBean callback;

	@Inject
	private OTIPersistenceUtil persistenceUtil;

    /**
     * Number of sessions purged. Assumed to read by intercepter
     */
    private List<String> purgedSEIDs = new ArrayList<String>();

	    /**
     * @see java.util.TimerTask#run()Ye
     */
	@Override

    @DataAccessMonitoring
    @PurgeMonitoring(purgeType = PurgeType.PURGE)
	public void run() {
		EntityManager entityManager = persistenceUtil.getEntityManager();

		EntityTransaction transaction = entityManager.getTransaction();
		try {
			log.debug("Purge procedure execution...");

			List<PrepareCommunicationResponse> sessionResponses = new ArrayList<>();

			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<SessionOTI> criteriaQuery = criteriaBuilder.createQuery(SessionOTI.class);
			Root<SessionOTI> sessionRoot = criteriaQuery.from(SessionOTI.class);

			criteriaQuery.select(sessionRoot).where(
					criteriaBuilder.and(criteriaBuilder.equal(sessionRoot.get(SessionOTI_.status), SessionStatus.INITIATED),
							criteriaBuilder.lessThan(sessionRoot.get(SessionOTI_.purgeDate), new Date())));

			TypedQuery<SessionOTI> theQuery = entityManager.createQuery(criteriaQuery);
			List<SessionOTI> resultList = theQuery.getResultList();

			transaction.begin();
			for (SessionOTI session : resultList) {
				long purgeTime = session.getPurgeDate().getTime() - session.getUpdatedDate().getTime();
				log.info("Purged SessionOTI[{}, {}, {}, {}, {}, {}, {}, {}]", session.getId(), session.getSecureElement(),
						session.getSessionId(), session.getStatus(), session.getTransactionId(), session.getCreatedDate(),
						session.getUpdatedDate(), purgeTime);
				PrepareCommunicationResponse response = new PrepareCommunicationResponse(session.getSessionId().toString());
				response.setTimeToLive(Long.valueOf(purgeTime).intValue());
				response.setSessionStatus(ReturnedStatus.TIME_OUT);

				sessionResponses.add(response);

                purgedSEIDs.add(session.getSecureElement());

				entityManager.remove(session);
			}

			entityManager.flush();
			entityManager.clear();

			for (PrepareCommunicationResponse response : sessionResponses) {
				callback.prepareCommunicationResponse(response);
			}
			sessionResponses.clear();

            log.debug("{} sessions purged succesfully", purgedSEIDs.size());

			transaction.commit();
		} catch (PersistenceException e) {
			log.error("Purge procedure encountered some problems - " + e.getMessage());
			if (transaction.isActive()) {
				transaction.rollback();
			}
		} finally {
			entityManager.close();
		}
	}

    /**
     * @return the purgedNumber
     */
    public String[] getPurgedSEIds() {
        return purgedSEIDs.toArray(new String[purgedSEIDs.size()]);
    }

}
