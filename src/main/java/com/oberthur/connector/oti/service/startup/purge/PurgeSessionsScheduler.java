/**
 * 
 */
package com.oberthur.connector.oti.service.startup.purge;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.common.rest.callback.ConnectorServiceCallbackBean;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.domain.SessionOTI_;
import com.oberthur.connector.oti.dto.SessionStatus;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;

/**
 * @author Illya Krakovskyy
 *
 */
@ApplicationScoped
public class PurgeSessionsScheduler {

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(PurgeSessionsScheduler.class);

    @Inject
    private OTIPersistenceUtil persistenceUtil;

    @Inject
    private ConnectorServiceCallbackBean callback;

    private ScheduledExecutorService scheduledExecutor;

    private Map<BigInteger, ScheduledFuture<BigInteger>> scheduledToPurge;

    private Map<BigInteger, ScheduledFuture<BigInteger>> scheduledToClean;

    @PostConstruct
    public void initSchedule() {
        scheduledExecutor = Executors.newScheduledThreadPool(20);
        scheduledToPurge = new HashMap<>();
        scheduledToClean = new HashMap<>();
    }

    /**
     * Schedule session for purge
     * 
     * @param sessionId
     * @param purgeDateMillis
     */
    public void scheduleForPurge(BigInteger sessionId, long purgeDateMillis) {
        long calculateDelay = calculateDelay(purgeDateMillis);
        log.debug("Scheduling purge for session id {} for {} milliseconds", sessionId.toString(), String.valueOf(calculateDelay));
        PurgeCallable purgeCallable = new PurgeCallable(sessionId, persistenceUtil, callback);
        scheduledToPurge.put(sessionId, scheduledExecutor.schedule(purgeCallable, calculateDelay, TimeUnit.MILLISECONDS));
    }

    /**
     * Schedule session for clean
     * 
     * @param sessionId
     * @param validDateMillis
     */
    public void scheduleForClean(BigInteger sessionId, long validDateMillis) {
        long delay = calculateDelay(validDateMillis);

        log.debug("Scheduling clean for session id {} for {} milliseconds", sessionId.toString(), String.valueOf(delay));

        CleanCallable cleanCallable = new CleanCallable(sessionId, persistenceUtil, callback);
        scheduledExecutor.schedule(cleanCallable, delay, TimeUnit.MILLISECONDS);
    }

    /**
     * Cancels previously scheduled purge for session identified by
     * <code>sessionId</code>
     * 
     * @param sessionId
     * @param isPurge TODO
     */
    public void removeFromScheduled(BigInteger sessionId, boolean isPurge) {
        log.debug("Unscheduling {} for session id {} ", isPurge ? "purge" : "clean", sessionId.toString());

        ScheduledFuture<BigInteger> scheduledFuture = isPurge ? scheduledToPurge.get(sessionId) : scheduledToClean.get(sessionId);
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);

            if (isPurge) {
                scheduledToPurge.remove(sessionId);
            } else {
                scheduledToClean.remove(sessionId);
            }
        }
    }

    /**
     * Executed at connector startup. Scans for existing sessions, finds those
     * that are to purge and clean and schedules according to their purge date
     * and validity date. For those sessions which purge dates and validity
     * dates are in the past and they comply purge/clean criteria - purge and
     * clean them immediately
     */
    public void schedulePurgeAndClean() {
        Map<BigInteger, Date> purgeSessions = new HashMap<BigInteger, Date>();
        Map<BigInteger, Date> cleanSessions = new HashMap<BigInteger, Date>();

        EntityManager entityManager = persistenceUtil.getEntityManager();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> purgeQuery = criteriaBuilder.createTupleQuery();
        Root<SessionOTI> purgeRoot = purgeQuery.from(SessionOTI.class);

        Predicate forPurge = criteriaBuilder.equal(purgeRoot.get(SessionOTI_.status), SessionStatus.INITIATED);
        Predicate forClean = criteriaBuilder.and(criteriaBuilder.isNotNull(purgeRoot.get(SessionOTI_.transactionId)),
                purgeRoot.get(SessionOTI_.status).in(SessionStatus.OPENED, SessionStatus.TO_BE_PURGED));

        Predicate purgePredicate = criteriaBuilder.and(forPurge, criteriaBuilder.greaterThan(purgeRoot.get(SessionOTI_.purgeDate), new Date()));
        Predicate cleanPredicate = criteriaBuilder.and(forClean, criteriaBuilder.greaterThan(purgeRoot.get(SessionOTI_.validDate), new Date()));

        List<Tuple> purgeList = entityManager.createQuery(purgeQuery.multiselect(purgeRoot.get(SessionOTI_.id), purgeRoot.get(SessionOTI_.purgeDate)).where(purgePredicate))
                .getResultList();

        for (Tuple tuple : purgeList) {
            BigInteger sessionID = tuple.get(0, BigInteger.class);
            Timestamp purgeDate = tuple.get(1, Timestamp.class);

            purgeSessions.put(sessionID, purgeDate);
        }
        scheduleSessions(purgeSessions, true);

        List<Tuple> cleanList = entityManager.createQuery(purgeQuery.multiselect(purgeRoot.get(SessionOTI_.id), purgeRoot.get(SessionOTI_.validDate)).where(cleanPredicate))
                .getResultList();
        for (Tuple tuple : cleanList) {
            BigInteger sessionID = tuple.get(0, BigInteger.class);
            Timestamp validDate = tuple.get(1, Timestamp.class);

            cleanSessions.put(sessionID, validDate);
        }
        scheduleSessions(cleanSessions, false);

        // Handle expired sessions
        CriteriaQuery<SessionOTI> criteriaQuery = criteriaBuilder.createQuery(SessionOTI.class);
        Root<SessionOTI> sessionRoot = criteriaQuery.from(SessionOTI.class);

        handleSessions(entityManager,
                criteriaQuery.select(sessionRoot).where(criteriaBuilder.and(forPurge, criteriaBuilder.lessThanOrEqualTo(purgeRoot.get(SessionOTI_.purgeDate), new Date()))), true);

        handleSessions(entityManager,
                criteriaQuery.select(sessionRoot).where(criteriaBuilder.and(forClean, criteriaBuilder.lessThanOrEqualTo(purgeRoot.get(SessionOTI_.validDate), new Date()))), false);
    }

    /**
     * Calculates schedule delay
     * 
     * @param dateMillis
     * @return
     */
    private long calculateDelay(long dateMillis) {
        long currentTimeMillis = System.currentTimeMillis();
        return dateMillis > currentTimeMillis ? dateMillis - currentTimeMillis : 0;
    }

    /**
     * @param entityManager
     * @param criteriaQuery
     * @param isPurge
     */
    private void handleSessions(EntityManager entityManager, CriteriaQuery<SessionOTI> criteriaQuery, boolean isPurge) {
        List<SessionOTI> sessions = entityManager.createQuery(criteriaQuery).getResultList();
        PurgeHelper.handleSessions(isPurge, entityManager, callback, sessions.toArray(new SessionOTI[sessions.size()]));
    }

    /**
     * @param sessionsMap
     * @param isPurge
     */
    private void scheduleSessions(Map<BigInteger, Date> sessionsMap, boolean isPurge) {
        Set<Entry<BigInteger, Date>> entrySet = sessionsMap.entrySet();
        for (Entry<BigInteger, Date> entry : entrySet) {
            BigInteger sessionId = entry.getKey();
            long delay = entry.getValue().getTime() - System.currentTimeMillis();

            Callable<BigInteger> callable = isPurge ? new PurgeCallable(sessionId, persistenceUtil, callback) : new CleanCallable(sessionId, persistenceUtil, callback);
            ScheduledFuture<BigInteger> scheduled = scheduledExecutor.schedule(callable, delay, TimeUnit.MILLISECONDS);

            log.debug("Scheduled session for {} with session id {} for {} milliseconds delay", isPurge ? "purge" : "clean", sessionId, String.valueOf(delay));

            if (isPurge) {
                scheduledToPurge.put(sessionId, scheduled);
            } else {
                scheduledToClean.put(sessionId, scheduled);
            }
        }
    }
}
