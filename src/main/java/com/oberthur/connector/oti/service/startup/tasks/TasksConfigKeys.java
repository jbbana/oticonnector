/**
 * 
 */
package com.oberthur.connector.oti.service.startup.tasks;

import java.math.BigInteger;

import com.oberthur.connector.oti.config.OTIConfigKeys;

/**
 * @author Illya Krakovskyy
 *
 */
public enum TasksConfigKeys implements OTIConfigKeys {
    CLEAN_PROCEDURE_TIME("tsm.clean.procedure.time", "00:00"),
    PURGE_PROCEDURE_TIME("tsm.purge.procedure.time", "00:00"),
    PURGE_PERIOD("tsm.purge.procedure.period", "60"),
    CLEAN_PERIOD("tsm.clean.procedure.period", "60");

    /**
     * Configuration key
     */
    private String configKey;
    /**
     * Default value
     */
    private String defaultValue;

    /**
     * @param configKey
     * @param defaultValue
     */
    private TasksConfigKeys(String configKey, String defaultValue) {
        this.configKey = configKey;
        this.defaultValue = defaultValue;
    }

    /**
     * @return the configKey
     */
    @Override
    public String getConfigKey() {
        return configKey;
    }

    /**
     * @return the defaultValue
     */
    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public BigInteger getNumericDefaultValue() {
        return BigInteger.valueOf(Long.parseLong(defaultValue));
    }
}
