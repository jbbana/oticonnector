/**
 * 
 */
package com.oberthur.connector.oti.service.startup.notify;

import javax.enterprise.context.Dependent;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.monitoring.Monitoring;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvent;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents;

/**
 * @author Illya Krakovskyy
 *
 */
@Dependent
public class JVMShutdownHook extends Thread {
    
    private static final Logger log = LoggerFactory.getLogger(JVMShutdownHook.class);
    
    @Inject @Monitoring private Event<OTICMonitoringEvent> event;

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        log.debug("JVM looks stopped, sending stop event");
        event.fire(new OTICMonitoringEvent(OTICMonitoringEvents.OTIC_STOP));
    }
    

}
