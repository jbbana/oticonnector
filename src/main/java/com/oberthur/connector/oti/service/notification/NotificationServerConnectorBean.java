package com.oberthur.connector.oti.service.notification;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Connector to the Notification Server
 *
 * @author Michal Kaminski, OT R&D PL
 */
@Slf4j
public class NotificationServerConnectorBean implements NotificationServerConnector {

    private final String notificationServiceUrl;
    private final String subjectId;
    private final Integer ttl;
    private final ExecutorService executorService = Executors.newFixedThreadPool(5);

    public NotificationServerConnectorBean(String notificationServiceUrl, String subjectId, Integer ttl) {
        this.notificationServiceUrl = notificationServiceUrl;
        this.subjectId = subjectId;
        this.ttl = ttl;
    }

    @Override
    public void sendNotification(final String seId, final String seType, final NotificationContent content){
        log.debug("Sending notification to: se: {}:{}, {}", seType , seId, content);
        executorService.submit(() -> {
            Client client = ClientBuilder.newClient();
            try{
                NotificationPayload payload = new NotificationPayload(content.notificationType(), content);
                Notification notification = new Notification(payload, subjectId, new NotificationIdentity(seType, seId), ttl);
                Response response = client.target(notificationServiceUrl).request().accept(MediaType.APPLICATION_JSON).buildPost(Entity.entity(notification, MediaType.APPLICATION_JSON)).invoke();
                if (response.getStatus() > 299) {
                    log.warn("HTTP: {}: {} returned for the: notification: {}", response.getStatus(), response.getEntity(), notification);
                }
                log.debug("Notification: {}, sent successfully", notification);
            }catch (Exception e){
                log.warn("could not send notification with content: {} for the SE: {}:{}, due to: {}",content , seType, seId, e);
            }finally{
                client.close();
            }
        });
    }

    /**
     * Mock for the communication to the notification server,
     * when the notifications are disabled
     */
    @Slf4j
    public static class NotificationServerConnectorMock implements NotificationServerConnector {

        @Override
        public void sendNotification(final String seId, final String seType, final NotificationContent content){
            log.info("Notification to: {}:{}, with content: {}, was not send, due to disabled notification sending.", seType, seId, content);
        }

    }
}
