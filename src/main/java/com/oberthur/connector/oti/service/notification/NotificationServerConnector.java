package com.oberthur.connector.oti.service.notification;

/*
 *
 * @author Michal Kaminski, OT R&D Poland
 */
public interface NotificationServerConnector {
    void sendNotification(String seId, String seType, NotificationContent notificationContent);
}
