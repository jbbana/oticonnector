/**
 * 
 */
package com.oberthur.connector.oti.service.startup.purge;

import java.math.BigInteger;
import java.util.concurrent.Callable;

import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.common.rest.callback.ConnectorServiceCallbackBean;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;

/**
 * @author Illya Krakovskyy
 *
 */
public class PurgeCallable extends ScheduledCallable implements Callable<BigInteger> {

    private static final Logger log = LoggerFactory.getLogger(PurgeCallable.class);

    private BigInteger sessionID;

    private ConnectorServiceCallbackBean callback;

    /**
     * @param sessionID
     * @param persistenceUtil
     */
    public PurgeCallable(BigInteger sessionID, OTIPersistenceUtil persistenceUtil, ConnectorServiceCallbackBean callback) {
        super(sessionID, persistenceUtil);
        this.callback = callback;
    }

    @Override
    public BigInteger call() throws Exception {

        try {
            log.debug("Executing session purge");

            SessionOTI sessionToPurge = findSession(true);

            BigInteger id = sessionToPurge.getId();
            PurgeHelper.handleSessions(true, entityManager, callback, sessionToPurge);

            return id;
        } catch (NoResultException nre) {
            log.info("Session with unique ID not found in INITIATE state, it might be already updated with RAM or removed earlier with another task", sessionID);
            return null;
        } catch (Exception e) {
            log.error("Exception occured while attempting to purge session", e);
            throw e;
        }
    }
}
