package com.oberthur.connector.oti.service.notification;

import static com.oberthur.connector.oti.service.notification.NotificationConfigKeys.NOTIFICATION_ENABLED;
import static com.oberthur.connector.oti.service.notification.NotificationConfigKeys.NOTIFICATION_OTI_URL;
import static com.oberthur.connector.oti.service.notification.NotificationConfigKeys.NOTIFICATION_SUBJECT_ID;
import static com.oberthur.connector.oti.service.notification.NotificationConfigKeys.NOTIFICATION_TTL;
import static com.oberthur.connector.oti.service.notification.NotificationConfigKeys.NOTIFICATION_URL;

import java.io.Serializable;
import java.util.prefs.Preferences;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import com.oberthur.connector.oti.service.startup.Startup;

/**
 * Notification service producer, based on the notification enabled/disabled
 *
 * @author Michał Kamiński, OT R&D Poland
 */
@ApplicationScoped
@Startup
public class NotificationServiceProducer implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1579909203979368897L;

    @Produces
    @ApplicationScoped
    public NotificationServiceBean createNotificationService() {

        Preferences prefs = Preferences.userNodeForPackage(getClass());
        boolean notificationEnabled = prefs.getBoolean(NOTIFICATION_ENABLED.getConfigKey(),
                Boolean.parseBoolean(NotificationConfigKeys.NOTIFICATION_ENABLED.getDefaultValue()));

        NotificationServerConnector connector;
        if (notificationEnabled) {
            Integer notificationTTL = Integer.valueOf(prefs.get(NOTIFICATION_TTL.getConfigKey(), NOTIFICATION_TTL.getDefaultValue()));
            String notificationSubjectId = prefs.get(NOTIFICATION_SUBJECT_ID.getConfigKey(), NOTIFICATION_SUBJECT_ID.getDefaultValue());
            String notificationServerURL = prefs.get(NOTIFICATION_URL.getConfigKey(), NOTIFICATION_URL.getDefaultValue());

            connector = new NotificationServerConnectorBean(notificationServerURL, notificationSubjectId, notificationTTL);
        } else {
            connector = new NotificationServerConnectorBean.NotificationServerConnectorMock();
        }
        return new NotificationServiceBean(connector, prefs.get(NOTIFICATION_OTI_URL.getConfigKey(), NOTIFICATION_OTI_URL.getDefaultValue()));
    }
}
