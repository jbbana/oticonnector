package com.oberthur.connector.oti.service.bean;

import static com.oberthur.apdugenerator.utils.Utils.hexStringToByteArray;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.api.SecureElementType;
import com.oberthur.connector.oti.domain.SessionOTI;
import com.oberthur.connector.oti.domain.SessionOTI_;
import com.oberthur.connector.oti.dto.ConnectorDTO;
import com.oberthur.connector.oti.dto.SendScriptDTO;
import com.oberthur.connector.oti.dto.SessionStatus;
import com.oberthur.connector.oti.exceptions.MissingSessionException;
import com.oberthur.connector.oti.exceptions.MissingSessionException.MissingExceptionReason;
import com.oberthur.connector.oti.exceptions.OTIException;
import com.oberthur.connector.oti.http.session.OTIAgentSession;
import com.oberthur.connector.oti.interceptor.ManageSession;
import com.oberthur.connector.oti.interceptor.MethodLogged;
import com.oberthur.connector.oti.interceptor.ScheduleSession;
import com.oberthur.connector.oti.interceptor.ScheduleSession.ScheduleType;
import com.oberthur.connector.oti.interceptor.ValidateDTO;
import com.oberthur.connector.oti.interceptor.monitoring.DataAccessMonitoring;
import com.oberthur.connector.oti.ram.state.AgentSessionState;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;
import com.oberthur.connector.oti.service.startup.purge.PurgeSessionsScheduler;

@RequestScoped
@MethodLogged
@DataAccessMonitoring
public class OTIServiceBean {

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(OTIServiceBean.class);

    @Inject
    private PurgeSessionsScheduler purgeScheduler;

    @Inject
    private OTIPersistenceUtil persistenceUtil;

    @Inject
    private OTIAgentSession agentSession;

    private EntityManager entityManager;

    /**
     *
     */
    @PostConstruct
    public void initPersistence() {
        if (this.entityManager == null || !this.entityManager.isOpen()) {
            this.entityManager = persistenceUtil.getEntityManager();
        }
    }

    /**
     *
     */
    @PreDestroy
    public void closePersistence() {
        if (this.entityManager != null && this.entityManager.isOpen()) {
            this.entityManager.close();
        }
    }

    /**
     * @param secureElement
     * @param secureElementType
     * @return
     * @throws OTIException
     */
    public boolean isSessionExist(String secureElement, SecureElementType secureElementType) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<SessionOTI> sessionRoot = criteriaQuery.from(SessionOTI.class);
        criteriaQuery.select(criteriaBuilder.count(sessionRoot))
                .where(criteriaBuilder.and(criteriaBuilder.equal(sessionRoot.get(SessionOTI_.secureElement), secureElement),
                        criteriaBuilder.equal(sessionRoot.get(SessionOTI_.secureElementType), secureElementType),
                        criteriaBuilder.isNull(sessionRoot.get(SessionOTI_.transactionId))));

        TypedQuery<Long> query = entityManager.createQuery(criteriaQuery);
        return query.getSingleResult() != 0;
    }

    /**
     * @param dto
     * @throws OTIException
     */
    @ValidateDTO
    @ManageSession(status = SessionStatus.INITIATED)
    @ScheduleSession(scheduleType = ScheduleType.REGISTER_PURGE)
    public SessionOTI prepareCommunicationInitiate(ConnectorDTO dto) {

        EntityTransaction transaction = entityManager.getTransaction();

        SessionOTI session = new SessionOTI();

        String login = dto.getLogin();
        if (login == null || login.trim().length() == 0) {
            login = Preferences.userNodeForPackage(getClass()).get(OTIServiceConfigKeys.LOGIN.getConfigKey(),
                    OTIServiceConfigKeys.LOGIN.getDefaultValue());
        }
        String password = dto.getPassword();
        if (password == null || password.trim().length() == 0) {
            password = Preferences.userNodeForPackage(getClass()).get(OTIServiceConfigKeys.PASSWORD.getConfigKey(),
                    OTIServiceConfigKeys.PASSWORD.getDefaultValue());
        }

        session.setSessionId(BigInteger.valueOf(Long.parseLong(dto.getSessionId())));

        SecureElementType secureElementType = dto.getSecureElementType();
        if (secureElementType != null) {
            session.setSecureElementType(secureElementType);
        }
        String seId = dto.getTargetSE();
        session.setSecureElement(seId);
        session.setAgentId(dto.getAgentId());

        session.setLogin(login);
        session.setPassword(password);
        session.setPurgeDate(new Timestamp(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(dto.getPurgeTime())));

        transaction.begin();
        entityManager.persist(session);
        entityManager.flush();
        entityManager.clear();
        transaction.commit();

        return session;
    }

    /**
     * @param sendScriptDTO
     *            DTO containing send script data
     * @return OK/FAIL notification
     */
    @ValidateDTO
    @ScheduleSession(scheduleType = ScheduleType.REGISTER_CLEAN)
    public SessionOTI sendScriptResponse(SendScriptDTO sendScriptDTO) throws MissingSessionException {

        EntityTransaction transaction = entityManager.getTransaction();
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<SessionOTI> criteriaQuery = criteriaBuilder.createQuery(SessionOTI.class);
            Root<SessionOTI> sessionRoot = criteriaQuery.from(SessionOTI.class);
            criteriaQuery.select(sessionRoot)
                    .where(criteriaBuilder.and(
                            criteriaBuilder.equal(sessionRoot.get(SessionOTI_.sessionId), sendScriptDTO.getConversationId().longValue()),
                            criteriaBuilder.equal(sessionRoot.get(SessionOTI_.status), SessionStatus.OPENED),
                            criteriaBuilder.isNull(sessionRoot.get(SessionOTI_.transactionId))));

            SessionOTI sessionOTI = entityManager.createQuery(criteriaQuery).getSingleResult();

            BigInteger otiSessionId = sessionOTI.getId();

            if (!agentSession.agentSessionStateExists(otiSessionId)) {
                throw new MissingSessionException(
                        String.format("The session with ConversationId %s does not exist within HTTP sessions kept alive",
                                sendScriptDTO.getConversationId()),
                        MissingExceptionReason.HTTP_SESSION_MISSED);
            } else {
                Integer validityPeriod = sendScriptDTO.getValidityPeriod();
                if (validityPeriod == null) {
                    validityPeriod = getDefaultValidityPeriod();
                }

                sessionOTI
                        .setValidDate(new Timestamp(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(validityPeriod.longValue())));
                sessionOTI.setTransactionId(sendScriptDTO.getTransactionId());

                fillRAMSessionState(sessionOTI, sendScriptDTO.getTarget(), sendScriptDTO.getScript());

                transaction.begin();
                entityManager.flush();
                entityManager.clear();
                transaction.commit();

                log.debug("SessionID={}; session updated with valid date and transaction ID {}", sessionOTI.getSessionId(),
                        sendScriptDTO.getTransactionId());

                log.debug("Checking for sent/timed out session");
                AgentSessionState sentState = agentSession.pollForSentState(otiSessionId);
                if (sentState.isTimedOut()) {
                    log.debug("Found timed out session with id {}", otiSessionId);
                    throw new MissingSessionException("Connection with Mobile Agent was dropped because of timeout",
                            MissingExceptionReason.SESSION_TIMED_OUT);
                } else {
                    log.debug("No timed out session found");
                }

                agentSession.removeSessionState(otiSessionId);
            }

            return sessionOTI;
        } catch (NoResultException nre) {
            throw new MissingSessionException(
                    String.format("The session with ConversationId %s and empty transaction ID is not opened (or doesn't exist)",
                            sendScriptDTO.getConversationId()),
                    MissingExceptionReason.SESSION_MISSED);
        }
    }

    /**
     * @param sessionId
     * @param partnerOID
     * @return
     * @throws MissingSessionException
     */
    public void dropCommunication(BigInteger sessionId, String partnerOID) throws MissingSessionException {
        EntityTransaction transaction = entityManager.getTransaction();
        try {

            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<SessionOTI> criteriaQuery = criteriaBuilder.createQuery(SessionOTI.class);
            Root<SessionOTI> sessionRoot = criteriaQuery.from(SessionOTI.class);

            criteriaQuery.select(sessionRoot)
                    .where(criteriaBuilder.equal(sessionRoot.get(SessionOTI_.sessionId), sessionId.longValue()));
            TypedQuery<SessionOTI> query = entityManager.createQuery(criteriaQuery);
            SessionOTI sessionOTI = query.getSingleResult();

            BigInteger otiSessionID = sessionOTI.getId();

            transaction.begin();
            Long transactionId = sessionOTI.getTransactionId();
            boolean dropImmediately = Preferences.userNodeForPackage(getClass()).getBoolean(
                    OTIServiceConfigKeys.DROP_IMMEDIATELY.getConfigKey(),
                    Boolean.parseBoolean(OTIServiceConfigKeys.DROP_IMMEDIATELY.getDefaultValue()));
            if (transactionId == null) {
                entityManager.remove(sessionOTI);
            } else {
                if (dropImmediately) {
                    log.debug("Drop session immediately set to true, dropping now");
                    purgeScheduler.removeFromScheduled(sessionId, false);
                    entityManager.remove(sessionOTI);
                } else {
                    log.debug("Drop session immediately is {}, setting to_be_purged", String.valueOf(dropImmediately));
                    sessionOTI.setStatus(SessionStatus.TO_BE_PURGED);
                }
            }

            entityManager.flush();
            entityManager.clear();
            transaction.commit();

            // In case if dropCommunication called instead of sendScript, mark
            // alive session state discarded
            AgentSessionState agentSessionState = agentSession.getAgentSessionState(otiSessionID);
            if (agentSessionState != null) {
                agentSessionState.markDiscarded();
            }
        } catch (NoResultException nre) {
            throw new MissingSessionException(
                    String.format("The session with Session Id = %s is not opened (or doesn't exist)", sessionId),
                    MissingExceptionReason.SESSION_MISSED);
        }
    }

    /**
     * @param sessionOTI
     * @param target
     * @param script
     * @throws OTIException
     */
    private void fillRAMSessionState(SessionOTI sessionOTI, String target, List<byte[]> script) throws MissingSessionException {
        BigInteger otiSessionID = sessionOTI.getId();
        byte[] aid = hexStringToByteArray(target);

        AgentSessionState agentSessionState = agentSession.getAgentSessionState(otiSessionID);

        agentSessionState.setSeID(sessionOTI.getSecureElement());
        agentSessionState.setApplicationId(aid);
        agentSessionState.setScript(script != null ? script.get(0) : new byte[0]);
        agentSessionState.markReleased();
        agentSessionState.setSessionId(sessionOTI.getSessionId());
        agentSessionState.setTransactionId(sessionOTI.getTransactionId());
        agentSessionState.setSeIdType(sessionOTI.getSecureElementType());
    }

    /**
     * Obtain default validity period from OTI configuration
     *
     * @return default validity period
     * @throws NumberFormatException
     */
    private Integer getDefaultValidityPeriod() {
        int defaultValidity = Preferences.userNodeForPackage(getClass()).getInt(
                OTIServiceConfigKeys.DEFAULT_VALIDITY_PERIOD.getConfigKey(),
                OTIServiceConfigKeys.DEFAULT_VALIDITY_PERIOD.getNumericDefaultValue().intValue());
        return Integer.valueOf(defaultValidity);
    }
}
