package com.oberthur.connector.oti.service.startup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.config.ConfigurationLoader;
import com.oberthur.connector.oti.monitoring.OTICMonitoringConfigKeys;
import com.oberthur.connector.oti.service.startup.notify.JVMShutdownHook;
import com.oberthur.connector.oti.service.startup.notify.StartStopNotifier;
import com.oberthur.connector.oti.service.startup.purge.PurgeSessionsScheduler;
import com.oberthur.connector.oti.service.startup.tasks.MonitoringStatisctisTask;

@ApplicationScoped
@Startup
public class OTICStartupBean {

    /**
     * Logging facility
     */
    private static final Logger log = LoggerFactory.getLogger(OTICStartupBean.class);

    @Inject
    private PurgeSessionsScheduler purgeSessionsScheduler;

    @Inject
    private MonitoringStatisctisTask monitoringTask;

    @Inject
    private ConfigurationLoader configLoader;

    @Inject
    private StartStopNotifier startStopNotifier;

    @Inject
    private JVMShutdownHook shutdownHook;

    @PostConstruct
    public void startUp() {
        configLoader.loadConfiguration();

        purgeSessionsScheduler.schedulePurgeAndClean();

        startJobs();

        // create JVM shutdown hook
        startStopNotifier.notifyStart();

        Runtime.getRuntime().addShutdownHook(shutdownHook);
    }

    @PreDestroy
    public void shutDown() {
        startStopNotifier.notifyStop();
    }

    /**
     * Start recurrent jobs
     */
    private void startJobs() {

        log.debug("Starting jobs");
        Timer timer = new Timer(true);

        int monitoringNotifPeriodMins = Preferences.userNodeForPackage(OTICMonitoringConfigKeys.class).getInt(
                OTICMonitoringConfigKeys.MONITORING_NOTIFICATION_FREQUENCY.getConfigKey(),
                OTICMonitoringConfigKeys.MONITORING_NOTIFICATION_FREQUENCY.getNumericDefaultValue().intValue());

        try {
            long notificationPeriod = TimeUnit.MINUTES.toMillis(monitoringNotifPeriodMins);
            timer.schedule(monitoringTask, new Date(), notificationPeriod);

        } catch (NumberFormatException nfe) {
            String errMsg = String.format("Failed to schedule Clean Procedure: clean period %s in connector configuration",
                    nfe.getMessage().equals(String.valueOf((Object) null)) ? "missed" : "incorrect format specified");

            log.error(errMsg, nfe);
        }
    }

    /**
     * @param startTimeFormatted
     * @param taskExecutionPeriod
     *            TODO
     * @return
     * @throws ParseException
     */
    private Date getTaskStartTime(String startTimeFormatted, long taskExecutionPeriod) throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(new Date());
        int year = startCalendar.get(Calendar.YEAR);
        int month = startCalendar.get(Calendar.MONTH);
        int date = startCalendar.get(Calendar.DATE);

        startCalendar.setTime(sdf.parse(startTimeFormatted));
        startCalendar.set(year, month, date);

        Date startTime = startCalendar.getTime();

        return startTime;
    }
}
