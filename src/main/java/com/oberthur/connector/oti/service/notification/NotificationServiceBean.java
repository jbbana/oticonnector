package com.oberthur.connector.oti.service.notification;


import com.oberthur.connector.api.SecureElementType;

/**
 *
 * Service for sending notification
 *
 * @author Michał Kamińki, OT R&D Poland
 */
public class NotificationServiceBean implements NotificationService {

    private final String otiUrl;
    private NotificationServerConnector connector;


    public NotificationServiceBean(NotificationServerConnector connector, String otiUrl) {
        this.otiUrl = otiUrl;
        this.connector = connector;
    }

    @Override
    public void sendSessionStartNotification(final String seId, final SecureElementType seType, String sessionId){
        connector.sendNotification(seId , toNsType(seType), new OTIScript(otiUrl));
    }

    private String toNsType(SecureElementType seType) {
        switch (seType){
            case ICCID: return "uicc";
            case CPLC: return "ese";
            case CUD: return "smc";
            case TEE: return "tee";
         }
        return "unknown";
    }
}
