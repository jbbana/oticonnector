package com.oberthur.connector.oti.service.notification;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;


/**
 * Notification Server entity
 *
 * @author Michal Kaminski, OT R&D Poland
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Notification {

    private final NotificationIdentity identity;
    private final NotificationPayload payload;
    private final String subject;
    private final Integer ttl;

    @JsonCreator
    public Notification(
            @JsonProperty("payload") NotificationPayload payload,
            @JsonProperty("subject") String subject,
            @JsonProperty("identity") NotificationIdentity identity,
            @JsonProperty("ttl") Integer ttl) {
        this.identity = identity;
        this.payload = payload;
        this.subject = subject;
        this.ttl = ttl;
    }

    public NotificationIdentity getIdentity() {
        return identity;
    }

    public NotificationPayload getPayload() {
        return payload;
    }

    public String getSubject() {
        return subject;
    }

    public Integer getTtl() {
        return ttl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Notification that = (Notification) o;

        return new EqualsBuilder()
                .append(identity, that.getIdentity())
                .append(payload, that.getPayload())
                .append(subject, that.getSubject())
                .append(ttl, that.getTtl())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(identity)
                .append(payload)
                .append(subject)
                .append(ttl)
                .toHashCode();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
