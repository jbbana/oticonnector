package com.oberthur.connector.oti.service.notification;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 *
 * @author Michal Kaminski, OT R&D Poland
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationIdentity {

    private final String type;
    private final String value;

    @JsonCreator
    public NotificationIdentity(@JsonProperty("type") String type, @JsonProperty("value") String value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotificationIdentity notificationIdentity = (NotificationIdentity) o;

        return new EqualsBuilder().append(type, notificationIdentity.getType()).append(value, notificationIdentity.getValue()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(type).append(value).toHashCode();
    }

    @Override
    public String toString() {
        return "SE ID: " + type + " ~> " + value;
    }
}
