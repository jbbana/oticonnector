/**
 * 
 */
package com.oberthur.connector.oti.service.startup.tasks;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oberthur.connector.oti.domain.AverageResponseTime;
import com.oberthur.connector.oti.domain.AverageResponseTime_;
import com.oberthur.connector.oti.domain.Interfaces;
import com.oberthur.connector.oti.domain.Interfaces_;
import com.oberthur.connector.oti.domain.Requests;
import com.oberthur.connector.oti.domain.Requests_;
import com.oberthur.connector.oti.interceptor.monitoring.DataAccessMonitoring;
import com.oberthur.connector.oti.monitoring.InterfacesNames;
import com.oberthur.connector.oti.monitoring.Monitoring;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvent;
import com.oberthur.connector.oti.monitoring.events.OTICMonitoringEvents;
import com.oberthur.connector.oti.service.OTIPersistenceUtil;

/**
 * @author Illya Krakovskyy
 * 
 *         Collects monitoring statistics and send them out
 *
 */
public class MonitoringStatisctisTask extends TimerTask {

    private static final Logger log = LoggerFactory.getLogger(MonitoringStatisctisTask.class);

    @Inject
    @Monitoring
    private Event<OTICMonitoringEvent> event;

    @Inject
    private OTIPersistenceUtil persistenceUtil;

    /*
     * (non-Javadoc)
     * 
     * @see java.util.TimerTask#run()
     */
    @Override
    @DataAccessMonitoring
    public void run() {

        EntityManager entityManager = persistenceUtil.getEntityManager();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> tupleQuery = criteriaBuilder.createTupleQuery();

        List<OTICMonitoringEvent> statisticsEventsList = populateStatisticsEvents(entityManager, criteriaBuilder, tupleQuery);

        statisticsEventsList.addAll(populateAverageResponseTimes(entityManager, tupleQuery));

        entityManager.close();

        for (OTICMonitoringEvent oticMonitoringEvent : statisticsEventsList) {
            event.fire(oticMonitoringEvent);
        }

    }

    /**
     * @param entityManager
     * @param criteriaBuilder
     * @param tupleQuery
     *            TODO
     * @return
     */
    private List<OTICMonitoringEvent> populateStatisticsEvents(EntityManager entityManager, CriteriaBuilder criteriaBuilder, CriteriaQuery<Tuple> tupleQuery) {

        List<OTICMonitoringEvent> eventsList = new ArrayList<OTICMonitoringEvent>();

        Root<Requests> reqsRoot = tupleQuery.from(Requests.class);
        Join<Requests, Interfaces> requestsInterfaces = reqsRoot.join(Requests_.interfaceName);

        tupleQuery.multiselect(reqsRoot, requestsInterfaces.get(Interfaces_.interfaceName)).where(
                criteriaBuilder.or(criteriaBuilder.lessThan(reqsRoot.get(Requests_.lastNotificationTime), new Date()),
                        criteriaBuilder.isNull(reqsRoot.get(Requests_.lastNotificationTime))));

        List<Tuple> results = entityManager.createQuery(tupleQuery).getResultList();

        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        int requestsCounter = 0;
        int errorsCounter = 0;
        for (Tuple tuple : results) {
            Requests requests = tuple.get(0, Requests.class);
            InterfacesNames interfaceName = tuple.get(1, InterfacesNames.class);

            BigInteger requestsNumber = requests.getRequestsNumber();
            BigInteger errorsNumber = requests.getErrorsNumber();

            long periodInSeconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - requests.getLastNotificationTime().getTime());

            long calculatedRequests = calculateHits(requestsNumber, periodInSeconds);
            long calculatedErrors = calculateHits(errorsNumber, periodInSeconds);

            if (calculatedRequests > 0) {
                eventsList.add(new OTICMonitoringEvent(OTICMonitoringEvents.INCOMING_REQUESTS, calculatedRequests, interfaceName.toString()));
                requestsCounter++;
            }
            if (calculatedErrors > 0) {
                eventsList.add(new OTICMonitoringEvent(OTICMonitoringEvents.INCOMING_ERRORS, calculatedErrors, interfaceName.toString()));
                errorsCounter++;
            }

            requests.refreshStatistics();
        }

        log.debug("{} requests statistics hits: {} requests and {} requests answered with errors", eventsList.size(), requestsCounter, errorsCounter);

        entityManager.flush();
        transaction.commit();

        return eventsList;
    }

    /**
     * @param entityManager
     * @param tupleQuery
     * @return
     */
    private List<OTICMonitoringEvent> populateAverageResponseTimes(EntityManager entityManager, CriteriaQuery<Tuple> tupleQuery) {
        List<OTICMonitoringEvent> avgStatisticsList = new ArrayList<OTICMonitoringEvent>();

        Root<AverageResponseTime> avgRoot = tupleQuery.from(AverageResponseTime.class);
        Join<AverageResponseTime, Interfaces> ifaceJoin = avgRoot.join(AverageResponseTime_.interfaceName);
        tupleQuery.multiselect(avgRoot, ifaceJoin.get(Interfaces_.interfaceName));

        List<Tuple> result = entityManager.createQuery(tupleQuery).getResultList();

        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        for (Tuple tuple : result) {
            AverageResponseTime avgRespTime = tuple.get(0, AverageResponseTime.class);
            InterfacesNames interfaceName = tuple.get(1, InterfacesNames.class);

            long avgResponseTime = avgRespTime.getAverageResponseTime().longValue();
			long hitsCounter = avgRespTime.getHitsCounter().longValue();

			if (!(avgResponseTime == 0 && hitsCounter == 0)) {
				avgStatisticsList.add(new OTICMonitoringEvent(OTICMonitoringEvents.INCOMING_AVG_RESPONSE_TIME, avgResponseTime, interfaceName.toString()));
			}
			avgRespTime.refreshStatistics();
        }

        log.debug("{} average response times calculated", avgStatisticsList.size());

        entityManager.flush();
        transaction.commit();

        return avgStatisticsList;
    }

    /**
     * @param hitsNumber
     * @param periodInSeconds
     * @return
     */
    private long calculateHits(BigInteger hitsNumber, long periodInSeconds) {
        long calculatedValue = new BigDecimal(hitsNumber).divide(new BigDecimal(periodInSeconds), RoundingMode.DOWN).longValue();
        return calculatedValue == 0 && !BigInteger.ZERO.equals(hitsNumber) ? BigInteger.ONE.longValue() : calculatedValue;
    }

}
