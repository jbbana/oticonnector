/**
 * 
 */
package com.oberthur.connector.oti.service;

import java.math.BigInteger;

import com.oberthur.connector.oti.config.OTIConfigKeys;

/**
 * @author Illya Krakovskyy
 *
 */
public enum OTIConnectorConfigKeys implements OTIConfigKeys {
    PURGE_TIME("tsm.default.purge.time", "3600");

    /**
     * Configuration key
     */
    private String configKey;
    /**
     * Default value
     */
    private String defaultValue;

    private OTIConnectorConfigKeys(String configKey, String defaultValue) {
        this.configKey = configKey;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getConfigKey() {
        return configKey;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public BigInteger getNumericDefaultValue() {
        return BigInteger.valueOf(Long.parseLong(defaultValue));
    }

}
