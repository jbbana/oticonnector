package com.oberthur.connector.oti.service.notification;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * Content of the notification about new session appear in OTI,
 * contains the URL with the script.
 *
 * @author Michal Kaminski, Oberthur R&D
 */
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE)
public class OTIScript implements NotificationContent{

    private static final String NOTIFICATION_TYPE="OTIScript";

    @JsonProperty("url")
    private String url;

    public OTIScript(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String notificationType() {
        return NOTIFICATION_TYPE;
    }

    @Override
    public String toString() {
        return "OTIScript{" +
                "url='" + url + '\'' +
                '}';
    }
}