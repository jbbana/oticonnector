/**
 * 
 */
package com.oberthur.connector.api;

/**
 * @author Illya Krakovskyy
 *
 */
public enum SecureElementType {
	ICCID,
	CUD,
	CPLC,
	TEE
}
