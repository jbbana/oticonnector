/**
 * 
 */
package com.oberthur.connector.api;

/**
 * @author Illya Krakovskyy
 *
 */
public enum RequestedScriptFormatEnum {
	COMPACT,
	EXPANDED
}
