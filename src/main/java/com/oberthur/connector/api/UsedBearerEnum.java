/**
 * 
 */
package com.oberthur.connector.api;

/**
 * @author Illya Krakovskyy
 *
 */
public enum UsedBearerEnum {
	SMS_TO_UICC(1),
	CAT_TP_TO_UICC(2),
	HTTP_TO_UICC(3),
	SMS_TO_ESE_SMC(11),
	HTTP_TO_ESE_SMC(12);

	private int code;

	private UsedBearerEnum(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public static UsedBearerEnum getByCode(int code) {
		for (UsedBearerEnum value : UsedBearerEnum.values()) {
			if (value.getCode() == code) {
				return value;
			}
		}
		throw new IllegalArgumentException("No UsedBearerEnum with code " + code);
	}
}
