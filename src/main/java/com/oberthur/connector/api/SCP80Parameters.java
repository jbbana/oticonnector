package com.oberthur.connector.api;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.oberthur.apdugenerator.keyset.KeySet;
import com.oberthur.scp80module.SCP80Security;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class SCP80Parameters implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8357007896528461092L;
	private KeySet scp80KeySet;
	private KeySet scp80TriggerKeySet;
	private SCP80Security scp80Security;
	private SCP80Security scp80TriggerSecurity;

	public SCP80Parameters(KeySet scp80KeySet, KeySet scp80TriggerKeySet, SCP80Security scp80Security) {
		this.scp80KeySet = scp80KeySet;
		this.scp80TriggerKeySet = scp80TriggerKeySet;
		this.scp80Security = scp80Security;
	}

	public SCP80Parameters(KeySet scp80KeySet, KeySet scp80TriggerKeySet, SCP80Security scp80Security, SCP80Security scp80TriggerSecurity) {
		this(scp80KeySet, scp80TriggerKeySet, scp80Security);
		this.scp80TriggerSecurity = scp80TriggerSecurity;
	}

	public KeySet getScp80KeySet() {
		return scp80KeySet;
	}

	public KeySet getScp80TriggerKeySet() {
		return scp80TriggerKeySet;
	}

	public SCP80Security getScp80Security() {
		return scp80Security;
	}

	public SCP80Security getScp80TriggerSecurity() {
		return scp80TriggerSecurity;
	}
}
