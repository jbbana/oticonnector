/**
 * 
 */
package com.oberthur.connector.api;

/**
 * @author Illya Krakovskyy
 *
 */
public enum SessionParamEnum {
	MOBILE_SUBSCRIPTION,
	SERVICE_ID,
	MAJOR,
	MINOR,
	REVISION,
	REQ_SCRIPT_FORMAT,
	ESTIM_APDU_SCRIPT_SIZE,
	BANK_ID,
	ACCOUNT_ID,
	ACCOUNT_PASSWORD,
	CUSTOMER_REFERENCE_ID,
	ISSUER_OID,
	USER_ID,
	PURGE_TIME,
	SE_BLOCK_SIZE,
	APPLICATION_OID
}
