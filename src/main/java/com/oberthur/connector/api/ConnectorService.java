package com.oberthur.connector.api;

import java.math.BigInteger;

import com.oberthur.connector.api.xml.bind.notification.Notification;
import com.oberthur.connector.api.xml.bind.request.PrepareCommunicationRequest;
import com.oberthur.connector.api.xml.bind.request.SendScriptRequest;

public interface ConnectorService {

	Notification disconnect(BigInteger sessionId, String partnerOID);

	Notification prepareCommunicationRequest(PrepareCommunicationRequest request);

	Notification sendScriptRequest(SendScriptRequest sendScriptRequest);
}
