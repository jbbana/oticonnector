package com.oberthur.connector.api;

/**
 * Indicates how the message shall be handled by targeted equipment when received (displayed on screen, stored in handset or SIM mailbox),
 * and the character encoding defined for it: Default alphabet = GSM alphabet; or Unicode.
 * Default value = TPDcs_SCREEN(0xF0)
 * DCS values with 8-bit data encoding are not supported; encoding format being dependant on the unpredictable ASCII table used by addressed handset,
 * to map each byte with a displayed character
 */
public enum DCS {

	TPDCS_NC_DA(0x00),           ///< no class, default alphabet
	TPDCS_NC_8B(0x04),           ///< no class, 8 bits Data
	TPDCS_NC_UC(0x08),           ///< no class, Unicode
	TPDCS_SCR_UC(0x18),          ///< SM Class 0 Unicode
	TPDCS_MOB_UC(0x19),          ///< SM Class 1 Unicode
	TPDCS_SIM_UC(0x1A),          ///< SM Class 2 Unicode
	TPDCS_SCREEN(0xF0),          ///< SM Class 0 Default Alphabet
	TPDCS_MOBILE(0xF1),          ///< SM Class 1 Default Alphabet
	TPDCS_SIM_DA(0xF2),          ///< SM Class 2 Default Alphabet
	TPDCS_SCR_8B(0xF4),          ///< SM Class 0 8 bits data
	TPDCS_MOB_8B(0xF5),          ///< SM Class 1 8 bits data
	TPDCS_SIM_8B(0xF6),          ///< SM Class 2 8 bits data
	TPDCS_TE_MOB_8B(0xF7);       ///< SM Class 3 8 bits Data (May be non official)

	private final int code;

	private DCS(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
