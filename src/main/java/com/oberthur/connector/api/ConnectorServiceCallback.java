package com.oberthur.connector.api;

import com.oberthur.connector.api.xml.bind.notification.Notification;
import com.oberthur.connector.api.xml.bind.response.OutOfSessionResponse;
import com.oberthur.connector.api.xml.bind.response.PrepareCommunicationResponse;
import com.oberthur.connector.api.xml.bind.response.SendScriptResponse;


/**
 * @author Kyrylo Holodnov
 */
public interface ConnectorServiceCallback {

    Notification prepareCommunicationResponse(PrepareCommunicationResponse response);

    Notification outOfSessionResponse(OutOfSessionResponse response);

    Notification sendScriptResponse(SendScriptResponse response);
}
