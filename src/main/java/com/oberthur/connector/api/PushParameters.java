package com.oberthur.connector.api;

import java.io.Serializable;

import lombok.Data;

@Data
public class PushParameters implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9099846125401624562L;
	private String ipServer;
	private int portServer;
	private String apn;
	private String login;
	private String password;
	private String registrationId;
	private String apiKey;
	private String agentId;
}
