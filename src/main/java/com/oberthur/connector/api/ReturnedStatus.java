package com.oberthur.connector.api;

public enum ReturnedStatus {

	// Global OK
	OK(0x00),                                            // Script was delivered, execution may have failed

	// SCP80 errors
	SCP80_RC_CC_DS_FAIL(0x01, Category.PERMANENT),
	SCP80_CNTR_LOW(0x02, Category.TRANSIENT),
	SCP80_CNTR_HIGH(0x03, Category.TRANSIENT),
	SCP80_CNTR_BLOCKED(0x04, Category.PERMANENT),   
	SCP80_CIPHERING_ERROR(0x05, Category.PERMANENT),
	SCP80_UNIDENTIFIED_SECURITY_ERROR(0x06, Category.PERMANENT),
	SCP80_INSUFFICIENT_MEMORY(0x07, Category.PERMANENT),
	SCP80_TIME_OUT(0x08, Category.TRANSIENT),
	SCP80_TAR_UNKNOWN(0x09, Category.PERMANENT),
	SCP80_INSUFFICIENT_SECURITY_LEVEL(0x0A, Category.PERMANENT),
	SCP80_UNWRAP_ERROR(0x65, Category.PERMANENT),   // Error during PoR unwrapping

	// Gateway ACK
	GatewayACK_CONGESTION(201, Category.TRANSIENT),       ///< try later (transient)
	GatewayACK_UNKNOWN_SUBSCRIBER(202, Category.PERMANENT),       ///< unknown subscriber (permanent)
	GatewayACK_CALL_BARRED(203, Category.PERMANENT),       ///< forbidden call for the subscriber (permanent in SM-MT sending sense)
	GatewayACK_INVALID_ADDRESS(204, Category.PERMANENT),       ///< badly formed address, not in numbering plan (permanent)
	GatewayACK_KO(205, Category.PERMANENT),      ///< default
	GatewayACK_NO_ACK(206, Category.TRANSIENT),       ///< MD did not receive ACK at validity period expiration
	GatewayACK_CONGESTION_MD(207, Category.TRANSIENT),       ///< MD is xoffed
	GatewayACK_NOT_SENT(208, Category.TRANSIENT),       ///< SM-MT could not be send

	// Network ACK
	NetworkDiagErr_UNKNOWN_SUBSCRIBER(301, Category.PERMANENT),               ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_TELESERVICE_NOT_PROVISIONED(302, Category.PERMANENT),     ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_CALL_BARRED(303, Category.PERMANENT),                     ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_FACILITY_NOT_SUPPORTED(304, Category.PERMANENT),          ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_ABSENT_SUBSCRIBER(305, Category.PERMANENT),               ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_MT_BUSY_FOR_MT_SMS(306, Category.TRANSIENT),              ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_SMS_LOWER_LAYERS_CAPABILITIES_NOT_PROVISIONED(307, Category.PERMANENT),                ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_ERROR_IN_MS(308, Category.TRANSIENT),                     ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_ILLEGAL_SUBSCRIBER(309, Category.PERMANENT),              ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_ILLEGAL_EQUIPMENT(310, Category.PERMANENT),               ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_SYSTEM_FAILURE(311, Category.TRANSIENT),                 ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_MEMORY_CAPACITY_EXCEEDED(312, Category.PERMANENT),     ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_SIM_SMS_STORAGE_FULL(313, Category.TRANSIENT),     ///< see GSM340 chapter 3.3.2
	NetworkDiagErr_SM_EXPIRED(314, Category.TRANSIENT),
	NetworkDiagErr_SM_DELETED(315, Category.PERMANENT),
	NetworkDiagErr_SM_UNDELIVERABLE(316, Category.PERMANENT),
	NetworkDiagErr_NO_ACK(317, Category.TRANSIENT),
	NetworkDiagErr_CONGESTION_MD(318, Category.TRANSIENT),

	TIME_OUT(401, Category.TRANSIENT),            // none answer received in time
	SESSION_WITH_CARD_EXPIRED(402, Category.TRANSIENT), // OTI-1203

	BAD_PUSH_TRANSIENT(512, Category.TRANSIENT),  
	BAD_PUSH_PERMANENT(513, Category.PERMANENT),

	// RAM HTTP  
	RAM_HTTP_UNKNOWN_APPLICATION(601, Category.PERMANENT),      // Application targeted by the previous remote APDU format string could not be found. No response string is sent.
	RAM_HTTP_NOT_A_SECURITY_DOMAIN(602, Category.PERMANENT),    // Application targeted by the previous remote APDU format string is not a Security Domain.No response string is sent.
	RAM_HTTP_SECURITY_ERROR(603, Category.PERMANENT),           // Security Domain targeted by the previous secured remote APDU format string is not able to check its security. No response string is sent.
	SE_RAM_HTTP_UNAVAILABLE_SE(604, Category.PERMANENT);        // SE targeted by the previous remote request could not be found. No response string is sent.

	private int statusCode;
	private Category category;

	private ReturnedStatus(int statusCode) {
		this(statusCode, null);
	}

	private ReturnedStatus(int statusCode, Category category) {
		this.statusCode = statusCode;
		this.category = category;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public static ReturnedStatus getStatus(int code) {
		for (ReturnedStatus status : values()) {
			if (status.getStatusCode() == code) {
				return status;
			}
		}
		return null;
	}

	private enum Category {
		TRANSIENT,
		PERMANENT,
		UNKNOWN
	}

	public boolean isPermanentError() {
		return category == Category.PERMANENT;
	}
}
