package com.oberthur.connector.api;

/**
 * Indicates the type of message sent
 * Default value = 0x00.
 */
public enum PID {

	TPPid_SIMPLE_SM(0x00),
	TPPid_SM_TYPE_0(0x40);

	private final int code;

	private PID(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
