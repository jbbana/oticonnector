package com.oberthur.connector.api;

public enum OutOfSessionParamEnum {
	MSISDN, SEID, AGENTID
}
