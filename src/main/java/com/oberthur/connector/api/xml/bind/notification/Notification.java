/**
 * 
 */
package com.oberthur.connector.api.xml.bind.notification;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * @author Illya Krakovskyy
 *
 */
public class Notification {
	@ApiModelProperty(required = true)
	private Status status;
	private String reason;
	private Integer code;

	public static Notification newNotificationOk() {
		Notification notificationResp = new Notification();
		notificationResp.setStatus(Status.OK);
		notificationResp.setCode(ResponseCode.OK.getCode());
		return notificationResp;
	}

	public static Notification newNotificationFail(String reason) {
		return newNotificationFail(reason, ResponseCode.NO_SEND_SCRIPT_SESSION);
	}

	public static Notification newNotificationFail(String reason, ResponseCode responseCode) {
		Notification notificationResp = new Notification();
		notificationResp.setStatus(Status.FAIL);
		notificationResp.setCode(responseCode.getCode());
		notificationResp.setReason(reason);
		return notificationResp;
	}

	private Notification() {
	}

	public enum Status {
		OK, FAIL
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
}
