package com.oberthur.connector.api.xml.bind.response;

import java.math.BigInteger;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

import com.oberthur.apdugenerator.utils.Utils;
import com.oberthur.connector.api.ReturnedStatus;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@Data
@XmlRootElement(name = "sendScriptResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value = "SendScriptResponse", description = "SendScriptResponse object to Sequencer")
public class SendScriptResponse {

	@ApiModelProperty(required = true)
	private long transactionId;
	@XmlElementWrapper
	@XmlElement(name = "apduBytes")
	private List<byte[]> responseAPDU;
	private byte[] sw;
	private BigInteger conversationId;
	private Integer failedAPDUIndex;
	private ReturnedStatus deliveryStatus;

	public SendScriptResponse() {
	}

	public SendScriptResponse(long transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		return "SendScriptResponse{" +
				"transactionId=" + transactionId +
				", responseAPDU=" + (responseAPDU != null ? responsesToString() : "null") +
				", sw=" + (sw != null ? Utils.byteArrayToHexString(sw) : "null") +
				", conversationId=" + conversationId +
				", failedAPDUIndex=" + failedAPDUIndex +
				", deliveryStatus=" + deliveryStatus +
				'}';
	}

	private String responsesToString() {
		StringBuilder sb = new StringBuilder();
		for (byte[] response : responseAPDU) {
			sb.append("[").append(Utils.byteArrayToHexString(response)).append("]");
		}
		return sb.toString();
	}
}
