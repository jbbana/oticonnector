package com.oberthur.connector.api.xml.bind.request;

public abstract class AbstractRequest {

	public abstract void checkMandatoryParams();

	protected void checkTargetIdentifier(byte[] targetId) {
		if (targetId.length != 3 && (targetId.length < 5 || targetId.length > 16)) {
			throw new IllegalArgumentException("Target identifier length is not correct");
		}
	}
}
