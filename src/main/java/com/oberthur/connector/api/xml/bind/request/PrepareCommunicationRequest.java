package com.oberthur.connector.api.xml.bind.request;

import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.oberthur.apdugenerator.keyset.KeySet;
import com.oberthur.apdugenerator.utils.Utils;
import com.oberthur.connector.api.PushParameters;
import com.oberthur.connector.api.RequestedScriptFormatEnum;
import com.oberthur.connector.api.SCP80Parameters;
import com.oberthur.connector.api.SecureElementType;
import com.oberthur.connector.api.SessionParamEnum;
import com.oberthur.connector.api.UsedBearerEnum;
import com.oberthur.scp80module.SCP80Security;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@Data
@EqualsAndHashCode(callSuper = false)
@XmlRootElement(name = "prepareCommunicationRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value = "PrepareCommunicationRequest", description = "PrepareCommunicationRequest object to connector")
public class PrepareCommunicationRequest extends AbstractRequest {

	@ApiModelProperty(required = true)
	@NotNull(message = "Partner OID is empty")
	private String partnerOID;
	@NotNull(message = "Secure element should not be null")
	private String secureElement;
	@ApiModelProperty(required = true)
	@NotNull(message = "Target identifier should not be null")
	@Size(min = 1, message = "Target identitifier should not be empty")
	private String targetIdentifier;
	@ApiModelProperty(required = true)
	@NotNull(message = "Session id is empty")
	private String sessionID;
	private String applicationOID;
	private UsedBearerEnum bearer;
	private String applicationSessionId;
	private String callbackUrl;

	private String mobileSubscription;
	private Long serviceId;
	private Long majorVersion;
	private Long minorVersion;
	private Long revision;
	private RequestedScriptFormatEnum requestedScriptFormat;
	private Integer estimatedAPDUScriptSize;
	private String bankId;
	private String accountId;
	private String accountPassword;
	private String customerReferenceId;
	private String issuerOid;
	private String userId;
	private Integer maxResponseDelay;
	private Integer seBlockSize;

	private KeySet scp80KeySet;
	private KeySet scp80TriggerKeySet;
	private SCP80Security scp80Security;
	private SCP80Security scp80TriggerSecurity;

	private String ipServer;
	private Integer portServer;
	private String apn;
	private String login;
	private String password;
	private String registrationId;
	private String apiKey;

	@NotNull(message = "Agent identifier should not be null")
	@Size(min = 1, message = "Agent identitifier should not be empty")
	private String agentId;

	private SecureElementType secureElementType = SecureElementType.ICCID;

	public static PrepareCommunicationRequest newInstance(String partnerOID,
	                                                      String seid,
	                                                      String targetId,
	                                                      String sessionID,
	                                                      UsedBearerEnum bearer,
	                                                      Map<SessionParamEnum, String> params,
	                                                      String appSessionId) {
		PrepareCommunicationRequest request = newInstance(partnerOID, seid, targetId, sessionID);
		request.setBearer(bearer != null ? bearer : null);
		request.applicationSessionId = appSessionId;
		request.parseParamsMap(params);
		return request;
	}

	public static PrepareCommunicationRequest newInstance(String partnerOID, String seid, String targetId, String sessionID) {
		PrepareCommunicationRequest request = new PrepareCommunicationRequest();
		request.partnerOID = partnerOID;
		request.secureElement = seid;
		request.targetIdentifier = targetId;
		request.sessionID = sessionID;
		request.checkMandatoryParams();
		return request;
	}

	@Override
	public void checkMandatoryParams() {
		checkTargetIdentifier(Utils.hexStringToByteArray(targetIdentifier));
	}

	private void parseParamsMap(Map<SessionParamEnum, String> params) {
		this.serviceId = Long.parseLong(params.get(SessionParamEnum.SERVICE_ID));
		this.requestedScriptFormat = RequestedScriptFormatEnum.valueOf(params.get(SessionParamEnum.REQ_SCRIPT_FORMAT));
		this.estimatedAPDUScriptSize = Integer.parseInt(params.get(SessionParamEnum.ESTIM_APDU_SCRIPT_SIZE));
		this.majorVersion = Long.parseLong(params.get(SessionParamEnum.MAJOR));
		this.minorVersion = Long.parseLong(params.get(SessionParamEnum.MINOR));
		this.revision = Long.parseLong(params.get(SessionParamEnum.REVISION));

		this.mobileSubscription = params.get(SessionParamEnum.MOBILE_SUBSCRIPTION);
		this.bankId = params.get(SessionParamEnum.BANK_ID);
		this.accountId = params.get(SessionParamEnum.ACCOUNT_ID);
		this.accountPassword = params.get(SessionParamEnum.ACCOUNT_PASSWORD);
		this.customerReferenceId = params.get(SessionParamEnum.CUSTOMER_REFERENCE_ID);
		this.issuerOid = params.get(SessionParamEnum.ISSUER_OID);
		this.userId = params.get(SessionParamEnum.USER_ID);

		this.maxResponseDelay = Integer.parseInt(params.get(SessionParamEnum.PURGE_TIME));
		this.seBlockSize = Integer.parseInt(params.get(SessionParamEnum.SE_BLOCK_SIZE));

		this.applicationOID = params.get(SessionParamEnum.APPLICATION_OID);
	}

	public void addPushParamsToRequest(@NotNull(message = "Push parameters are null") PushParameters pushParams) {
		setIpServer(pushParams.getIpServer());
		setPortServer(pushParams.getPortServer());
		setApn(pushParams.getApn());
		setLogin(pushParams.getLogin());
		setPassword(pushParams.getPassword());
		setRegistrationId(pushParams.getRegistrationId());
		setApiKey(pushParams.getApiKey());
		setAgentId(pushParams.getAgentId());
	}

	public void addSCP80ParamsToRequest(@NotNull(message = "SCP80 parameters are null") SCP80Parameters scp80Params) {
		setScp80KeySet(scp80Params.getScp80KeySet());
		setScp80TriggerKeySet(scp80Params.getScp80TriggerKeySet());
		setScp80Security(scp80Params.getScp80Security());
		setScp80TriggerSecurity(scp80Params.getScp80TriggerSecurity());
	}

	public void addSecureElementType(SecureElementType secureElementType) {
		if (secureElementType != null) {
			setSecureElementType(secureElementType);
		}
	}
}
