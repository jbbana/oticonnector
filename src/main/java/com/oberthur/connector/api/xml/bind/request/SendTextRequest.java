package com.oberthur.connector.api.xml.bind.request;

import static org.apache.commons.lang3.StringUtils.isEmpty;

import java.math.BigInteger;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.oberthur.connector.api.DCS;
import com.oberthur.connector.api.PID;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@Data
@EqualsAndHashCode(callSuper = false)
@XmlRootElement(name = RequestRootElementName.SEND_TEXT_REQUEST)
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value = "SendTextRequest", description = "SendTextRequest object to connector")
public class SendTextRequest extends AbstractRequest {

	@ApiModelProperty(required = true)
	@NotNull
	private BigInteger conversationId;
	@ApiModelProperty(required = true)
	@NotNull
	private String extRequestID;
	@ApiModelProperty(required = true)
	@NotNull
	private String mobileSubscription;
	@ApiModelProperty(required = true)
	@NotNull(message = "Text parameter is mandatory")
	@Size(min = 1, message = "Text is empty")
	private byte[] text;
	private Long transactionID;
	private PID pid = PID.TPPid_SIMPLE_SM;
	private DCS dcs = DCS.TPDCS_SCR_8B;
	private Boolean askNetworkAck;
	@DecimalMin(value = "5", message = "Validity period must be more then 0, default value is 5 min.")
	private Integer vp = 5;
	private String issuerOID;
	private Boolean udhi;

	public static SendTextRequest newInstance(BigInteger conversationId,
	                                          String extRequestID,
	                                          String mobileSubscription,
	                                          byte[] text,
	                                          PID pid,
	                                          DCS dcs,
	                                          boolean askNetworkAck,
	                                          int validityPeriod,
	                                          String issuerOID,
	                                          Boolean udhi) {
		SendTextRequest request = new SendTextRequest();
		request.conversationId = conversationId;
		request.extRequestID = extRequestID;
		request.mobileSubscription = mobileSubscription;
		request.text = text;

		if (pid != null) {
			request.pid = pid;
		}

		if (dcs != null) {
			request.dcs = dcs;
		}

		request.askNetworkAck = askNetworkAck;
		request.vp = validityPeriod;
		request.issuerOID = issuerOID;
		request.udhi = udhi;

		request.checkMandatoryParams();
		return request;
	}

	@Override
	public void checkMandatoryParams() {
		if (!((conversationId != null && extRequestID != null) || (!isEmpty(mobileSubscription) && !isEmpty(issuerOID)))) {
			throw new IllegalArgumentException("mobileSubscription and issuerOID must not be empty if conversationId and extRequestID are null");
		}
	}
}
