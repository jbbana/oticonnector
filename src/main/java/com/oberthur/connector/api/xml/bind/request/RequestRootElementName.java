package com.oberthur.connector.api.xml.bind.request;

public class RequestRootElementName {

	public static final String SEND_SCRIPT_REQUEST = "sendScriptRequest";
	public static final String SEND_TEXT_REQUEST = "sendTextRequest";
	public static final String SEND_SMS_RAW_SCRIPT = "sendSMSRawScript";
}
