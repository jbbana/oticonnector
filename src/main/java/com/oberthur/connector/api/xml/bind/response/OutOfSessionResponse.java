package com.oberthur.connector.api.xml.bind.response;

import static com.oberthur.connector.api.OutOfSessionParamEnum.MSISDN;
import static com.oberthur.connector.api.OutOfSessionParamEnum.SEID;

import java.util.EnumMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

import com.oberthur.connector.api.OutOfSessionParamEnum;
import com.oberthur.tsm.common.constants.UsedBearerEnum;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@Data
@XmlRootElement(name = "outOfSessionResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value = "OutOfSessionResponse", description = "OutOfSessionResponse object to Sequencer")
public class OutOfSessionResponse {

	@ApiModelProperty(required = true)
	private String originatingConnector;
	@ApiModelProperty(required = true)
	private Map<OutOfSessionParamEnum, String> sourceId = new EnumMap<>(OutOfSessionParamEnum.class);
	private String originatingEntity;
	private UsedBearerEnum usedBearer;
	@ApiModelProperty(required = true)
	private byte[] rawMessage;
	private String reason;

	public OutOfSessionResponse(String originatingConnector, byte[] rawMessage) {
		this.originatingConnector = originatingConnector;
		this.rawMessage = rawMessage;
	}

	public OutOfSessionResponse(){}

	public String getMsisdn() {
		return sourceId.get(MSISDN);
	}

	public void setMsisdn(String msisdn) {
		if (msisdn != null) {
			sourceId.put(MSISDN, msisdn);
		} else {
			sourceId.remove(MSISDN);
		}
	}

	public String getSecureElement() {
		return sourceId.get(OutOfSessionParamEnum.SEID);
	}

	public void setSecureElement(String secureElement) {
		if (secureElement != null) {
			sourceId.put(SEID, secureElement);
		} else {
			sourceId.remove(SEID);
		}
	}
}
