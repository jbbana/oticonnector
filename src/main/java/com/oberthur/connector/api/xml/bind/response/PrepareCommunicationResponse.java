package com.oberthur.connector.api.xml.bind.response;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.oberthur.connector.api.RequestedScriptFormatEnum;
import com.oberthur.connector.api.ReturnedStatus;
import com.oberthur.connector.api.UsedBearerEnum;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "prepareCommunicationResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value = "PrepareCommunicationResponse", description = "PrepareCommunicationResponse object to Sequencer")
public class PrepareCommunicationResponse {

	@ApiModelProperty(required = true)
	private String sessionId;
	private BigInteger conversationId;
	private UsedBearerEnum usedBearer;
	private int apduScriptSize;
	private Integer apdusNumber;
	private RequestedScriptFormatEnum scriptFormat;
	private int apduResponseScriptSize;
	private int timeToLive;
	private String reason;
	private ReturnedStatus sessionStatus;

	public PrepareCommunicationResponse() {
	}

	public PrepareCommunicationResponse(String sessionId) {
		this.sessionId = sessionId;
	}
}
