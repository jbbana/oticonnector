package com.oberthur.connector.api.xml.bind.request;

import java.math.BigInteger;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@Data
@EqualsAndHashCode(callSuper = false)
@XmlRootElement(name = "dropCommunicationRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value = "DropCommunicationRequest", description = "DropCommunicationRequest object to connector")
public class DropCommunicationRequest extends AbstractRequest {

	@ApiModelProperty(required = true)
	@NotNull(message="SE Connector Session Id is null") private BigInteger conversationId;
	private String partnerOID;

	@Override
	public void checkMandatoryParams() {
	}

	public static DropCommunicationRequest newInstance(BigInteger seCSessionId, String partnerOID) {
		DropCommunicationRequest request = new DropCommunicationRequest();
		request.conversationId = seCSessionId;
		request.partnerOID = partnerOID;
		request.checkMandatoryParams();
		return request;
	}

	public static DropCommunicationRequest newInstance(BigInteger seCSessionId) {
		return newInstance(seCSessionId, null);
	}
}
