/**
 * 
 */
package com.oberthur.connector.api.xml.bind.notification;

/**
 * @author Illya Krakovskyy
 *
 */
public enum ResponseCode {
	OK(200),
	INVALID_ARGUMENT(401),
	NO_SEND_SCRIPT_SESSION(410),
	SESSION_EXISTS(419),
	TIMEOUT_DROP(601);
	private final int code;

	ResponseCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
