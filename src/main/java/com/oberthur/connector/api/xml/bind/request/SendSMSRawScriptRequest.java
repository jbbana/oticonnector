/**
 * 
 */
package com.oberthur.connector.api.xml.bind.request;

import java.math.BigInteger;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.oberthur.connector.api.DCS;
import com.oberthur.connector.api.PID;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * @author Illya Krakovskyy
 *
 */
@XmlRootElement(name = RequestRootElementName.SEND_SMS_RAW_SCRIPT)
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value = "SendSMSRawScriptRequest", description = "SendSMSRawScriptRequest object to connector")
public class SendSMSRawScriptRequest {

	/**
	 * Id given by OTA connector referring to the session acknowledged by
	 * PrepareCommunicationResponse.
	 */
	@ApiModelProperty(notes = "Id given by OTA connector referring to the session acknowledged by PrepareCommunicationResponse.")
	private BigInteger conversationId;
	/**
	 * MSISDN value to be used to address the SE through SMS bearer
	 */
	@ApiModelProperty(notes = "MSISDN value to be used to address the SE through SMS bearer")
	private String mobileSubscription;
	/**
	 * It is conditional due to it's availability directly
	 */
	@ApiModelProperty(notes = "It is conditional due to it's availability directly")
	private Long transactionId;
	/**
	 * User Data Header Indicator field is “1 bit field of SMS-SUBMIT PDU”
	 * defined in GSM 03.40 specs.
	 * 
	 * When udhi is set to True, 03.40 header of the message delivered will have
	 * TP-UDHI bit set to 1. If set to False, it will have TP-UDHI bit set to 0.
	 * 
	 * If not provided, OTAC considers value from the value configured by:
	 * sendSMSRawScript.defaultUDHI
	 */
	@ApiModelProperty(notes = "User Data Header Indicator field is “1 bit field of SMS-SUBMIT PDU” defined in GSM 03.40 specs.\r\n"
			+ "\r\n"
			+ "When udhi is set to True, 03.40 header of the message delivered will have TP-UDHI bit set to 1.\r\n"
			+ "\r\n"
			+ "If set to False, it will have TP-UDHI bit set to 0. If not provided, OTAC considers value from the value configured by: sendSMSRawScript.defaultUDHI")
	private Boolean udhi;

	/**
	 * UCS2 encoded value of the SMS content message.
	 */
	@NotNull(message = "The field should not be empty")
	@ApiModelProperty(required = true, notes = "UCS2 encoded value of the SMS content message.")
	private byte[] data;

	/**
	 * Value for the TP-PID field of the 03.40 header to be contained in
	 * addressed message.
	 * 
	 * Indicates the type of message sent.
	 */
	@ApiModelProperty(notes = "Value for the TP-PID field of the 03.40 header to be contained in addressed message.\r\n" + "\r\n"
			+ "Indicates the type of message sent. Default value = 0x00.")
	private PID pid = PID.TPPid_SIMPLE_SM;
	/**
	 * Value for the TP-DCS field of 03.40 header to be contained in addressed
	 * message.
	 * 
	 * Indicates how the message shall be handled by targeted equipment when
	 * received (displayed on screen SCR, stored in handset: MOB or SIM mailbox:
	 * SIM), and the character encoding defined for it
	 */
	@ApiModelProperty(notes = "Value for the TP-DCS field of 03.40 header to be contained in addressed message.\r\n"
			+ "\r\n"
			+ "Indicates how the message shall be handled by targeted equipment when received (displayed on screen  SCR, stored in handset: MOB or SIM mailbox: SIM), and the character encoding defined for it.Default value is changed from TPDcs_SCREEN(0xF0) to TPDcs_SCR_8b(0xF4)")
	private DCS dcs = DCS.TPDCS_SCR_8B;
	/**
	 * If not specified, the value is taken from the session’s MD set; labelled:
	 * ‘mdSet.SMS.X.ackType’.
	 */
	@ApiModelProperty(notes = "If not specified, the value is taken from the session’s MD set; labelled: ‘mdSet.SMS.X.ackType’.")
	private Boolean askNetworkAck;
	/**
	 * Validity Period in seconds of the message sent. If it is not passed the
	 * value will be taken from configured default setting.
	 */
	@ApiModelProperty(notes = "Validity Period in seconds of the message sent. If it is not passed the value will be taken from configured default setting.")
	private Integer vp;
	/**
	 * Selects through which group of Media Drivers this text will be delivered
	 * for this session. In industrial deployment, it represents the alias of
	 * MNO or SMS broker in charge of delivering the SMS messages.
	 */
	@ApiModelProperty(notes = "Selects through which group of Media Drivers this text will be delivered for this session.  In industrial deployment, it represents the alias of MNO or SMS broker in charge of delivering the SMS messages.")
	private String issuerOID;

	/**
	 * @return the conversationId
	 */
	public BigInteger getConversationId() {
		return conversationId;
	}

	/**
	 * @param conversationId
	 *            the conversationId to set
	 */
	public void setConversationId(BigInteger conversationId) {
		this.conversationId = conversationId;
	}

	/**
	 * @return the mobileSubscription
	 */
	public String getMobileSubscription() {
		return mobileSubscription;
	}

	/**
	 * @param mobileSubscription
	 *            the mobileSubscription to set
	 */
	public void setMobileSubscription(String mobileSubscription) {
		this.mobileSubscription = mobileSubscription;
	}

	/**
	 * @return the transactionId
	 */
	public Long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 *            the transactionId to set
	 */
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the udhi
	 */
	public Boolean getUdhi() {
		return udhi;
	}

	/**
	 * @param udhi
	 *            the udhi to set
	 */
	public void setUdhi(Boolean udhi) {
		this.udhi = udhi;
	}

	/**
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(byte[] data) {
		this.data = data;
	}

	/**
	 * @return the pid
	 */
	public PID getPid() {
		return pid;
	}

	/**
	 * @param pid
	 *            the pid to set
	 */
	public void setPid(PID pid) {
		this.pid = pid;
	}

	/**
	 * @return the dcs
	 */
	public DCS getDcs() {
		return dcs;
	}

	/**
	 * @param dcs
	 *            the dcs to set
	 */
	public void setDcs(DCS dcs) {
		this.dcs = dcs;
	}

	/**
	 * @return the askNetworkAck
	 */
	public Boolean getAskNetworkAck() {
		return askNetworkAck;
	}

	/**
	 * @param askNetworkAck
	 *            the askNetworkAck to set
	 */
	public void setAskNetworkAck(Boolean askNetworkAck) {
		this.askNetworkAck = askNetworkAck;
	}

	/**
	 * @return the vp
	 */
	public Integer getVp() {
		return vp;
	}

	/**
	 * @param vp
	 *            the vp to set
	 */
	public void setVp(Integer vp) {
		this.vp = vp;
	}

	/**
	 * @return the issuerOID
	 */
	public String getIssuerOID() {
		return issuerOID;
	}

	/**
	 * @param issuerOID
	 *            the issuerOID to set
	 */
	public void setIssuerOID(String issuerOID) {
		this.issuerOID = issuerOID;
	}

}
