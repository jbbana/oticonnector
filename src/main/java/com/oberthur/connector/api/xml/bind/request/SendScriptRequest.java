package com.oberthur.connector.api.xml.bind.request;

import static com.oberthur.apdugenerator.utils.Utils.hexStringToByteArray;

import java.math.BigInteger;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.oberthur.apdugenerator.keyset.KeySet;
import com.oberthur.scp80module.SCP80Security;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@XmlRootElement(name = RequestRootElementName.SEND_SCRIPT_REQUEST)
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value = "SendScriptRequest", description = "SendScriptRequest object to connector")
public class SendScriptRequest extends AbstractRequest {

	@ApiModelProperty(required = true)
	@NotNull(message = "Session id is null")
	private BigInteger conversationId;
	private String partnerOid;
	@ApiModelProperty(required = true)
	@NotNull(message = "Transaction id is null")
	private Long transactionId;
	@XmlElementWrapper
	@XmlElement(name = "apduBytes")
	@ApiModelProperty(required = true)
	@NotNull(message = "Script is empty")
	@Size(min = 1, message = "Script is empty")
	private List<byte[]> script;
	private String target;
	private Boolean askNetworkAck;
	private Integer validityPeriod;
	private KeySet scp80KeySet;
	private SCP80Security scp80Security;

	public static SendScriptRequest newInstance(BigInteger sessionId,
	                                            String partnerOid,
	                                            Long transactionId,
	                                            List<byte[]> script,
	                                            String target,
	                                            Boolean askNetworkAck,
	                                            Integer validityPeriod,
	                                            KeySet scp80KeySet,
	                                            SCP80Security scp80Security) {
		SendScriptRequest request = newInstance(sessionId, transactionId, script, target, validityPeriod);
		request.partnerOid = partnerOid;
		request.askNetworkAck = askNetworkAck;
		request.scp80KeySet = scp80KeySet;
		request.scp80Security = scp80Security;
		request.checkMandatoryParams();
		return request;
	}

	public static SendScriptRequest newInstance(BigInteger sessionId,
	                                            Long transactionId,
	                                            List<byte[]> script,
	                                            String target,
	                                            Integer validityPeriod) {
		SendScriptRequest request = new SendScriptRequest();
		request.conversationId = sessionId;
		request.transactionId = transactionId;
		request.script = script;
		request.target = target;
		request.validityPeriod = validityPeriod;
		request.checkMandatoryParams();
		return request;
	}

	@Override
	public void checkMandatoryParams() {
		if (target != null && target.trim().length() > 0) {
			checkTargetIdentifier(hexStringToByteArray(target));
		}
	}
}
