drop table if exists  oti_session;
CREATE TABLE oti_session (
    id                      BIGINT(20) NOT NULL AUTO_INCREMENT,
    session_id              BIGINT(20) NOT NULL,
    transaction_id          BIGINT(20) NULL,
    agent_id                VARCHAR(50) NOT NULL,
    secure_element          VARCHAR(50) NOT NULL,
    secure_element_type     VARCHAR(50) NOT NULL,
    target_id               VARCHAR(50) NULL,
    status                  VARCHAR(50) NOT NULL,
    login                   VARCHAR(50) NULL,
    password                VARCHAR(50) NULL,
    date_updated            VARCHAR(50) NULL,
    date_created            TIMESTAMP NOT NULL,
    purge_date              TIMESTAMP NOT NULL,
    date_valid              TIMESTAMP NULL, 
    PRIMARY KEY (id)
) COLLATE='utf8_general_ci' ENGINE=InnoDB;

SET foreign_key_checks = 0;
drop table if exists  interfaces;
SET foreign_key_checks = 1;
CREATE TABLE interfaces (
    id                     BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    interface_name         VARCHAR(50) NULL DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX interface_name (interface_name)
) COLLATE='utf8_general_ci' ENGINE=InnoDB;

drop table if exists  requests;
CREATE TABLE requests (
    id                     BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    requests_number        BIGINT(20) NULL DEFAULT NULL,
    errors_number          BIGINT(20) NULL DEFAULT NULL,
    last_notif_time        TIMESTAMP NULL DEFAULT NULL,
    fk_interface           BIGINT(20) UNSIGNED NOT NULL,
    PRIMARY KEY (id),
    INDEX REQ_FK1 (fk_interface),
    CONSTRAINT REQ_FK1 FOREIGN KEY (fk_interface) REFERENCES interfaces (id)
) COLLATE='utf8_general_ci' ENGINE=InnoDB;

drop table if exists  average_resp_time;
CREATE TABLE average_resp_time (
    id                    BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    hits_counter          BIGINT(20) NULL,
    avg_resp_time         BIGINT(20) NULL,
    fk_interface          BIGINT(20) UNSIGNED NOT NULL,
    PRIMARY KEY (id),
    INDEX AVG_FK1 (fk_interface),
    CONSTRAINT AVG_FK1 FOREIGN KEY (fk_interface) REFERENCES interfaces (id)
) COLLATE='utf8_general_ci' ENGINE=InnoDB;

insert into interfaces(interface_name) values('PREPARE_COMMUNICATION_ASYNC_RESPONSE'), ('CALLER_DROP_COMMUNICATION'), ('CALLER_PREPARE_COMMUNICATION'), ('CALLER_SEND_SCRIPT'), ('DEVICE_REQUEST'), ('DEVICE_RESPONSE'), ('SEND_SCRIPT_ASYNC_RESPONSE');


