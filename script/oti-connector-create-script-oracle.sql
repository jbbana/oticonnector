DROP TABLE OTI_SESSION;
CREATE TABLE OTI_SESSION
(
  ID                    NUMBER PRIMARY KEY,
  SESSION_ID            NUMBER NOT NULL,
  TRANSACTION_ID        NUMBER,
  AGENT_ID              VARCHAR2(50) NOT NULL,
  SECURE_ELEMENT        VARCHAR2(50) NOT NULL,
  SECURE_ELEMENT_TYPE   VARCHAR2(50) NOT NULL,
  STATUS                VARCHAR2(50) NOT NULL,
  LOGIN                 VARCHAR2(50),
  PASSWORD              VARCHAR2(50),
  DATE_CREATED          TIMESTAMP    NOT NULL,
  DATE_UPDATED          TIMESTAMP	 NOT NULL,
  PURGE_DATE            TIMESTAMP    NOT NULL,
  DATE_VALID            TIMESTAMP,
  CONSTRAINT UQ_OTI_SESSION_SECURE_ELEMENT UNIQUE (SECURE_ELEMENT)
);

DROP SEQUENCE OTI_ID_SEQ;
CREATE SEQUENCE OTI_ID_SEQ
START WITH 1
INCREMENT BY 1
NOMAXVALUE;
/
